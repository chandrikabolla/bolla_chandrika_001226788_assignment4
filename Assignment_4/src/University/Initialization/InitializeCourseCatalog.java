/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Initialization;

import University.Business.Course;
import University.Business.DepartmentCourseCatalog;
import java.util.ArrayList;

/**
 *
 * @author Nidhi Mittal <mittal.n@husky.neu.edu>
 */
public class InitializeCourseCatalog {
    
    public static DepartmentCourseCatalog departmentCourseCatalog;
    static Course course1;
    static Course course2;
    
    public static DepartmentCourseCatalog initializeCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CSYE6200");
        course1.setCourseName("Concepts of Object Oriented");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the concepts of object oriented");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("INFO5100");
        course2.setCourseName("Application Engineering and Development");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the concepts of Engineering");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }

    
}
