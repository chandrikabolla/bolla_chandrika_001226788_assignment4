/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Initialization;

import University.Business.Staff;
import University.Business.Teacher;

/**
 *
 * @author chand
 */
public class InitializeTeacherStaff {
    static Teacher teacher;
    static Staff staff;
   
    public static Teacher initializeTeacher(){
        teacher=new Teacher("professor");
        teacher.getPerson().setFirstName("Chandrika");
        teacher.getPerson().setLastName("Bolla");
        teacher.getPerson().setDateOfBirth("09-Oct-1995");
        teacher.getPerson().setEmailId("Chandrikabolla999@gmail.com");
        teacher.getPerson().setPhoneNumber("857-707-6850");
        teacher.getPerson().setSocialSecurityNumber("333-333-333");
        teacher.getPerson().setAddress("11 tetlow street,Boston,MA 02115");
        teacher.getPosition().setPositionType("Full-time");
       
        
        return teacher;
    }
    public static Staff initializeStaff(){
        staff=new Staff("advisor");
        System.out.println(staff.getPerson());
        staff.getPerson().setFirstName("Chadnri");
        staff.getPerson().setLastName("Bolla");
        staff.getPerson().setEmailId("chand9mail@gmail.com");
        staff.getPerson().setPhoneNumber("857-707-6851");
        staff.getPerson().setDateOfBirth("09-08-1cd 995");
        staff.getPerson().setAddress("12 tetlow,Boston,MA 02115");
        staff.getPerson().setSocialSecurityNumber("404-404-404");
        staff.getPosition().setPositionType("Part-time");
       
       return staff; 
    }
   
}
