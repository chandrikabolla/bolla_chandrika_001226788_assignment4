/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Initialization;

import University.Business.ClassRoom;
import University.Business.CourseOffering;
import University.Business.DepartmentCourseSchedule;
import University.Business.JobPosition;
import University.Business.Person;
import University.Business.Semester;
import University.Business.Teacher;
import static University.Initialization.InitializeTeacherStaff.initializeTeacher;
import static University.Initialization.InitializeTeacherStaff.teacher;

/**
 *
 * @author Nidhi Mittal <mittal.n@husky.neu.edu>
 */
public class InitializeCourseSchedule {
    
    static ClassRoom classRoom1;
    static ClassRoom classRoom2;
    static Semester semester1;
    static Semester semester2;
    public static DepartmentCourseSchedule departmentCourseSchedule;
    static CourseOffering courseOffering1;
    static CourseOffering courseOffering2;
    
    
    
    public static DepartmentCourseSchedule initializeCourseSchedule() {
        classRoom1 = new ClassRoom();
        classRoom1.setRoomNumber("315");
        classRoom1.setBuildingNumber("West Village F20");
        
        classRoom2 = new ClassRoom();
        classRoom2.setRoomNumber("208");
        classRoom2.setBuildingNumber("Snell Engineering Center");
        
        semester1 = new Semester();
        semester1.setStartDate("Januaray 2016");
        semester1.setEndDate("May 2016");
        semester1.setSemesterName("Spring 2016");
        
        semester2 = new Semester();
        semester2.setStartDate("August 2016");
        semester2.setEndDate("December 2016");
        semester2.setSemesterName("Fall 2016");
        
        
        
        
        departmentCourseSchedule = new DepartmentCourseSchedule();
        
        courseOffering1 = departmentCourseSchedule.addCourseOffering();       
        courseOffering1.setSectionNumber("A");
        //courseOffering1.setTeacher("Nan Yang");
        InitializeTeacherStaff initialization = new InitializeTeacherStaff();
        initialization.initializeTeacher();
        courseOffering1.setTeacher(teacher);
        courseOffering1.setCapacity(250);
        courseOffering1.setSeatsFilled(180);
        courseOffering1.setTiming("Monday 8am - 11:30am");
        courseOffering1.setClassRoom(classRoom1);
        courseOffering1.setSemester(semester1);
        courseOffering1.setCourse(InitializeCourseCatalog.course1);
        
        courseOffering2 = departmentCourseSchedule.addCourseOffering();
        courseOffering2.setSectionNumber("b");
        //courseOffering2.setTeacher("Neeraj sing Rajput");
        courseOffering2.setCapacity(150);
        courseOffering2.setSeatsFilled(90);
        courseOffering2.setTiming("Friday 8am - 11:30am");
        courseOffering2.setClassRoom(classRoom2);
        courseOffering2.setSemester(semester2);
        courseOffering2.setCourse(InitializeCourseCatalog.course2);
        
        return departmentCourseSchedule;
        
    }
    
}
