/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Business;

import University.*;
import java.util.ArrayList;

/**
 *
 * @author Nidhi Mittal <mittal.n@husky.neu.edu>
 */
public class DepartmentCourseCatalog {

    private ArrayList<Course> departmentCourseCatalog;

    public DepartmentCourseCatalog() {
        departmentCourseCatalog = new ArrayList<Course>();
    }

    public ArrayList<Course> getDepartmentCourseCatalog() {
        return departmentCourseCatalog;
    }

    public void setDepartmentCourseCatalog(ArrayList<Course> departmentCourseCatalog) {
        this.departmentCourseCatalog = departmentCourseCatalog;
    }

    public Course addCourse() {
        Course course = new Course();
        departmentCourseCatalog.add(course);
        return course;
    }

    public void deleteCourse(Course course) {
        departmentCourseCatalog.remove(course);
    }

    public Course findCourseByCourseNumber(String courseNumber) {
        for (Course course : departmentCourseCatalog) {
            if (courseNumber.equals(course.getCourseNumber())) {
                return course;
            }

        }
        return null;
    }
}