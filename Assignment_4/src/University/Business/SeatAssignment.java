/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Business;

/**
 *
 * @author Guest
 */
public class SeatAssignment {

    private double studentGrade;
    private double courseFees;
    private Student studentName;
    private String course; 

     public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }
    public double getCourseFees() {
        return courseFees;
    }

    public void setCourseFees(double courseFees) {
        this.courseFees = courseFees;
    }

    public Student getStudentName() {
        return studentName;
    }

    public void setStudentName(Student studentName) {
        this.studentName = studentName;
    }

    public double getStudentGrade() {
        return studentGrade;
    }

    public void setStudentGrade(double studentGrade) {
        this.studentGrade = studentGrade;
    }

    public double getPaidCoursePrice() {

        return courseFees;
    }

}
