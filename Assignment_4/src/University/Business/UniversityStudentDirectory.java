/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Business;

/**
 *
 * @author Guest
 */
public class UniversityStudentDirectory {
    private Student studentName;
    private Student studentId;
    private Student email;
    private Student phoneNumber;

    public Student getStudentName() {
        return studentName;
    }

    public void setStudentName(Student studentName) {
        this.studentName = studentName;
    }

    public Student getStudentId() {
        return studentId;
    }

    public void setStudentId(Student studentId) {
        this.studentId = studentId;
    }

    public Student getEmail() {
        return email;
    }

    public void setEmail(Student email) {
        this.email = email;
    }

    public Student getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Student phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    
    
}
