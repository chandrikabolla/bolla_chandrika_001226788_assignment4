/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Business;

/**
 *
 * @author chand
 */
public class Teacher extends Role {

    JobPosition position;
    private Person person;

    public Teacher(String role) {
        super(role);
        person = new Person();
        position = new JobPosition();
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public JobPosition getPosition() {
        return position;
    }

    public void setPosition(JobPosition position) {
        this.position = position;
    }
    
    @Override
    public String toString(){
        return this.person.getFirstName() + "\n" + this.person.getLastName() + "\n" + this.person.getDateOfBirth() + "\n" + this.person.getEmailId() + "\n" + this.person.getPhoneNumber() + "\n" + this.person.getAddress() + "\n" + this.person.getSocialSecurityNumber() + "\n" + this.position.getPositionType() + "\n" + this.getRole();
        
    }

}
