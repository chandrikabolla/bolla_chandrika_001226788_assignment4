/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Business;

/**
 *
 * @author Guest
 */
public class Department {
    private String departmentName;
    private int departmentCode;
    private DepartmentCourseSchedule departmentCourseSchedule;
    private Degree degree;
    private DepartmentCourseCatalog departmentCourseCatalog;

    public DepartmentCourseSchedule getDepartmentCourseSchedule() {
        return departmentCourseSchedule;
    }

    public void setDepartmentCourseSchedule(DepartmentCourseSchedule departmentCourseSchedule) {
        this.departmentCourseSchedule = departmentCourseSchedule;
    }

    public Degree getDegree() {
        return degree;
    }

    public void setDegree(Degree degree) {
        this.degree = degree;
    }

    public DepartmentCourseCatalog getDepartmentCourseCatalog() {
        return departmentCourseCatalog;
    }

    public void setDepartmentCourseCatalog(DepartmentCourseCatalog departmentCourseCatalog) {
        this.departmentCourseCatalog = departmentCourseCatalog;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public int getDepartmentCode() {
        return departmentCode;
    }

    public void setDepartmentCode(int departmentCode) {
        this.departmentCode = departmentCode;
    }
    
    
}
