/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Business;

import University.*;
import java.util.ArrayList;

/**
 *
 * @author Nidhi Mittal <mittal.n@husky.neu.edu>
 */
public class Course {

    private String courseNumber;
    private String courseName;
    private double courseFees;
    private String currencyString;
    private String courseDescription;
    private String creditHours;
    private boolean isCore;
    private boolean isElective;
    private ArrayList<Course> preRequisiteCourses;
    
    public Course() {
        preRequisiteCourses = new ArrayList<Course>();
    }

    public String getCourseNumber() {
        return courseNumber;
    }

    public void setCourseNumber(String courseNumber) {
        this.courseNumber = courseNumber;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public double getCourseFees() {
        return courseFees;
    }

    public void setCourseFees(double courseFees) {
        this.courseFees = courseFees;
    }

    public String getCourseDescription() {
        return courseDescription;
    }

    public void setCourseDescription(String courseDescription) {
        this.courseDescription = courseDescription;
    }

    public String getCreditHours() {
        return creditHours;
    }

    public void setCreditHours(String creditHours) {
        this.creditHours = creditHours;
    }
   
    public boolean isIsCore() {
        return isCore;
    }

    public void setIsCore(boolean isCore) {
        this.isCore = isCore;
    }

    public boolean isIsElective() {
        return isElective;
    }

    public String getCurrencyString() {
        return currencyString;
    }

    public void setCurrencyString(String currencyString) {
        this.currencyString = currencyString;
    }
    

    public void setIsElective(boolean isElective) {
        this.isElective = isElective;
    }

    public ArrayList<Course> getPreRequisiteCourses() {
        return preRequisiteCourses;
    }

    public void setPreRequisiteCourses(ArrayList<Course> preRequisiteCourses) {
        this.preRequisiteCourses = preRequisiteCourses;
    }
    

}
