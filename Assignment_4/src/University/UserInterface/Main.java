/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.UserInterface;

import University.Business.Course;
import University.Business.CourseOffering;
import University.Initialization.*;

/**
 *
 * @author Nidhi Mittal <mittal.n@husky.neu.edu>
 */
public class Main {
    
    public static void main(String[] args) {
        InitializeCourseCatalog.initializeCourseCatalog();
        
        // print course catalog
        for (Course course : InitializeCourseCatalog.initializeCourseCatalog().getDepartmentCourseCatalog()) {
            System.out.println("\nCourse Number-->" + course.getCourseNumber() +
                    "\n Course Name-->" + course.getCourseName() + 
                    "\n Course Price-->" + course.getCourseFees() + 
                    "\n Course Description-->" + course.getCourseDescription() + 
                    "\n Credit hours-->" + course.getCreditHours() +
                    "\n Core Course -- >" + course.isIsCore() +
                    "\n Elective Course -- >" + course.isIsElective()
                    );
            if (course.getPreRequisiteCourses() != null) {
                System.out.println("Pre-requisite courses: ");
            for (Course preReqCourse : course.getPreRequisiteCourses()) {
                System.out.print(preReqCourse.getCourseName());
            }
            System.out.println("\n"); 
            }   
        }
        
        // print course schedule
        for (CourseOffering courseOffering : InitializeCourseSchedule.initializeCourseSchedule().getDepartmentCourseSchedule()) {
            System.out.println(
                    "\n Teacher ---> " + courseOffering.getTeacher() + 
                    "\n Timing ---> " + courseOffering.getTiming() + 
                    "\n Capacity ---> " + courseOffering.getCapacity() + 
                    "\n SeatsFilled ---> " + courseOffering.getSeatsFilled() + 
                    "\n Vacant seats ---> " + courseOffering.getSeatsVacant() + 
                    "\n Section number ---> " + courseOffering.getSectionNumber() +
                    "\n Class Room Number ---> " + courseOffering.getClassRoom().getRoomNumber() + 
                    "\n Class Room Building ---> " + courseOffering.getClassRoom().getBuildingNumber() +
                    "\n Semester name ---> " + courseOffering.getSemester().getSemesterName() + 
                    "\n Semester start date ---> " + courseOffering.getSemester().getStartDate() + 
                    "\n Semester End Date ---> " + courseOffering.getSemester().getEndDate() +
                    "\n Course Number ---> " + courseOffering.getCourse().getCourseNumber() + 
                    "\n Course Name ---> " + courseOffering.getCourse().getCourseName() 
                    
            );
        }
        
    }

    
}
