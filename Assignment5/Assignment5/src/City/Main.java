/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package City;

import City.Business.Community;
import City.Business.CommunityDirectory;
import City.Business.Family;
import City.Business.House;
import City.Business.Patient;
import Initialization.CityInitialization;

/**
 *
 * @author Guest
 */
public class Main {
    
    public static void main(String args[])
    {
        CommunityDirectory communityDirectory=Initialization.CityInitialization.initializecity();
        
       for(Community community:communityDirectory.getCommunityList())
       {
       
        System.out.println(community.getHouseDirectory().getHouseList().get(0).getHouseaddress());
       for(House house:community.getHouseDirectory().getHouseList())
       {
           System.out.println("---------------------------------------------------------------------------------------------------------");
           System.out.println("House number: "+house.getHouseNumber()+"\nHouse Address: "+house.getHouseaddress());
           for(Family family:house.getFamilyDirectory().getFamilyList())
           {
               System.out.println("*******************************************************************************************************");
               System.out.println("Family name: "+family.getFamilyName()+"\nFamily address: "+family.getFamilyAddress()+"Number of patients :"+family.getPatientDirectory().getPatientList().size());
               System.out.println("No of grand parents: "+family.getGrandParents().size());
               for(Patient patient:family.getGrandParents())
               {
                   System.out.println("Patient name: "+patient.getFirstName()+" "+patient.getLastName()+"  Age:"+patient.getAge()+" Gender: "+patient.getGender());
                   
               }
               System.out.println("Number of Parents: "+family.getParents().size());
               for(Patient patient:family.getParents())
                {
                   System.out.println("Patient name: "+patient.getFirstName()+" "+patient.getLastName()+"  Age:"+patient.getAge()+" Gender: "+patient.getGender());
                }
              
               
               
               
               System.out.println("Number of individuals: "+ family.getParents().size());
               for(Patient patient:family.getIndividuals())
               {
                   System.out.println("Patient name: "+patient.getFirstName()+" "+patient.getLastName()+"  Age:"+patient.getAge()+" Gender: "+patient.getGender());
               }
               
               System.out.println("Number of Children: "+family.getChildren().size());
               for(Patient patient:family.getChildren())
               {
                   System.out.println("Patient name: "+patient.getFirstName()+" "+patient.getLastName()+"  Age:"+patient.getAge()+" Gender: "+patient.getGender());
               }
               
               
               
                System.out.println("*******************************************************************************************************");
               
           }
           System.out.println("---------------------------------------------------------------------------------------------------------");
           
       }
       }
       
  
        
        
    }
    
}
