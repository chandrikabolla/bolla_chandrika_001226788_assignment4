/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package City.Business;

import java.util.ArrayList;

/**
 *
 * @author Nidhi Mittal
 */
public class VitalSignsDirectory {
    
     private ArrayList<VitalSigns> vitalSignsList;

    public VitalSignsDirectory() {
        this.vitalSignsList = new ArrayList<>();
    }

    /* Setter & Getter for VitalSignList */
    public ArrayList<VitalSigns> getVitalSignsList() {
        return vitalSignsList;
    }

    public void setVitalSignsList(ArrayList<VitalSigns> vitalSignsList) {
        this.vitalSignsList = vitalSignsList;
    }

    public VitalSigns addVitalSigns() {

        VitalSigns vitalSigns = new VitalSigns();
        this.vitalSignsList.add(vitalSigns);
        return vitalSigns;

    }

                                                
    public void removeVitalSigns(VitalSigns vitalSigns) {
        this.vitalSignsList.remove(vitalSigns);
    }
    
    
     /* Checks if the Vital Signs are Normal or Abnormal
    
     Returns:
     1. 1 - Integer denoting that the Vital Signs are normal
     2. 0 - Integer denoting that the Vital Signs are abnormal */
    
    /*public int checkVitalSignsStatus(VitalSigns vitalSigns, int age) {

        int status = 0;

        float respiratoryRate = vitalSigns.getRespiratoryRate();
        float heartRate = vitalSigns.getHeartRate();
        int systolicBloodPressure = vitalSigns.getSystolicBloodPressure();
        float weight = vitalSigns.getWeight();

        if ((age >= 1 && age <= 3) && (respiratoryRate >= 20 && respiratoryRate <= 30)
                && (heartRate >= 80 && heartRate <= 130)
                && (systolicBloodPressure >= 80 && systolicBloodPressure <= 110)
                && (weight >= 22 && weight <= 31)) {
            status = 1;
        } else if ((age >= 4 && age <= 5) && (respiratoryRate >= 20 && respiratoryRate <= 30)
                && (heartRate >= 80 && heartRate <= 120)
                && (systolicBloodPressure >= 80 && systolicBloodPressure <= 110)
                && (weight >= 31 && weight <= 40)) {
            status = 1;
        } else if ((age >= 6 && age <= 12) && (respiratoryRate >= 20 && respiratoryRate <= 30)
                && (heartRate >= 70 && heartRate <= 110)
                && (systolicBloodPressure >= 80 && systolicBloodPressure <= 120)
                && (weight >= 41 && weight <= 92)) {
            status = 1;
        } else if ((age >= 13) && (respiratoryRate >= 12 && respiratoryRate <= 20)
                && (heartRate >= 55 && heartRate <= 105)
                && (systolicBloodPressure >= 110 && systolicBloodPressure <= 120)
                && (weight >= 110)) {
            status = 1;
        }

        return status;

    }*/
}
