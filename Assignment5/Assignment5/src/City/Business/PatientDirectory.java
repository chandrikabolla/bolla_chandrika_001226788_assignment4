/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package City.Business;
import City.Business.Person;
import City.Business.PersonDirectory;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Guest
 */
public class PatientDirectory {
        
    List<Patient> patientList;
    Patient patient1;
    //public PersonDirectory personDirectory;
     String personName;
      public ArrayList<Person> personList;
    
 

    public PatientDirectory() {
        patientList = new ArrayList<Patient>();
       personList=new ArrayList<Person>();
    }
 
    public List<Patient> getPatientList() {
        return patientList;
    }

    public void setPatientList(List<Patient> patientList) {
        this.patientList = patientList;
    }

    public Patient addPatient() {
        Patient patient=new Patient();
        patientList.add(patient);
        return patient;
    }
    public Person searchPerson(String personname)
    {
     this.personName=personname;
        for(Person person2 :personList)
        {
            if(person2.getFirstName().equals(personname))
            {
                    return person2;
            }
            
        }
        
        return null;
    }
    
}
