/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package City.Business;

/**
 *
 * @author Guest
 */
public class Patient extends Person {

    //public Person person;
    public String primDocName;
    public String prefPharmacy;
    public boolean smoking;
    public boolean diabetes;
    private float avgRespiratoryRate;
    private float avgHeartRate;
    private float avgSystolicBloodPressure;
    private float avgWeight;
    private float totalPoints;
    private VitalSignsDirectory vitalSignsDirectory;


    /*public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }*/
    public String getPrimDocName() {
        return primDocName;
    }

    public void setPrimDocName(String primDocName) {
        this.primDocName = primDocName;
    }

    public String getPrefPharmacy() {
        return prefPharmacy;
    }

    public void setPrefPharmacy(String prefPharmacy) {
        this.prefPharmacy = prefPharmacy;
    }

    public boolean isSmoking() {
        return smoking;
    }

    public void setSmoking(boolean smoking) {
        this.smoking = smoking;
    }

    public boolean isDiabetes() {
        return diabetes;
    }

    public void setDiabetes(boolean diabetes) {
        this.diabetes = diabetes;
    }

    public float getAvgRespiratoryRate() {
        return avgRespiratoryRate;
    }

    public void setAvgRespiratoryRate(float avgRespiratoryRate) {
        this.avgRespiratoryRate = avgRespiratoryRate;
    }

    public float getAvgHeartRate() {
        return avgHeartRate;
    }

    public void setAvgHeartRate(float avgHeartRate) {
        this.avgHeartRate = avgHeartRate;
    }

    public float getAvgSystolicBloodPressure() {
        return avgSystolicBloodPressure;
    }

    public void setAvgSystolicBloodPressure(float avgSystolicBloodPressure) {
        this.avgSystolicBloodPressure = avgSystolicBloodPressure;
    }

    public float getAvgWeight() {
        return avgWeight;
    }

    public void setAvgWeight(float avgWeight) {
        this.avgWeight = avgWeight;
    }

    
    /*public String meanNormalAbnormal() {
        float sum = 0;
        String riskFactor = "High Risk";
        VitalSignsDirectory vitalSign = new VitalSignsDirectory();
        for (int i = 0; i < 5; i++) {
            VitalSigns vtEntry = vitalSign.addVitalSigns();
            sum = sum + vtEntry.getHeartRate();
            avgHeartRate = sum / 5;

        }

        for (int i = 0; i < 5; i++) {
            VitalSigns vtEntry = vitalSign.addVitalSigns();
            sum = sum + vtEntry.getRespiratoryRate();
            avgRespiratoryRate = sum / 5;

        }

        for (int i = 0; i < 5; i++) {
            VitalSigns vtEntry = vitalSign.addVitalSigns();
            sum = sum + vtEntry.getSystolicBloodPressure();
            avgSystolicBloodPressure = sum / 5;

        }

        for (int i = 0; i < 5; i++) {
            VitalSigns vtEntry = vitalSign.addVitalSigns();
            sum = sum + vtEntry.getWeight();
            avgWeight = sum / 5;

        }

        if ((age >= 1 && age <= 3) && (avgRespiratoryRate >= 20 && avgRespiratoryRate <= 30)
                && (avgHeartRate >= 80 && avgHeartRate <= 130)
                && (avgSystolicBloodPressure >= 80 && avgSystolicBloodPressure <= 110)
                && (avgWeight >= 22 && avgWeight <= 31)) {
            riskFactor = "Normal";
        } else if ((age >= 4 && age <= 5) && (avgRespiratoryRate >= 20 && avgRespiratoryRate <= 30)
                && (avgHeartRate >= 80 && avgHeartRate <= 120)
                && (avgSystolicBloodPressure >= 80 && avgSystolicBloodPressure <= 110)
                && (avgWeight >= 31 && avgWeight <= 40)) {
            riskFactor = "Normal";
        } else if ((age >= 6 && age <= 12) && (avgRespiratoryRate >= 20 && avgRespiratoryRate <= 30)
                && (avgHeartRate >= 70 && avgHeartRate <= 110)
                && (avgSystolicBloodPressure >= 80 && avgSystolicBloodPressure <= 120)
                && (avgWeight >= 41 && avgWeight <= 92)) {
            riskFactor = "Normal";
        } else if ((age >= 13) && (avgRespiratoryRate >= 12 && avgRespiratoryRate <= 20)
                && (avgHeartRate >= 55 && avgHeartRate <= 105)
                && (avgSystolicBloodPressure >= 110 && avgSystolicBloodPressure <= 120)
                && (avgWeight >= 110)) {
            riskFactor = "Normal";
        }

        return riskFactor;

    }*/

    public VitalSignsDirectory getVitalSignsDirectory() {
        return vitalSignsDirectory;
    }

    public void setVitalSignsDirectory(VitalSignsDirectory vitalSignsDirectory) {
        this.vitalSignsDirectory = vitalSignsDirectory;
    }

}
