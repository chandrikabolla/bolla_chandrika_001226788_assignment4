/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package City.Business;

import java.util.ArrayList;

/**
 *
 * @author Guest
 */
public class Family {
    public PatientDirectory patientDirectory;
    ArrayList<Patient> grandParents,parents,individuals,children;
    
    public String familyName;
    public String familyAddress;
    
    public Family(){
        patientDirectory=new PatientDirectory();
            grandParents=new ArrayList<Patient>();
            parents=new ArrayList<Patient>();
            individuals=new ArrayList<Patient>();
            children=new ArrayList<Patient>();
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getFamilyAddress() {
        return familyAddress;
    }

    public void setFamilyAddress(String familyAddress) {
        this.familyAddress = familyAddress;
    }

    public PatientDirectory getPatientDirectory() {
        return patientDirectory;
    }

    public void setPatientDirectory(PatientDirectory patientDirectory) {
        this.patientDirectory = patientDirectory;
    }

    public ArrayList<Patient> getGrandParents() {
        return grandParents;
    }

    public void setGrandParents(ArrayList<Patient> grandParents) {
        this.grandParents = grandParents;
    }

    public ArrayList<Patient> getParents() {
        return parents;
    }

    public void setParents(ArrayList<Patient> parents) {
        this.parents = parents;
    }

    public ArrayList<Patient> getIndividuals() {
        return individuals;
    }

    public void setIndividuals(ArrayList<Patient> individuals) {
        this.individuals = individuals;
    }

    public ArrayList<Patient> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<Patient> children) {
        this.children = children;
    }

    
    
    
    
    @Override
    public String toString(){
        return familyName;
    }
}
