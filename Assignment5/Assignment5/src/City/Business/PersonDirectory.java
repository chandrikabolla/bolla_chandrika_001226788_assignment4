/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package City.Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Guest
 */
public class PersonDirectory {
        
    List<Person> personList;
    Person person1;
    String personName;

    public PersonDirectory() {
        personList = new ArrayList<Person>();
    }

    public List<Person> getPersonList() {
        return personList;
    }

    public void setPersonList(List<Person> personList) {
        this.personList = personList;
    }

    public Person addPerson() {
       Person person=new Person();
        personList.add(person);
        return person;
    }
    public Person searchPerson(String personname)
    {
        this.personName=personname;
        for(Person person2 :personList)
        {
            if(person2.getFirstName().equals(personname))
            {
                    return person2;
            }
            
        }
        return null;
    }
    
}
