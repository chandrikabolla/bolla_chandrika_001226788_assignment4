/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Initialization;
import City.Business.City;
import City.Business.Patient;
import City.Business.PatientDirectory;
import City.Business.Person;
import City.Business.PersonDirectory;
import City.Business.VitalSigns;
import City.Business.VitalSignsDirectory;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Guest
 */
public class PatientInitialization {
    static Patient patient;
    static VitalSignsDirectory vitalSignsDirectory;
    
    
   public static ArrayList<String> existing;

    
        public static int randInt(int min, int max) {

        // Usually this can be a field rather than a method variable
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    public static PatientDirectory initPatientDirectory(String lastname,int numberOfPersons) {
        PatientDirectory patientDirectory=new PatientDirectory();
        Random rand = new Random();
        existing=new ArrayList<String>();
        //There are two ways presented in here for random string generation you can use whatever you feel is right.
        // String myCharacters = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm";
        //Insert your user freindly names here
        String[] array = {"Neeraj", "Katie", "Courtney", "Kelsey", "David", "Alex", "Yi", "Zhang", "Hang", "Nan", "Yiming", "Sarah"};

String[] genderArray={"male","female"};
       
     int  generatedNumber=0;
     while(generatedNumber<=numberOfPersons)
     {
                    
         
            String name = array[rand.nextInt(array.length)];
           
                if(!existing.contains(name))
                {
                    //Personal details of patient 
                    existing.add(name);
                    Patient patient= patientDirectory.addPatient();
                    patient.setFirstName(name);
                    patient.setLastName(lastname);
                    int n = rand.nextInt(80) + 1;
                    patient.setAge(n);
            
                    patient.setGender(genderArray[rand.nextInt(genderArray.length)]);
           
            
            
                    //hospital details of patient
                    patient.setSmoking(true);
                    patient.setDiabetes(true);
                    patient.setVitalSignsDirectory(Initialization.VitalSignInitialization.initVitalSign());
                    patient.setPrefPharmacy("CVS");
                    patient.setPrimDocName("Chandrika");
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
       
                    //incrementing number of people in the family here
                    generatedNumber++;  
            
            }
           
         }
     
         return patientDirectory;
        
       
    } 
    
    public static int BloodPressurePointsStep4(Patient patient){
    
    int bloodPresuurePoints=0;
    int bloodPressurePointsSys=0;
    int bloodPressurePointsDia=0;
    VitalSignsDirectory vitalSignsDirectory;
    int sumDia=0;
    int sumSys=0;
    float averageDia=0;
    float averageSys=0;
    
    //for female
    if(patient.getGender().equalsIgnoreCase("female"))
    {
        
        vitalSignsDirectory=patient.getVitalSignsDirectory();
        
        //calculating average of systolic pressure
        for(VitalSigns vitalSign:vitalSignsDirectory.getVitalSignsList())
        {
            sumSys=sumSys+vitalSign.getSystolicBloodPressure();
        }
        averageSys=sumSys/6;
        
        //calculating average of diastolic pressure
         for(VitalSigns vitalSign:vitalSignsDirectory.getVitalSignsList())
        {
            sumDia=sumDia+vitalSign.getSystolicBloodPressure();
        }
        averageDia=sumDia/6;
        
        //setting points using systolic blood pressure
        if(averageSys<120)
        {
            bloodPressurePointsSys=(-3);
        }
        else if(averageSys>=120&&averageSys<=129)
        {
            bloodPressurePointsSys=0;
        }
        else if(averageSys>=130&&averageSys<=139)
        {
            bloodPressurePointsSys=0;
        }
        else if(averageSys>=140&&averageSys<=159)
        {
            bloodPressurePointsSys=2;
        }
        else if(averageSys>=160)
        {
            bloodPressurePointsSys=3;
        }
        
        //setting points using diastolic blood pressure
        if(averageDia<80)
        {
            bloodPressurePointsDia=(-3);
        }
        else if(averageDia>=80&&averageDia<=84)
        {
            bloodPressurePointsDia=0;
        }
        else if(averageDia>=85&&averageDia<=89)
        {
            bloodPressurePointsDia=0;
        }
        else if(averageDia>=90&&averageDia<=99)
        {
            bloodPressurePointsDia=2;
        }
        else if(averageDia>=100)
        {
            bloodPressurePointsDia=3;
        }
        if(bloodPressurePointsDia>bloodPressurePointsSys)
        {
            bloodPresuurePoints=bloodPressurePointsDia;
        }
        else if(bloodPressurePointsSys>bloodPressurePointsDia)
        {
            bloodPresuurePoints=bloodPressurePointsSys;
        }
        else if(bloodPressurePointsDia==bloodPressurePointsSys)
        {
            bloodPresuurePoints=bloodPressurePointsDia;
        }
       
        
        
        
    }
    
    //for male
    else if(patient.getGender().equalsIgnoreCase("male")){
        
        vitalSignsDirectory=patient.getVitalSignsDirectory();
        
        //calculating average of systolic pressure
        for(VitalSigns vitalSign:vitalSignsDirectory.getVitalSignsList())
        {
            sumSys=sumSys+vitalSign.getSystolicBloodPressure();
        }
        averageSys=sumSys/6;
        
        //calculating average of diastolic pressure
         for(VitalSigns vitalSign:vitalSignsDirectory.getVitalSignsList())
        {
            sumDia=sumDia+vitalSign.getSystolicBloodPressure();
        }
        averageDia=sumDia/6;
        
        //setting points using systolic blood pressure
        if(averageSys<120)
        {
            bloodPressurePointsSys=0;
        }
        else if(averageSys>=120&&averageSys<=129)
        {
            bloodPressurePointsSys=0;
        }
        else if(averageSys>=130&&averageSys<=139)
        {
            bloodPressurePointsSys=1;
        }
        else if(averageSys>=140&&averageSys<=159)
        {
            bloodPressurePointsSys=2;
        }
        else if(averageSys>=160)
        {
            bloodPressurePointsSys=3;
        }
        
        //setting points using diastolic blood pressure
        if(averageDia<80)
        {
            bloodPressurePointsDia=0;
        }
        else if(averageDia>=80&&averageDia<=84)
        {
            bloodPressurePointsDia=0;
        }
        else if(averageDia>=85&&averageDia<=89)
        {
            bloodPressurePointsDia=1;
        }
        else if(averageDia>=90&&averageDia<=99)
        {
            bloodPressurePointsDia=2;
        }
        else if(averageDia>=100)
        {
            bloodPressurePointsDia=3;
        }
        if(bloodPressurePointsDia>bloodPressurePointsSys)
        {
            bloodPresuurePoints=bloodPressurePointsDia;
        }
        else if(bloodPressurePointsSys>bloodPressurePointsDia)
        {
            bloodPresuurePoints=bloodPressurePointsSys;
        }
        else if(bloodPressurePointsDia==bloodPressurePointsSys)
        {
            bloodPresuurePoints=bloodPressurePointsDia;
        }
       
        
        
        
    }
    
    
    
    
    return bloodPresuurePoints;
    
}
  public static int DiabetesPoints(Patient patient){
      
      int diabetesPoints=0;
      //setting diabetes points for male members
      if(patient.getGender().equalsIgnoreCase("male"))
      {
          if(patient.isDiabetes())
          {
              diabetesPoints=2;
          }
          else if(!patient.isDiabetes())
          {
              diabetesPoints=0;
          }
          
          
      }
      //setting diabetes points for female members
      else if(patient.getGender().equalsIgnoreCase("female"))
      {
          if(patient.isDiabetes())
          {
              diabetesPoints=4;
          }
          else if(!patient.isDiabetes())
          {
              diabetesPoints=0;
          }
          
      }
      
      
      
      
      
      
      
      return diabetesPoints;
      
      
  }  
  
  
  public static int SmokerPoints(Patient patient)
  {
      int smokerPoints=0;
      
      //setting smoker Points for male members
      if(patient.getGender().equalsIgnoreCase("male"))
      {
          if(patient.isSmoking())
          {
              smokerPoints=2;
          }
          else
          {
              smokerPoints=0;
          }
      }
      //setting smoker Points for female members
      if(patient.getGender().equalsIgnoreCase("female"))
      {
          if(patient.isSmoking())
          {
              smokerPoints=2;
          }
          else
          {
              smokerPoints=0;
          }
          
          
          
          
      }
      
      
      
      
      
      
      return smokerPoints;
      
      
  }
  //calculating total LDL points through step(1-6)
  public static int calculateTotalLDLPoints(Patient patient){
      
      int totalLDLPoints=0;
      int diabetiesLDLPoints=DiabetesPoints(patient);
      int smokerLDLPoints=SmokerPoints(patient);
      int bloodPressureLDLPoints=BloodPressurePointsStep4(patient);
      
      
      
      
      
      
      return totalLDLPoints;
  }
  //calculating total Chol points through step(1-6)
  public static int calculateTotalCholPoints(Patient patient)
  {
      int totalCholPoints=0;
  int diabetiesCholPoints=DiabetesPoints(patient);
      int smokerCholPoints=SmokerPoints(patient);
      int bloodPressureCholPoints=BloodPressurePointsStep4(patient);
      
      
  
  
  
  return totalCholPoints;
  
  }
  //calculating CHD risk from LDLpoints
  public static int determineLDLCHDrisk(Patient patient){
      int LDLCHDrisk=0;
      int totalLDLPoints=calculateTotalLDLPoints(patient);
      //calculating  CHD risk using LDL for male members
      if(patient.getGender().equalsIgnoreCase("male")){
          
      
      if(totalLDLPoints<=(-3))
      {
          LDLCHDrisk=1;
      }
      else if(totalLDLPoints==(-2))
      {
          LDLCHDrisk=2;
      }
      else if(totalLDLPoints==(-1))
      {
          LDLCHDrisk=2;
      }
      else if(totalLDLPoints==0)
      {
          LDLCHDrisk=3;
      }
      else if(totalLDLPoints==1)
      {
          LDLCHDrisk=4;
      }
      else if(totalLDLPoints==2)
      {
          LDLCHDrisk=4;
      }
      else if(totalLDLPoints==3)
      {
          LDLCHDrisk=6;
      }
      else if(totalLDLPoints==4)
      {
          LDLCHDrisk=7;
      }
      else if(totalLDLPoints==5)
      {
         LDLCHDrisk=9;
      }
      else if(totalLDLPoints==6)
      {
          LDLCHDrisk=11;
      }
      else if(totalLDLPoints==7)
      {
          LDLCHDrisk=14;
      }
      else if(totalLDLPoints==8)
      {
          LDLCHDrisk=18;
      }
      else if(totalLDLPoints==9)
      {
          LDLCHDrisk=22;
      }
      else if(totalLDLPoints==10)
      {
          LDLCHDrisk=27;
      }
      else if(totalLDLPoints==11)
      {
          LDLCHDrisk=33;
      }
      else if(totalLDLPoints==12)
      {
          LDLCHDrisk=40;
      }
      else if(totalLDLPoints==13)
      {
          LDLCHDrisk=47;
      }
      else if(totalLDLPoints>=14)
      {
          LDLCHDrisk=56;
      }
      
      }
      //calculating CHD risk for female members
      if(patient.getGender().equalsIgnoreCase("female"))
      {
          if(totalLDLPoints<=(-2))
      {
          LDLCHDrisk=1;
      }
      else if(totalLDLPoints==(-1))
      {
          LDLCHDrisk=2;
      }
      else if(totalLDLPoints==0)
      {
          LDLCHDrisk=2;
      }
      else if(totalLDLPoints==1)
      {
          LDLCHDrisk=2;
      }
      else if(totalLDLPoints==2)
      {
          LDLCHDrisk=3;
      }
      else if(totalLDLPoints==3)
      {
          LDLCHDrisk=3;
      }
      else if(totalLDLPoints==4)
      {
          LDLCHDrisk=4;
      }
      else if(totalLDLPoints==5)
      {
         LDLCHDrisk=5;
      }
      else if(totalLDLPoints==6)
      {
          LDLCHDrisk=6;
      }
      else if(totalLDLPoints==7)
      {
          LDLCHDrisk=7;
      }
      else if(totalLDLPoints==8)
      {
          LDLCHDrisk=8;
      }
      else if(totalLDLPoints==9)
      {
          LDLCHDrisk=9;
      }
      else if(totalLDLPoints==10)
      {
          LDLCHDrisk=11;
      }
      else if(totalLDLPoints==11)
      {
          LDLCHDrisk=13;
      }
      else if(totalLDLPoints==12)
      {
          LDLCHDrisk=15;
      }
      else if(totalLDLPoints==13)
      {
          LDLCHDrisk=17;
      }
      else if(totalLDLPoints==14)
      {
          LDLCHDrisk=20;
      }
      else if(totalLDLPoints==15)
      {
          LDLCHDrisk=24;
      }
      else if(totalLDLPoints==16)
      {
          LDLCHDrisk=27;
      }
      else if(totalLDLPoints>=17)
      {
          LDLCHDrisk=32;
      }
      

      
      
      }
      
      
      
      
      
      
      
      
      return LDLCHDrisk;
      
  }
//calculating CHD risk from Chol points
  public static int determineCholCHDrisk(Patient patient)
  {
      int CholCHDrisk=0;
      int totalCholPoints=calculateTotalCholPoints(patient);
      
      //calculating CHD risk for male members
      if(patient.getGender().equalsIgnoreCase("male"))
      {
         if(totalCholPoints==(-1))
      {
          CholCHDrisk=2;
      }
      else if(totalCholPoints==0)
      {
          CholCHDrisk=3;
      }
      else if(totalCholPoints==1)
      {
          CholCHDrisk=3;
      }
      else if(totalCholPoints==2)
      {
          CholCHDrisk=4;
      }
      else if(totalCholPoints==3)
      {
          CholCHDrisk=5;
      }
      else if(totalCholPoints==4)
      {
          CholCHDrisk=7;
      }
      else if(totalCholPoints==5)
      {
         CholCHDrisk=8;
      }
      else if(totalCholPoints==6)
      {
          CholCHDrisk=10;
      }
      else if(totalCholPoints==7)
      {
          CholCHDrisk=13;
      }
      else if(totalCholPoints==8)
      {
          CholCHDrisk=16;
      }
      else if(totalCholPoints==9)
      {
          CholCHDrisk=20;
      }
      else if(totalCholPoints==10)
      {
          CholCHDrisk=25;
      }
      else if(totalCholPoints==11)
      {
          CholCHDrisk=31;
      }
      else if(totalCholPoints==12)
      {
          CholCHDrisk=37;
      }
      else if(totalCholPoints==13)
      {
          CholCHDrisk=45;
      }
      else if(totalCholPoints>=14)
      {
          CholCHDrisk=53;
      }
      }
      //calculating CHD risk for female members
      else if(patient.getGender().equalsIgnoreCase("female"))
      {
          
           if(totalCholPoints<=(-2))
      {
          CholCHDrisk=1;
      }
      else if(totalCholPoints==(-1))
      {
          CholCHDrisk=2;
      }
      else if(totalCholPoints==0)
      {
          CholCHDrisk=2;
      }
      else if(totalCholPoints==1)
      {
          CholCHDrisk=2;
      }
      else if(totalCholPoints==2)
      {
          CholCHDrisk=3;
      }
      else if(totalCholPoints==3)
      {
          CholCHDrisk=3;
      }
      else if(totalCholPoints==4)
      {
          CholCHDrisk=4;
      }
      else if(totalCholPoints==5)
      {
         CholCHDrisk=4;
      }
      else if(totalCholPoints==6)
      {
          CholCHDrisk=5;
      }
      else if(totalCholPoints==7)
      {
          CholCHDrisk=6;
      }
      else if(totalCholPoints==8)
      {
          CholCHDrisk=7;
      }
      else if(totalCholPoints==9)
      {
          CholCHDrisk=8;
      }
      else if(totalCholPoints==10)
      {
          CholCHDrisk=10;
      }
      else if(totalCholPoints==11)
      {
          CholCHDrisk=11;
      }
      else if(totalCholPoints==12)
      {
          CholCHDrisk=13;
      }
      else if(totalCholPoints==13)
      {
          CholCHDrisk=15;
      }
      else if(totalCholPoints==14)
      {
          CholCHDrisk=18;
      }
      else if(totalCholPoints==15)
      {
          CholCHDrisk=20;
      }
      else if(totalCholPoints==16)
      {
          CholCHDrisk=24;
      }
      else if(totalCholPoints>=17)
      {
          CholCHDrisk=27;
      }
      }
      
      
      
      
      
      
      
      
      return CholCHDrisk;
      
  }
    
    
}
