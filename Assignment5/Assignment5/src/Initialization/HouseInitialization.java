/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Initialization;
import City.Business.City;
import City.Business.House;
import City.Business.HouseDirectory;
import java.util.ArrayList;

/**
 *
 * @author Guest
 */
public class HouseInitialization {
    
    public static HouseDirectory brooklineHouseDirectory,woburnHouseDirectory,marbleheadHouseDirectory,newtonHouseDirectory,walthamHouseDirectory,
            winchesterHouseDirectory,belmontHouseDirectory,eastleighHouseDirectory,callahanHouseDirectory,newEnglandHouseDirectory;
    static ArrayList<House> houseList;
    public static HouseDirectory intializeBrooklineHouseDirectory(){
        brooklineHouseDirectory=new HouseDirectory();
       houseList=new ArrayList<House>();
       House house;
        //house 1 in brookline community
        house=new House();
        house.setHouseNumber(1);
        house.setHouseaddress("123 street,Brrokline");
        house.setFamilyDirectory(Initialization.FamilyInitialization.initializeBrooklineHouse1(house.getHouseaddress()));
        houseList.add(house);
        
         //house 2 in brookline community
        house=new House();
        house.setHouseNumber(2);
        house.setHouseaddress("123 street,Brrokline");
        house.setFamilyDirectory(Initialization.FamilyInitialization.initializeBrooklineHouse2(house.houseaddress));
        houseList.add(house);
       
        //house 2 in brookline community
        house=new House();
        house.setHouseNumber(3);
        house.setHouseaddress("123 street,Brrokline");
        house.setFamilyDirectory(Initialization.FamilyInitialization.initializeBrooklineHouse3(house.houseaddress));
        houseList.add(house);
       
        //house 2 in brookline community
        house=new House();
        house.setHouseNumber(4);
        house.setHouseaddress("123 street,Brrokline");
        house.setFamilyDirectory(Initialization.FamilyInitialization.initializeBrooklineHouse4(house.houseaddress));
        houseList.add(house);
       
        //house 2 in brookline community
        house=new House();
        house.setHouseNumber(5);
        house.setHouseaddress("123 street,Brrokline");
        house.setFamilyDirectory(Initialization.FamilyInitialization.initializeBrooklineHouse5(house.houseaddress));
        houseList.add(house);
       
        
        
        brooklineHouseDirectory.setHouseList(houseList);
        return brooklineHouseDirectory;
    }
    public static HouseDirectory intializeWoburnHouseDirectory(){
        woburnHouseDirectory=new HouseDirectory();
       houseList=new ArrayList<House>();
       House house;
        //house 1 in Woburn community
        house=new House();
        house.setHouseNumber(1);
        house.setHouseaddress("123 street,Woburn ");
        house.setFamilyDirectory(Initialization.FamilyInitialization.initializeWoburnHouse1(house.houseaddress));
        houseList.add(house);
        
         //house 2 in Woburn community
        house=new House();
        house.setHouseNumber(2);
        house.setHouseaddress("123 street,Woburn");
        house.setFamilyDirectory(Initialization.FamilyInitialization.initializeWoburnHouse2(house.houseaddress));
        houseList.add(house);
       
        //house 2 in Woburn community
        house=new House();
        house.setHouseNumber(3);
        house.setHouseaddress("123 street,Woburn");
        house.setFamilyDirectory(Initialization.FamilyInitialization.initializeWoburnHouse3(house.houseaddress));
        houseList.add(house);
       
        //house 2 in Woburn community
        house=new House();
        house.setHouseNumber(4);
        house.setHouseaddress("123 street,Woburn");
        house.setFamilyDirectory(Initialization.FamilyInitialization.initializeWoburnHouse4(house.houseaddress));
        houseList.add(house);
       
        //house 2 in Woburn community
        house=new House();
        house.setHouseNumber(5);
        house.setHouseaddress("123 street,Woburn");
        house.setFamilyDirectory(Initialization.FamilyInitialization.initializeWoburnHouse5(house.houseaddress));
        houseList.add(house);
       
        
        
        woburnHouseDirectory.setHouseList(houseList);
        return woburnHouseDirectory;
    }
     public static HouseDirectory intializeMarbleheadHouseDirectory(){
        marbleheadHouseDirectory=new HouseDirectory();
       houseList=new ArrayList<House>();
       House house;
       int i;
        //house 1 in Woburn community
        for(i=1;i<6;i++){
        house=new House();
        house.setHouseNumber(1);
        house.setHouseaddress("123 street,Marblehead");
        house.setFamilyDirectory(Initialization.FamilyInitialization.initializeMarbleheadHouse(house.houseaddress));
        houseList.add(house);
        }
        marbleheadHouseDirectory.setHouseList(houseList);
        return  marbleheadHouseDirectory;
     }
      public static HouseDirectory intializeNewtonHouseDirectory(){
        newtonHouseDirectory=new HouseDirectory();
       houseList=new ArrayList<House>();
       House house;
       int i;
        //house 1 in Woburn community
        for(i=1;i<6;i++){
        house=new House();
        house.setHouseNumber(1);
        house.setHouseaddress("123 street, Newton");
        house.setFamilyDirectory(Initialization.FamilyInitialization.initializeNewtonHouse(house.houseaddress));
        houseList.add(house);
        }
        newtonHouseDirectory.setHouseList(houseList);
        return newtonHouseDirectory;
     }
       public static HouseDirectory intializeWalthamHouseDirectory(){
        walthamHouseDirectory=new HouseDirectory();
       houseList=new ArrayList<House>();
       House house;
       int i;
        //house 1 in Woburn community
        for(i=1;i<6;i++){
        house=new House();
        house.setHouseNumber(1);
        house.setHouseaddress("123 street,Waltham");
        house.setFamilyDirectory(Initialization.FamilyInitialization.initializeWalthamHouse(house.houseaddress));
        houseList.add(house);
        }
        walthamHouseDirectory.setHouseList(houseList);
        return walthamHouseDirectory;
     }
       public static HouseDirectory intializeWinchesterHouseDirectory(){
        winchesterHouseDirectory=new HouseDirectory();
       houseList=new ArrayList<House>();
       House house;
       int i;
        //house 1 in Woburn community
        for(i=1;i<6;i++){
        house=new House();
        house.setHouseNumber(1);
        house.setHouseaddress("123 street,Winchester");
        house.setFamilyDirectory(Initialization.FamilyInitialization.initializeWinchesterHouse(house.houseaddress));
        houseList.add(house);
        }
        winchesterHouseDirectory.setHouseList(houseList);
        return winchesterHouseDirectory;
     }
       public static HouseDirectory intializeBelmontHouseDirectory(){
        belmontHouseDirectory=new HouseDirectory();
       houseList=new ArrayList<House>();
       House house;
       int i;
        //house 1 in Woburn community
        for(i=1;i<6;i++){
        house=new House();
        house.setHouseNumber(1);
        house.setHouseaddress("123 street,Belmont");
        house.setFamilyDirectory(Initialization.FamilyInitialization.initializeBelmontHouse(house.houseaddress));
        houseList.add(house);
        }
        belmontHouseDirectory.setHouseList(houseList);
        return belmontHouseDirectory;
     }
       public static HouseDirectory intializeEastleighHouseDirectory(){
        eastleighHouseDirectory=new HouseDirectory();
       houseList=new ArrayList<House>();
       House house;
       int i;
        //house 1 in Woburn community
        for(i=1;i<6;i++){
        house=new House();
        house.setHouseNumber(1);
        house.setHouseaddress("123 street,Eastleigh");
        house.setFamilyDirectory(Initialization.FamilyInitialization.initializeEastleighHouse(house.houseaddress));
        houseList.add(house);
        }
        eastleighHouseDirectory.setHouseList(houseList);
        return eastleighHouseDirectory;
     }
       public static HouseDirectory intializeCallahanHouseDirectory(){
        callahanHouseDirectory=new HouseDirectory();
       houseList=new ArrayList<House>();
       House house;
       int i;
        //house 1 in Woburn community
        for(i=1;i<6;i++){
        house=new House();
        house.setHouseNumber(1);
        house.setHouseaddress("123 street,Callahan");
        house.setFamilyDirectory(Initialization.FamilyInitialization.initializeCallahanHouse(house.houseaddress));
        houseList.add(house);
        }
        callahanHouseDirectory.setHouseList(houseList);
        return callahanHouseDirectory;
     }
       public static HouseDirectory intializeNewEnglandHouseDirectory(){
        newEnglandHouseDirectory=new HouseDirectory();
       houseList=new ArrayList<House>();
       House house;
       int i;
        //house 1 in Woburn community
        for(i=1;i<6;i++){
        house=new House();
        house.setHouseNumber(1);
        house.setHouseaddress("123 street, New England");
        house.setFamilyDirectory(Initialization.FamilyInitialization.initializeNewEnglandHouse(house.houseaddress));
        houseList.add(house);
        }
        newEnglandHouseDirectory.setHouseList(houseList);
        return newEnglandHouseDirectory;
     }



    
}
