/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Initialization;
import City.Business.City;
import City.Business.Family;
import City.Business.FamilyDirectory;
import City.Business.Patient;
import City.Business.Person;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Guest
 */
public class FamilyInitialization {
    
    public static FamilyDirectory familyDirectory;
    static ArrayList<Family> familyList;
    static ArrayList<String> existingFamily=new ArrayList<String>();
    public static FamilyDirectory initializeBrooklineHouse1(String houseaddress){
        familyDirectory=new FamilyDirectory();
        Family family;
        familyList=new ArrayList<Family>();
        //family 1 in Brookline house1
        family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#1,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),8));
        initPerfectFamily(family);
        familyList.add(family);
        
        
        //family 2 in Brrokline house1
        family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#2,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),9));
        initPerfectFamily(family);
        familyList.add(family);
        
        //family3 in Brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#3,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),8));
        initPerfectFamily(family);
        familyList.add(family);
       
        //family4 in brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#4,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),7));
        initPerfectFamily(family);
        familyList.add(family);
       
        //family5 in brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#5,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),6));
        initPerfectFamily(family);
        familyList.add(family);
       
        familyDirectory.setFamilyList(familyList);
        
        return familyDirectory;
    }
     public static FamilyDirectory initializeBrooklineHouse2(String houseaddress){
        familyDirectory=new FamilyDirectory();
        Family family;
        familyList=new ArrayList<Family>();
        //family 1 in Brookline house1
        family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#1,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),5));
        initPerfectFamily(family);
        familyList.add(family);
        
        
        //family 2 in Brrokline house1
        family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#2,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),6));
        initPerfectFamily(family);
        familyList.add(family);
        
        //family3 in Brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#3,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),7));
        initPerfectFamily(family);
        familyList.add(family);
       
        //family4 in brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#4,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),7));
        initPerfectFamily(family);
        familyList.add(family);
       
        //family5 in brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#5,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),5));
        initPerfectFamily(family);
        familyList.add(family);
       
        familyDirectory.setFamilyList(familyList);
        
        return familyDirectory;
    }
     public static FamilyDirectory initializeBrooklineHouse3(String houseaddress){
        familyDirectory=new FamilyDirectory();
        Family family;
        familyList=new ArrayList<Family>();
        //family 1 in Brookline house1
        family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#1,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),6));
        initPerfectFamily(family);
        familyList.add(family);
        
        
        //family 2 in Brrokline house1
        family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#2,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),7));
        initPerfectFamily(family);
        familyList.add(family);
        
        //family3 in Brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#3,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),9));
        initPerfectFamily(family);
        familyList.add(family);
       
        //family4 in brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#4,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),8));
        initPerfectFamily(family);
        familyList.add(family);
       
        //family5 in brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#5,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),9));
        initPerfectFamily(family);
        familyList.add(family);
       
        familyDirectory.setFamilyList(familyList);
        
        return familyDirectory;
    }
      public static FamilyDirectory initializeBrooklineHouse4(String houseaddress){
        familyDirectory=new FamilyDirectory();
        Family family;
        familyList=new ArrayList<Family>();
        //family 1 in Brookline house1
        family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#1,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),7));
        initPerfectFamily(family);
        familyList.add(family);
        
        
        //family 2 in Brrokline house1
        family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#2,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),7));
        initPerfectFamily(family);
        familyList.add(family);
        
        //family3 in Brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#3,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),6));
        initPerfectFamily(family);
        familyList.add(family);
       
        //family4 in brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#4,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),9));
        initPerfectFamily(family);
        familyList.add(family);
       
        //family5 in brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#5,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),10));
        initPerfectFamily(family);
        familyList.add(family);
       
        familyDirectory.setFamilyList(familyList);
        
        return familyDirectory;
    }
       public static FamilyDirectory initializeBrooklineHouse5(String houseaddress){
        familyDirectory=new FamilyDirectory();
        Family family;
        familyList=new ArrayList<Family>();
        //family 1 in Brookline house1
        family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#1,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),7));
        initPerfectFamily(family);
        familyList.add(family);
        
        
        
        //family 2 in Brrokline house1
        family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#2,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),8));
        initPerfectFamily(family);
        familyList.add(family);
        
        //family3 in Brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#3,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),9));
        initPerfectFamily(family);
        familyList.add(family);
       
        //family4 in brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#4,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),8));
        initPerfectFamily(family);
        familyList.add(family);
       
        //family5 in brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#5,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),7));
        initPerfectFamily(family);
        familyList.add(family);
       
        familyDirectory.setFamilyList(familyList);
        
        return familyDirectory;
    }
        public static FamilyDirectory initializeWoburnHouse1(String houseaddress){
        familyDirectory=new FamilyDirectory();
        Family family;
        familyList=new ArrayList<Family>();
        //family 1 in Brookline house1
        family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#1,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),8));
        initPerfectFamily(family);
        familyList.add(family);
        
        
        //family 2 in Brrokline house1
        family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#2,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),7));
        initPerfectFamily(family);
        familyList.add(family);
        
        //family3 in Brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#3,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),8));
        initPerfectFamily(family);
        familyList.add(family);
       
        //family4 in brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#4,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),7));
        initPerfectFamily(family);
        familyList.add(family);
       
        //family5 in brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#5,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),8));
        initPerfectFamily(family);
        familyList.add(family);
       
        familyDirectory.setFamilyList(familyList);
        
        return familyDirectory;
    }
         public static FamilyDirectory initializeWoburnHouse2(String houseaddress){
        familyDirectory=new FamilyDirectory();
        Family family;
        familyList=new ArrayList<Family>();
        //family 1 in Brookline house1
        family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#1,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),7));
        initPerfectFamily(family);
        familyList.add(family);
        
        
        //family 2 in Brrokline house1
        family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#2,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),9));
        initPerfectFamily(family);
        familyList.add(family);
        
        //family3 in Brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#3,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),8));
        initPerfectFamily(family);
        familyList.add(family);
       
        //family4 in brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#4,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),8));
        initPerfectFamily(family);
        familyList.add(family);
       
        //family5 in brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#5,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),9));
        initPerfectFamily(family);
        familyList.add(family);
       
        familyDirectory.setFamilyList(familyList);
        
        return familyDirectory;
    }
          public static FamilyDirectory initializeWoburnHouse3(String houseaddress){
        familyDirectory=new FamilyDirectory();
        Family family;
        familyList=new ArrayList<Family>();
        //family 1 in Brookline house1
        family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#1,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),8));
        initPerfectFamily(family);
        familyList.add(family);
        
        
        //family 2 in Brrokline house1
        family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#2,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),7));
        initPerfectFamily(family);
        familyList.add(family);
        
        //family3 in Brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#3,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),7));
        initPerfectFamily(family);
        familyList.add(family);
       
        //family4 in brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#4,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),8));
        initPerfectFamily(family);
        familyList.add(family);
       
        //family5 in brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#5,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),9));
        initPerfectFamily(family);
        familyList.add(family);
       
        familyDirectory.setFamilyList(familyList);
        
        return familyDirectory;
    }
           public static FamilyDirectory initializeWoburnHouse4(String houseaddress){
        familyDirectory=new FamilyDirectory();
        Family family;
        familyList=new ArrayList<Family>();
        //family 1 in Brookline house1
        family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#1,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),10));
        initPerfectFamily(family);
        familyList.add(family);
        
        
        //family 2 in Brrokline house1
        family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#2,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),7));
        initPerfectFamily(family);
        familyList.add(family);
        
        //family3 in Brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#3,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),8));
        initPerfectFamily(family);
        familyList.add(family);
       
        //family4 in brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#4,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),8));
        initPerfectFamily(family);
        familyList.add(family);
       
        //family5 in brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#5,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),9));
        initPerfectFamily(family);
        familyList.add(family);
       
        familyDirectory.setFamilyList(familyList);
        
        return familyDirectory;
    }
            public static FamilyDirectory initializeWoburnHouse5(String houseaddress){
        familyDirectory=new FamilyDirectory();
        Family family;
        familyList=new ArrayList<Family>();
        //family 1 in Brookline house1
        family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#1,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),9));
        initPerfectFamily(family);
        familyList.add(family);
        
        
        //family 2 in Brrokline house1
        family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#2,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),8));
        initPerfectFamily(family);
        familyList.add(family);
        
        //family3 in Brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#3,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),8));
        initPerfectFamily(family);
        familyList.add(family);
       
        //family4 in brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#4,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),9));
        initPerfectFamily(family);
        familyList.add(family);
       
        //family5 in brookline house1
         family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#5,"+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),8));
        initPerfectFamily(family);
        familyList.add(family);
       
        familyDirectory.setFamilyList(familyList);
        
        return familyDirectory;
    }
    public static FamilyDirectory initializeMarbleheadHouse(String houseaddress){
        familyDirectory=new FamilyDirectory();
        familyList =new ArrayList<Family>();
        Family family;
        int i;
        for(i=1;i<6;i++)
        {
        
        family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#"+i+","+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),8));
        initPerfectFamily(family);
        familyList.add(family);
      
        }
        familyDirectory.setFamilyList(familyList);
        return familyDirectory;
    }
     public static FamilyDirectory initializeNewtonHouse(String houseaddress){
        familyDirectory=new FamilyDirectory();
        familyList =new ArrayList<Family>();
        Family family;
        int i;
        for(i=1;i<6;i++)
        {
        
        family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#"+i+","+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),8));
        initPerfectFamily(family);
        familyList.add(family);
      
        }
        familyDirectory.setFamilyList(familyList);
        return familyDirectory;
    }
     public static FamilyDirectory initializeWalthamHouse(String houseaddress){
        familyDirectory=new FamilyDirectory();
        familyList =new ArrayList<Family>();
        Family family;
        int i;
        for(i=1;i<6;i++)
        {
        
        family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#"+i+","+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),9));
        initPerfectFamily(family);
        familyList.add(family);
      
        }
        familyDirectory.setFamilyList(familyList);
        return familyDirectory;
    }
     public static FamilyDirectory initializeWinchesterHouse(String houseaddress){
        familyDirectory=new FamilyDirectory();
        familyList =new ArrayList<Family>();
        Family family;
        int i;
        for(i=1;i<6;i++)
        {
        
        family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#"+i+","+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),7));
        initPerfectFamily(family);
        familyList.add(family);
      
        }
        familyDirectory.setFamilyList(familyList);
        return familyDirectory;
    }
     public static FamilyDirectory initializeBelmontHouse(String houseaddress){
        familyDirectory=new FamilyDirectory();
        familyList =new ArrayList<Family>();
        Family family;
        int i;
        for(i=1;i<6;i++)
        {
        
        family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#"+i+","+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),6));
        initPerfectFamily(family);
        familyList.add(family);
      
        }
        familyDirectory.setFamilyList(familyList);
        return familyDirectory;
    }
      public static FamilyDirectory initializeEastleighHouse(String houseaddress){
        familyDirectory=new FamilyDirectory();
        familyList =new ArrayList<Family>();
        Family family;
        int i;
        for(i=1;i<6;i++)
        {
        
        family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#"+i+","+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),9));
        initPerfectFamily(family);
        familyList.add(family);
      
        }
        familyDirectory.setFamilyList(familyList);
        return familyDirectory;
    }
       public static FamilyDirectory initializeCallahanHouse(String houseaddress){
        familyDirectory=new FamilyDirectory();
        familyList =new ArrayList<Family>();
        Family family;
        int i;
        for(i=1;i<6;i++)
        {
        
        family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#"+i+","+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),8));
        initPerfectFamily(family);
        familyList.add(family);
      
        }
        familyDirectory.setFamilyList(familyList);
        return familyDirectory;
    }
    public static FamilyDirectory initializeNewEnglandHouse(String houseaddress){
        familyDirectory=new FamilyDirectory();
        familyList =new ArrayList<Family>();
        Family family;
        int i;
        for(i=1;i<6;i++)
        {
        
        family=new Family();
        family.setFamilyName(initFamilyName());
        family.setFamilyAddress("#"+i+","+houseaddress);
        family.setPatientDirectory(Initialization.PatientInitialization.initPatientDirectory(family.getFamilyName(),9));
            initPerfectFamily(family);
        familyList.add(family);
      
        }
        familyDirectory.setFamilyList(familyList);
        return familyDirectory;
    }
    public static String initFamilyName()
    {
        Random rand = new Random();

    
        int i;
        String familyName;
         String[] array = {"Rhyne","Rhyno","Rhyner","Rhyther","RhytherSon","Ryser","Ryon",
             "Rynn","Rynne","Rys","Rysavy","Rynne","Rynders","Rymen","Ryley","Ryll","Rynard",
         "Sadler","Sadlier","Sade","Sadd","Sadiq","Sadlon","Sadberry","Sacra","Sacre","Sacks",
         "Sacks","Saxena","Sackmann","Sackrider","Sachar","Sachdeva","Sachdev","Sacha","Sachs",
         "Sackett","Sacher","Sabot","Sabo","Sablich","Saboe","Sacca","Sabir","Sabino","Sabine",
         "Sabiston","Sable","Sabharwal","Sabet","Sabers","Saber","Sabia","Sabbagh","Sabb","Sabato",
         "Smith","Johns","James","Willson","William","Brown","Miller","Martin","Moor","White","Rose",
         "Hiller","Thomson","Allen","Green","Ereen","Wreen","kreen","preen","dreen","mreen","Adams",
         "Baker","saker","maker","paker","daker","laker","Maker",
         "walker","awalker","bwalker","cwalker","dwalker","ewalker","fwalker","gwalker","hwalker","iwalker","kwalker","jwalker",
         "lwalker","mwalker","nwalker","owalker","qwalker","rwalker","swalker","twalker","uwalker","vwalker","xwalker","ywalker",
         "anelson","bnelson","cnelson","dnelson","enelson","fnelson","gnelson",
         "hnelson","inelson","jnelson","knelson","lnelson","mnelson","nnelson","onelson","pnelson","qnelson","rnelson",
         "snelson","tnelson","unelson","vnelson","ilnelson","jlnelson","klnelson","llnelson","mlnelson","nlnelson",
         "olnelson","plnelson","heeeall","haell","ehaleel","haell","ehaleel","haeeeell","haell","hallee","halle",
         "ahall","qhall","whall","hwall","heall","hqqall","halwl","hawwll","hwalwl","hiall","hpall","harll","hsall",
         "halfl","hddall","hffall","hfall","hffffall","hsall","hfallf","hfallddd","fhafll","fhafllf","fhfaflfl","fhfall","ffffhall",
         "halvvvl","halvvl","hvvall","havll","halvl","hallv",
         "Clintonq","Clintonw","Clintone","Clintonr","Clintont","Clintony","Clintonu",
         "Clintoni","Clintono","Clintonp","Clintona","Clintons","Clintond","Clintonf",
         "Clintong","Clintonh","Clintonj","Clintonk","Clintonl","Clintonz","Clintonx",
         "Clintonc","Clintonv","Clintonb","Clintonn","Clintonm","Clintonqq","Clintonww",
         "Clintonee","Clintonrr","Clintontt","Clintonyy","Clintonuu","Clintonii","Clintonoo","Clintonpp",
         "Clintonaa","Clintonss","Clintondd","Clintonfff","Clintongg","Clintonhh","Clintonjj","Clintonk","Clintonll",
         "Clintonzzz","Clintonxxx","Clintonccc","Clintonvvv","Clintonbb","Clintonnn","Clintonmm","Cmlinton","Cnlinton","Cblinton"
         ,"Celinton","Cdlinton","Clintosn","Clintaon","Clinzton","Clinxton","Clintcon","Clinvton","Clibnton","Cbblinton",
         "mqac","mwac","wmac","emac","rmac","mact","macy","marc","mdac","mfvac","mghafc","mach","mxcxac","masc","macw",
         "marwc","sdmac","mfxzac","mvcac",
         "macww","maceezz","mamhkuc","maiolc","miolac","maciol","mpolac","macpolb","mapolhc","macpoli","mailcc","mallic","makjc","mabhdsc",
         "macj","mack","macsssa","macq","vmac","macqw","macwe","macdds","masc","masdc","mfacv","vmacmac","mssrac","meac","maopc","vmaccv",
         "mach","mace","macddd","macghf","macgd","macg","macd","macs","maca","macz","macx","macc","macv","macb","macn","nmacm","macm",
         "Pauli","paulo","paulik","paulmn","paulm","paulh","pauop","paugh","paulir","pamer","palle","palhar","pulser",
         "peri","patel","puli","pilla","picahi",
         "pichai","pulhor","pulhora","pulihora","paulik","paulimc",
         "manchi","manchu","manchan","manam","manik","malhothra","malhothran","mastani",
         "masakali","Martin","luther","martinp","mankin","manister","Raymond","harry","scott","bright","kim","jill",
         "hillp","brill","dill","mann",
         "Rhyneq","Rhynow","Rhynere","Rhytherr","RhytherSont","Rysery","Ryony",
             "Rynnq","Rynneq","Rysq","Rysavqy","Rqynne","Rynqders","Rqymen","Ryqley","Rylql","Rynaqrd",
         "Sadler","Sadlier","Sade","Sadd","Sadiq","Sadlon","Sadberry","Sacra","Sacre","Sacks",
         "Saqcks","Saqxena","Sackqmann","Sackriderq","Sachqar","Sachdevnna","Sachdenv","Snacha","Sacnhs",
         "Sanckett","Sachern","Sabotn","Sabon","Sablichn","Saboen","Saccan","Sabinr","Snabino","Sanbine",
         "Sanbiston","Sanble","Sanbharwal","Sabnet","Snabers","Snaber","Snabia","Sanbbagh","Sanbb","Sanbato",
         "Smithn","Johnsn","Jamesn","Willsonn","Williamn","Brownn","Millern","Martinn","Moorn","Whiten","Rosen",
         "Hillern","Thomsonn","Allenn","Greenn","Ereenn","Wreenn","knreen","prenen","drenen","mrenen","Adamns",
         "Bakenr","nsaker","manker","pakern","dakern","lanker","Manker",
         "walkenr","awalkner","bwalkenr","cwalkner","dwalkenr","ewalkenr","fwanlker","gwalkern","hwalkern","iwalkern",
         "kwalkern","jwalkern",
         "lwalkern","mwalkern","nwalkern","owalkern","qwalkern","rwalkenr","swanlker","twanlker","uwanlker","vwanlker",
         "xwalskern","ywalksern",
         "anelsosnn","bnelsossssnn","cnelsosnn","dnelssonn","enelsonn","fnelsonsn","gneslsonn",
         "hnelsosnn","inselsonn","jnelsonsn","knselsonn","lnelsosnnn","mneslsonn","nnelsnon","onelsnon","pnelson","qnelsonn","nrnelson",
         "snelnsson","tnelssnon","unenlsson","vnesnlson","ilneslsonnnnnn","jlnenlson","klnelnson","llnnelson","mlnnelson","nlnnelson",
         "oalnelsopn","planelsopn","heeaefall","haaedll","ehaaleesl","haesall","aehalseel","haeseeaell","haselal",
         "halsalee","hallsae",
         "ahpaall","qhaalpl","whpall","hwalppl","healpla","hqqalpl","halwpl","hawwlpl","hwalpwl","hiapll","hpalpl","harllp","hsallp",
        
         
         };
         /*String[] array = {"Rhyne","Rhyno","Rhyner","Rhyther","RhytherSon","Ryser","Ryon",
             "Rynn","Rynne","Rys","Rysavy","Rynne","Rynders","Rymen","Ryley","Ryll","Rynard",
         "Sadler","Sadlier","Sade","Sadd","Sadiq","Sadlon","Sadberry","Sacra","Sacre","Sacks",
         "Sacks","Saxena","Sackmann","Sackrider","Sachar","Sachdeva","Sachdev","Sacha","Sachs",
         "Sackett","Sacher","Sabot","Sabo","Sablich","Saboe","Sacca","Sabir","Sabino","Sabine",
         "Sabiston","Sable","Sabharwal","Sabet","Sabers","Saber","Sabia","Sabbagh","Sabb","Sabato"};*/
         boolean alreadyexists=false;
         familyName=null;
        
         
            for(i=0;i<(array.length);i++)
            {
                    
                        familyName = array[rand.nextInt(array.length)];
                        if(!(existingFamily.contains(familyName)))
                        {
                
                            existingFamily.add(familyName);
                            alreadyexists=true;
            
                            return familyName;
                        }
                        else{
                                
                                alreadyexists=false;
                                
                        }
                    
         
            }
         
        
         return familyName;
    }
    public static  void initPerfectFamily(Family family){
        ArrayList<Patient> grandparents,parents,individual,children;
        grandparents=new ArrayList<Patient>();
        parents=new ArrayList<Patient>();
        individual=new ArrayList<Patient>();
        children=new ArrayList<Patient>();
        
        for(Patient patient:family.getPatientDirectory().getPatientList())
           {
               
               
               if(patient.getAge()>=65)
               {
                 grandparents.add(patient);
               }
               if(patient.getAge()<65 && patient.getAge()>=45)
               {
                   parents.add(patient);
               }
               if(patient.getAge()<45 && patient.getAge()>=20)
               {
                   individual.add(patient);
               }
               if(patient.getAge()<20)
               {
                children.add(patient);
               }
               
           }
               family.setGrandParents(grandparents);
               family.setParents(parents);
               family.setIndividuals(individual);
               family.setChildren(children);             
              ArrayList<Patient> sonList;
              ArrayList<Patient> daughterList;
            for(Patient patient:family.getGrandParents())
            {
                daughterList=new ArrayList<Patient>();
                sonList=new ArrayList<Patient>();
                for(Patient patient1:family.getParents())
                {
                    if(patient1.getGender().equalsIgnoreCase("female"))
                    {
                       daughterList.add(patient1);
                    }
                    else if(patient1.getGender().equalsIgnoreCase("male"))
                    {
                       sonList.add(patient1);
                    }
                }
                patient.setDaughterList(daughterList);
                patient.setSonList(sonList);
            }
            for(Patient patient:family.getParents())
            {
                    daughterList=new ArrayList<Patient>();
                sonList=new ArrayList<Patient>();
                
                    for(Patient patient2:family.getIndividuals())
                    {
                        if(patient2.getGender().equalsIgnoreCase("female"))
                        {
                             daughterList.add(patient2);
                        }
                        else if(patient2.getGender().equalsIgnoreCase("male"))
                        {
                            sonList.add(patient2);
                        }
                        
                    }
                    for(Patient patient3:family.getChildren())
                    {
                        if(patient3.getGender().equalsIgnoreCase("female"))
                        {
                            daughterList.add(patient3);
                        }
                        else if(patient3.getGender().equalsIgnoreCase("male"))
                        {
                            sonList.add(patient3);
                        }
                    }
                    patient.setDaughterList(daughterList);
                patient.setSonList(sonList);
                    
                    
                }
            }
               
        
        
    
    
}
