/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Initialization;

import City.Business.VitalSigns;
import City.Business.VitalSignsDirectory;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Nidhi Mittal <mittal.n@husky.neu.edu>
 */
public class VitalSignInitialization {
    
    public static ArrayList<String> existingTimestamp;
    
    public static int randInt(int min, int max) {
    Random rand = new Random();

    // nextInt is normally exclusive of the top value,
    // so add 1 to make it inclusive
    int randomNum = rand.nextInt((max - min) + 1) + min;

    return randomNum;
}
    
    public static VitalSignsDirectory initVitalSign()
    {
        //public static ArrayList<String> existingTimestamp;
        VitalSignsDirectory vitalSignDirectory= new VitalSignsDirectory();
        Random random = new Random();
        int numberofTimestamps=0;
         String[] timestamp = {"17th October 2016 5.00pm","18th October 2016 6.00pm","19th October 2016 7.00pm","20th October 2016 8.00pm","21st October 2016 8.00pm","22nd October 2016 9.00pm"};
        existingTimestamp = new ArrayList<String>();
        
        while(numberofTimestamps<6)
        {
            //vtEntry.setWeight(randInt(22,200));
            //vtEntry.setTimestamp();
           
            
          
                String check = timestamp[random.nextInt(timestamp.length)];
                
                if(!existingTimestamp.contains(check))
                {
                    VitalSigns vtEntry = vitalSignDirectory.addVitalSigns();
            
           
                    vtEntry.setSystolicBloodPressure(randInt(80,120));
                    vtEntry.setCholestrol(randInt(50,290));
                    vtEntry.setDiastolicBloodPressure(randInt(70,120));
                    vtEntry.setHdcCholestrol(randInt(20,70));
                    vtEntry.setLdlCholestrol(randInt(90,200));
          
            
                    vtEntry.setTimestamp(timestamp[random.nextInt(timestamp.length)]);
                    existingTimestamp.add(check);
                    numberofTimestamps++;
                    
                }
        }
            
         return vitalSignDirectory;           
                    
            
            
            
        }
        
        
        
    }

