/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Initialization;
import City.Business.City;
import City.Business.Community;
import City.Business.CommunityDirectory;
import java.util.ArrayList;

/**
 *
 * @author Guest
 */
public class CityInitialization {
    
    public static CommunityDirectory communityDirectory;
    static ArrayList<Community> communityList;
    public static CommunityDirectory initializecity()
    {
        City city=new City();
        communityDirectory=new CommunityDirectory();
        communityList= new ArrayList<Community>();
        city.setCityName("Framingham");
        city.setCityState("MA");
        communityList.add(Initialization.CommunityInitialization.initializeBrooklineCommunity());
        communityList.add(Initialization.CommunityInitialization.initializeWoburnCommunity());
        communityList.add(Initialization.CommunityInitialization.initializeMarbleheadCommunity());
        communityList.add(Initialization.CommunityInitialization.initializeNewtonCommunity());
        communityList.add(Initialization.CommunityInitialization.initializeWalthamCommunity());
        communityList.add(Initialization.CommunityInitialization.initializeWinchesterCommunity());
        communityList.add(Initialization.CommunityInitialization.initializeBelmontCommunity());
        communityList.add(Initialization.CommunityInitialization.initializeEastleighCommunity());
        communityList.add(Initialization.CommunityInitialization.initializeNewEnglandCommunity());
        communityList.add(Initialization.CommunityInitialization.initializeCallahanCommunity());
        communityDirectory.setCommunityList(communityList);
        city.setCommunityDirectory(communityDirectory);
        
        return communityDirectory;
        
    }
    
}
