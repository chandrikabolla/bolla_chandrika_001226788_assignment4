/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Initialization;
import City.Business.City;
import City.Business.Patient;
import City.Business.PatientDirectory;
import City.Business.Person;
import City.Business.PersonDirectory;
import City.Business.VitalSigns;
import City.Business.VitalSignsDirectory;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Guest
 */
public class PatientInitialization {
    static Patient patient;
    static VitalSignsDirectory vitalSignsDirectory;
    
    
   public static ArrayList<String> existing;

    
        public static int randInt(int min, int max) {

        // Usually this can be a field rather than a method variable
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    public static PatientDirectory initPatientDirectory(String lastname,int numberOfPersons) {
        PatientDirectory patientDirectory=new PatientDirectory();
        Random rand = new Random();
        existing=new ArrayList<String>();
        //There are two ways presented in here for random string generation you can use whatever you feel is right.
        // String myCharacters = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm";
        //Insert your user freindly names here
        String[] array = {"Neeraj", "Katie", "Courtney", "Kelsey", "David", "Alex", "Yi", "Zhang", "Hang", "Nan", "Yiming", "Sarah"};

String[] genderArray={"male","female"};
       
     int  generatedNumber=0;
     while(generatedNumber<=numberOfPersons)
     {
                    
         
            String name = array[rand.nextInt(array.length)];
           
                if(!existing.contains(name))
                {
                    //Personal details of patient 
                    existing.add(name);
                    Patient patient= patientDirectory.addPatient();
                    patient.setFirstName(name);
                    patient.setLastName(lastname);
                    int n = rand.nextInt(80) + 1;
                    patient.setAge(n);
            
                    patient.setGender(genderArray[rand.nextInt(genderArray.length)]);
           
            
            
                    //hospital details of patient
                    patient.setSmoking(true);
                    patient.setDiabetes(true);
                    patient.setVitalSignsDirectory(Initialization.VitalSignInitialization.initVitalSign());
                    patient.setPrefPharmacy("CVS");
                    patient.setPrimDocName("Chandrika");
                    patient.setLDLRisk(determineLDLCHDrisk(patient));
                    patient.setCholRisk(determineCholCHDrisk(patient));
                    patient.setAtRisk(ComparativeRisk(patient));
                    
                    
                    
                    
                    
                    
                    
                    
                    
       
                    //incrementing number of people in the family here
                    generatedNumber++;  
            
            }
           
         }
     
         return patientDirectory;
        
       
    } 
    
    public static int BloodPressurePointsStep4(Patient patient){
    
    int bloodPresuurePoints=0;
    int bloodPressurePointsSys=0;
    int bloodPressurePointsDia=0;
    VitalSignsDirectory vitalSignsDirectory;
    int sumDia=0;
    int sumSys=0;
    float averageDia=0;
    float averageSys=0;
    
    //for female
    if(patient.getGender().equalsIgnoreCase("female"))
    {
        
        vitalSignsDirectory=patient.getVitalSignsDirectory();
        
        //calculating average of systolic pressure
        for(VitalSigns vitalSign:vitalSignsDirectory.getVitalSignsList())
        {
            sumSys=sumSys+vitalSign.getSystolicBloodPressure();
        }
        averageSys=sumSys/6;
        
        //calculating average of diastolic pressure
         for(VitalSigns vitalSign:vitalSignsDirectory.getVitalSignsList())
        {
            sumDia=sumDia+vitalSign.getSystolicBloodPressure();
        }
        averageDia=sumDia/6;
        
        //setting points using systolic blood pressure
        if(averageSys<120)
        {
            bloodPressurePointsSys=(-3);
        }
        else if(averageSys>=120&&averageSys<=129)
        {
            bloodPressurePointsSys=0;
        }
        else if(averageSys>=130&&averageSys<=139)
        {
            bloodPressurePointsSys=0;
        }
        else if(averageSys>=140&&averageSys<=159)
        {
            bloodPressurePointsSys=2;
        }
        else if(averageSys>=160)
        {
            bloodPressurePointsSys=3;
        }
        
        //setting points using diastolic blood pressure
        if(averageDia<80)
        {
            bloodPressurePointsDia=(-3);
        }
        else if(averageDia>=80&&averageDia<=84)
        {
            bloodPressurePointsDia=0;
        }
        else if(averageDia>=85&&averageDia<=89)
        {
            bloodPressurePointsDia=0;
        }
        else if(averageDia>=90&&averageDia<=99)
        {
            bloodPressurePointsDia=2;
        }
        else if(averageDia>=100)
        {
            bloodPressurePointsDia=3;
        }
        if(bloodPressurePointsDia>bloodPressurePointsSys)
        {
            bloodPresuurePoints=bloodPressurePointsDia;
        }
        else if(bloodPressurePointsSys>bloodPressurePointsDia)
        {
            bloodPresuurePoints=bloodPressurePointsSys;
        }
        else if(bloodPressurePointsDia==bloodPressurePointsSys)
        {
            bloodPresuurePoints=bloodPressurePointsDia;
        }
       
        
        
        
    }
    
    //for male
    else if(patient.getGender().equalsIgnoreCase("male")){
        
        vitalSignsDirectory=patient.getVitalSignsDirectory();
        
        //calculating average of systolic pressure
        for(VitalSigns vitalSign:vitalSignsDirectory.getVitalSignsList())
        {
            sumSys=sumSys+vitalSign.getSystolicBloodPressure();
        }
        averageSys=sumSys/6;
        
        //calculating average of diastolic pressure
         for(VitalSigns vitalSign:vitalSignsDirectory.getVitalSignsList())
        {
            sumDia=sumDia+vitalSign.getSystolicBloodPressure();
        }
        averageDia=sumDia/6;
        
        //setting points using systolic blood pressure
        if(averageSys<120)
        {
            bloodPressurePointsSys=0;
        }
        else if(averageSys>=120&&averageSys<=129)
        {
            bloodPressurePointsSys=0;
        }
        else if(averageSys>=130&&averageSys<=139)
        {
            bloodPressurePointsSys=1;
        }
        else if(averageSys>=140&&averageSys<=159)
        {
            bloodPressurePointsSys=2;
        }
        else if(averageSys>=160)
        {
            bloodPressurePointsSys=3;
        }
        
        //setting points using diastolic blood pressure
        if(averageDia<80)
        {
            bloodPressurePointsDia=0;
        }
        else if(averageDia>=80&&averageDia<=84)
        {
            bloodPressurePointsDia=0;
        }
        else if(averageDia>=85&&averageDia<=89)
        {
            bloodPressurePointsDia=1;
        }
        else if(averageDia>=90&&averageDia<=99)
        {
            bloodPressurePointsDia=2;
        }
        else if(averageDia>=100)
        {
            bloodPressurePointsDia=3;
        }
        if(bloodPressurePointsDia>bloodPressurePointsSys)
        {
            bloodPresuurePoints=bloodPressurePointsDia;
        }
        else if(bloodPressurePointsSys>bloodPressurePointsDia)
        {
            bloodPresuurePoints=bloodPressurePointsSys;
        }
        else if(bloodPressurePointsDia==bloodPressurePointsSys)
        {
            bloodPresuurePoints=bloodPressurePointsDia;
        }
       
        
        
        
    }
    
    
    
    
    return bloodPresuurePoints;
    
}
  public static int DiabetesPoints(Patient patient){
      
      int diabetesPoints=0;
      //setting diabetes points for male members
      if(patient.getGender().equalsIgnoreCase("male"))
      {
          if(patient.isDiabetes())
          {
              diabetesPoints=2;
          }
          else if(!patient.isDiabetes())
          {
              diabetesPoints=0;
          }
          
          
      }
      //setting diabetes points for female members
      else if(patient.getGender().equalsIgnoreCase("female"))
      {
          if(patient.isDiabetes())
          {
              diabetesPoints=4;
          }
          else if(!patient.isDiabetes())
          {
              diabetesPoints=0;
          }
          
      }
      
      
      
      
      
      
      
      return diabetesPoints;
      
      
  }  
  
  
  public static int SmokerPoints(Patient patient)
  {
      int smokerPoints=0;
      
      //setting smoker Points for male members
      if(patient.getGender().equalsIgnoreCase("male"))
      {
          if(patient.isSmoking())
          {
              smokerPoints=2;
          }
          else
          {
              smokerPoints=0;
          }
      }
      //setting smoker Points for female members
      if(patient.getGender().equalsIgnoreCase("female"))
      {
          if(patient.isSmoking())
          {
              smokerPoints=2;
          }
          else
          {
              smokerPoints=0;
          }
          
          
          
          
      }
      
      
      
      
      
      
      return smokerPoints;
      
      
  }
  //calculating total LDL points through step(1-6)
  public static int calculateTotalLDLPoints(Patient patient){
      
      int totalLDLPoints=0;
      int ageLDLPoints=LDLPointsStep1(patient);
      int LDLPointsStep2=LDLPointsStep2(patient);
       int HDLLDLPointsStep3=HDLLDLPointsStep3(patient);
       int bloodPressureLDLPoints=BloodPressurePointsStep4(patient);
      int diabetiesLDLPoints=DiabetesPoints(patient);
      int smokerLDLPoints=SmokerPoints(patient);
      
     
     totalLDLPoints=ageLDLPoints+LDLPointsStep2+HDLLDLPointsStep3+bloodPressureLDLPoints+diabetiesLDLPoints+smokerLDLPoints;
      
      
      
      
      
      
      return totalLDLPoints;
  }
  //calculating total Chol points through step(1-6)
  public static int calculateTotalCholPoints(Patient patient)
  {
      int totalCholPoints=0;
      int ageCholPoints=CholPointsStep1(patient);
       int CholPointsStep2=CholPointsStep2(patient);
        int HDLCholPointsStep3=CholPointsStep3(patient);
        int bloodPressureCholPoints=BloodPressurePointsStep4(patient);
        int diabetiesCholPoints=DiabetesPoints(patient);
      int smokerCholPoints=SmokerPoints(patient);
      
      
      
      totalCholPoints=ageCholPoints+CholPointsStep2+HDLCholPointsStep3+bloodPressureCholPoints+diabetiesCholPoints+smokerCholPoints;
      
  
  
  
  return totalCholPoints;
  
  }
  
  
  //calculating LDL points from HDL in step3
  public static int CholPointsStep3(Patient patient)
  {
      int CholPointsStep3=0;
      VitalSignsDirectory vitalSigns=patient.getVitalSignsDirectory();
      double avgHDL=0;
      double sumHDL=0;
      for(VitalSigns vitalSign:vitalSigns.getVitalSignsList())
      {
          sumHDL=sumHDL+vitalSign.getHdcCholestrol();
      }
      
      avgHDL=sumHDL/6;
      
      //whether the patient is male
      if(patient.getGender().equalsIgnoreCase("male"))
      {
      
      if(avgHDL<35)
      {
          CholPointsStep3=2;
      }
      else if(avgHDL>=35&&avgHDL<=44)
      {
          CholPointsStep3=1;
      }
      else if(avgHDL>=45&&avgHDL<=49)
      {
          CholPointsStep3=0;
      }
      else if(avgHDL>=50&&avgHDL<=59)
      {
          CholPointsStep3=0;
      }
      else if(avgHDL>=60)
      {
          CholPointsStep3=(-2);
      }
      
      }
      //whther it is female
      if(patient.getGender().equalsIgnoreCase("female"))
      {
          if(avgHDL<35)
      {
          CholPointsStep3=5;
      }
      else if(avgHDL>=35&&avgHDL<=44)
      {
          CholPointsStep3=2;
      }
      else if(avgHDL>=45&&avgHDL<=49)
      {
          CholPointsStep3=1;
      }
      else if(avgHDL>=50&&avgHDL<=59)
      {
          CholPointsStep3=0;
      }
      else if(avgHDL>=60)
      {
          CholPointsStep3=(-3);
      }
          
          
          
          
      }
      
      
      
      return CholPointsStep3;
  }
  
  //calculating LDL points from HDL Points in step3
  public static int HDLLDLPointsStep3(Patient patient)
  {
      int HDLLDLPointsStep3=0;
      VitalSignsDirectory vitalSigns=patient.getVitalSignsDirectory();
      double avgHDL=0;
      double sumHDL=0;
      for(VitalSigns vitalSign:vitalSigns.getVitalSignsList())
      {
          sumHDL=sumHDL+vitalSign.getHdcCholestrol();
      }
      
      avgHDL=sumHDL/6;
      
      //whether the patient is male
      if(patient.getGender().equalsIgnoreCase("male"))
      {
      if(avgHDL<35)
      {
          HDLLDLPointsStep3=2;
      }
      else if(avgHDL>=35&&avgHDL<=44)
      {
          HDLLDLPointsStep3=1;
      }
      else if(avgHDL>=45&&avgHDL<=49)
      {
          HDLLDLPointsStep3=0;
      }
      else if(avgHDL>=50&&avgHDL<=59)
      {
          HDLLDLPointsStep3=0;
      }
      else if(avgHDL>=60)
      {
          HDLLDLPointsStep3=(-1);
      }
      }
      //whether it is female
      if(patient.getGender().equalsIgnoreCase("female"))
      {
           if(avgHDL<35)
      {
          HDLLDLPointsStep3=5;
      }
      else if(avgHDL>=35&&avgHDL<=44)
      {
          HDLLDLPointsStep3=2;
      }
      else if(avgHDL>=45&&avgHDL<=49)
      {
          HDLLDLPointsStep3=1;
      }
      else if(avgHDL>=50&&avgHDL<=59)
      {
          HDLLDLPointsStep3=0;
      }
      else if(avgHDL>=60)
      {
          HDLLDLPointsStep3=(-3);
      }
     
          
      }
      
      
      
      
      return HDLLDLPointsStep3;
  }
  
  //Calculating LDL points from LDL-C in step2
  public static int LDLPointsStep2(Patient patient)
  {
      int LDLPointsStep2=0;
      VitalSignsDirectory vitalSigns=patient.getVitalSignsDirectory();
      double avgLDL=0;
      double sumLDL=0;
      for(VitalSigns vitalSign:vitalSigns.getVitalSignsList())
      {
          sumLDL=sumLDL+vitalSign.getLdlCholestrol();
      }
      avgLDL=sumLDL/6;
      //whether the patuient is male
      if(patient.getGender().equalsIgnoreCase("male"))
      {
          
      if(avgLDL<100)
      {
          LDLPointsStep2=(-3);
      }
      else if(avgLDL>=100 && avgLDL<=129)
      {
          LDLPointsStep2=0;
      }
      else if(avgLDL>=130 && avgLDL<=159)
      {
          LDLPointsStep2=0;
      }
      else if(avgLDL>=160&&avgLDL<=190)
      {
          LDLPointsStep2=1;
      }
      else if(avgLDL>=190)
      {
          LDLPointsStep2=2;
      }
      }
      //whether the patient is female
      if(patient.getGender().equalsIgnoreCase("female"))
      {
          if(avgLDL<100)
      {
          LDLPointsStep2=(-2);
      }
      else if(avgLDL>=100 && avgLDL<=129)
      {
          LDLPointsStep2=0;
      }
      else if(avgLDL>=130 && avgLDL<=159)
      {
          LDLPointsStep2=0;
      }
      else if(avgLDL>=160&&avgLDL<=190)
      {
          LDLPointsStep2=2;
      }
      else if(avgLDL>=190)
      {
          LDLPointsStep2=2;
      }
      
          
      }
      
      
      return LDLPointsStep2;
      
      
      
  }
  //calculating Chol points from LDL-C in step2
  public static int CholPointsStep2(Patient patient)
  {
      int CholPointsStep2=0;
      VitalSignsDirectory vitalSigns=patient.getVitalSignsDirectory();
      double avgLDL=0;
      double sumLDL=0;
      for(VitalSigns vitalSign:vitalSigns.getVitalSignsList())
      {
          sumLDL=sumLDL+vitalSign.getLdlCholestrol();
      }
      avgLDL=sumLDL/6;
      //whether it is male
      if(patient.getGender().equalsIgnoreCase("male"))
      {
      if(avgLDL<100)
      {
          CholPointsStep2=(-3);
      }
      else if(avgLDL>=100 && avgLDL<=129)
      {
          CholPointsStep2=0;
      }
      else if(avgLDL>=130 && avgLDL<=159)
      {
          CholPointsStep2=0;
      }
      else if(avgLDL>=160&&avgLDL<=190)
      {
          CholPointsStep2=1;
      }
      else if(avgLDL>=190)
      {
          CholPointsStep2=2;
      }
      }
      //wnhether the patient is female
      if(patient.getGender().equalsIgnoreCase("female"))
      {
          
          if(avgLDL<160)
      {
          CholPointsStep2=(-2);
      }
      else if(avgLDL>=160 && avgLDL<=199)
      {
          CholPointsStep2=0;
      }
      else if(avgLDL>=200 && avgLDL<=239)
      {
          CholPointsStep2=1;
      }
      else if(avgLDL>=240&&avgLDL<=279)
      {
          CholPointsStep2=1;
      }
      else if(avgLDL>=280)
      {
          CholPointsStep2=3;
      }
      
          
          
          
          
      }
      
      return CholPointsStep2;
      
      
      
  }
  
  
  
  
  
  //calculating CHD risk from LDLpoints
  public static int determineLDLCHDrisk(Patient patient){
      int LDLCHDrisk=0;
      int totalLDLPoints=calculateTotalLDLPoints(patient);
      //calculating  CHD risk using LDL for male members
      if(patient.getGender().equalsIgnoreCase("male")){
          
      
      if(totalLDLPoints<=(-3))
      {
          LDLCHDrisk=1;
      }
      else if(totalLDLPoints==(-2))
      {
          LDLCHDrisk=2;
      }
      else if(totalLDLPoints==(-1))
      {
          LDLCHDrisk=2;
      }
      else if(totalLDLPoints==0)
      {
          LDLCHDrisk=3;
      }
      else if(totalLDLPoints==1)
      {
          LDLCHDrisk=4;
      }
      else if(totalLDLPoints==2)
      {
          LDLCHDrisk=4;
      }
      else if(totalLDLPoints==3)
      {
          LDLCHDrisk=6;
      }
      else if(totalLDLPoints==4)
      {
          LDLCHDrisk=7;
      }
      else if(totalLDLPoints==5)
      {
         LDLCHDrisk=9;
      }
      else if(totalLDLPoints==6)
      {
          LDLCHDrisk=11;
      }
      else if(totalLDLPoints==7)
      {
          LDLCHDrisk=14;
      }
      else if(totalLDLPoints==8)
      {
          LDLCHDrisk=18;
      }
      else if(totalLDLPoints==9)
      {
          LDLCHDrisk=22;
      }
      else if(totalLDLPoints==10)
      {
          LDLCHDrisk=27;
      }
      else if(totalLDLPoints==11)
      {
          LDLCHDrisk=33;
      }
      else if(totalLDLPoints==12)
      {
          LDLCHDrisk=40;
      }
      else if(totalLDLPoints==13)
      {
          LDLCHDrisk=47;
      }
      else if(totalLDLPoints>=14)
      {
          LDLCHDrisk=56;
      }
      
      }
      //calculating CHD risk for female members
      if(patient.getGender().equalsIgnoreCase("female"))
      {
          if(totalLDLPoints<=(-2))
      {
          LDLCHDrisk=1;
      }
      else if(totalLDLPoints==(-1))
      {
          LDLCHDrisk=2;
      }
      else if(totalLDLPoints==0)
      {
          LDLCHDrisk=2;
      }
      else if(totalLDLPoints==1)
      {
          LDLCHDrisk=2;
      }
      else if(totalLDLPoints==2)
      {
          LDLCHDrisk=3;
      }
      else if(totalLDLPoints==3)
      {
          LDLCHDrisk=3;
      }
      else if(totalLDLPoints==4)
      {
          LDLCHDrisk=4;
      }
      else if(totalLDLPoints==5)
      {
         LDLCHDrisk=5;
      }
      else if(totalLDLPoints==6)
      {
          LDLCHDrisk=6;
      }
      else if(totalLDLPoints==7)
      {
          LDLCHDrisk=7;
      }
      else if(totalLDLPoints==8)
      {
          LDLCHDrisk=8;
      }
      else if(totalLDLPoints==9)
      {
          LDLCHDrisk=9;
      }
      else if(totalLDLPoints==10)
      {
          LDLCHDrisk=11;
      }
      else if(totalLDLPoints==11)
      {
          LDLCHDrisk=13;
      }
      else if(totalLDLPoints==12)
      {
          LDLCHDrisk=15;
      }
      else if(totalLDLPoints==13)
      {
          LDLCHDrisk=17;
      }
      else if(totalLDLPoints==14)
      {
          LDLCHDrisk=20;
      }
      else if(totalLDLPoints==15)
      {
          LDLCHDrisk=24;
      }
      else if(totalLDLPoints==16)
      {
          LDLCHDrisk=27;
      }
      else if(totalLDLPoints>=17)
      {
          LDLCHDrisk=32;
      }
      

      
      
      }
      
      
      
      
      
      
      
      
      return LDLCHDrisk;
      
  }
//calculating CHD risk from Chol points
  public static int determineCholCHDrisk(Patient patient)
  {
      int CholCHDrisk=0;
      int totalCholPoints=calculateTotalCholPoints(patient);
      
      //calculating CHD risk for male members
      if(patient.getGender().equalsIgnoreCase("male"))
      {
         if(totalCholPoints==(-1))
      {
          CholCHDrisk=2;
      }
      else if(totalCholPoints==0)
      {
          CholCHDrisk=3;
      }
      else if(totalCholPoints==1)
      {
          CholCHDrisk=3;
      }
      else if(totalCholPoints==2)
      {
          CholCHDrisk=4;
      }
      else if(totalCholPoints==3)
      {
          CholCHDrisk=5;
      }
      else if(totalCholPoints==4)
      {
          CholCHDrisk=7;
      }
      else if(totalCholPoints==5)
      {
         CholCHDrisk=8;
      }
      else if(totalCholPoints==6)
      {
          CholCHDrisk=10;
      }
      else if(totalCholPoints==7)
      {
          CholCHDrisk=13;
      }
      else if(totalCholPoints==8)
      {
          CholCHDrisk=16;
      }
      else if(totalCholPoints==9)
      {
          CholCHDrisk=20;
      }
      else if(totalCholPoints==10)
      {
          CholCHDrisk=25;
      }
      else if(totalCholPoints==11)
      {
          CholCHDrisk=31;
      }
      else if(totalCholPoints==12)
      {
          CholCHDrisk=37;
      }
      else if(totalCholPoints==13)
      {
          CholCHDrisk=45;
      }
      else if(totalCholPoints>=14)
      {
          CholCHDrisk=53;
      }
      }
      //calculating CHD risk for female members
      else if(patient.getGender().equalsIgnoreCase("female"))
      {
          
           if(totalCholPoints<=(-2))
      {
          CholCHDrisk=1;
      }
      else if(totalCholPoints==(-1))
      {
          CholCHDrisk=2;
      }
      else if(totalCholPoints==0)
      {
          CholCHDrisk=2;
      }
      else if(totalCholPoints==1)
      {
          CholCHDrisk=2;
      }
      else if(totalCholPoints==2)
      {
          CholCHDrisk=3;
      }
      else if(totalCholPoints==3)
      {
          CholCHDrisk=3;
      }
      else if(totalCholPoints==4)
      {
          CholCHDrisk=4;
      }
      else if(totalCholPoints==5)
      {
         CholCHDrisk=4;
      }
      else if(totalCholPoints==6)
      {
          CholCHDrisk=5;
      }
      else if(totalCholPoints==7)
      {
          CholCHDrisk=6;
      }
      else if(totalCholPoints==8)
      {
          CholCHDrisk=7;
      }
      else if(totalCholPoints==9)
      {
          CholCHDrisk=8;
      }
      else if(totalCholPoints==10)
      {
          CholCHDrisk=10;
      }
      else if(totalCholPoints==11)
      {
          CholCHDrisk=11;
      }
      else if(totalCholPoints==12)
      {
          CholCHDrisk=13;
      }
      else if(totalCholPoints==13)
      {
          CholCHDrisk=15;
      }
      else if(totalCholPoints==14)
      {
          CholCHDrisk=18;
      }
      else if(totalCholPoints==15)
      {
          CholCHDrisk=20;
      }
      else if(totalCholPoints==16)
      {
          CholCHDrisk=24;
      }
      else if(totalCholPoints>=17)
      {
          CholCHDrisk=27;
      }
      }
      
      
      
      
      
      
      
      
      return CholCHDrisk;
      
  }
  //calculating LDL points from age
  public static int LDLPointsStep1(Patient patient)
  {
      int ageLDLPoints=0;
      int age=patient.getAge();
      //check for male
      if(patient.getGender().equalsIgnoreCase("male"))
      {
         if(age>=30&&age<=34)
         {
             ageLDLPoints=-1;
         }
         else if(age>=35&&age<=39)
         {
             ageLDLPoints=0;
         }
         else if(age>=40&&age<=44)
         {
             ageLDLPoints=1;
         }
         else if(age>=45&&age<=49)
         {
             ageLDLPoints=2;
         }
         else if(age>=50&&age<=54)
         {
             ageLDLPoints=3;
         }
         else if(age>=55&&age<=59)
         {
             ageLDLPoints=4;
         }
         else if(age>=60&&age<=64)
         {
             ageLDLPoints=5;
         }
         else if(age>=65&&age<=69)
         {
             ageLDLPoints=6;
         }
         else if(age>=70&&age<=74)
         {
             ageLDLPoints=7;
         }
          
      } 
          
         
      //check for female
      if(patient.getGender().equalsIgnoreCase("female"))
      {
          if(age>=30&&age<=34)
         {
             ageLDLPoints=(-9);
         }
         else if(age>=35&&age<=39)
         {
             ageLDLPoints=(-4);
         }
         else if(age>=40&&age<=44)
         {
             ageLDLPoints=0;
         }
         else if(age>=45&&age<=49)
         {
             ageLDLPoints=3;
         }
         else if(age>=50&&age<=54)
         {
             ageLDLPoints=6;
         }
         else if(age>=55&&age<=59)
         {
             ageLDLPoints=7;
         }
         else if(age>=60&&age<=64)
         {
             ageLDLPoints=8;
         }
         else if(age>=65&&age<=69)
         {
             ageLDLPoints=8;
         }
         else if(age>=70&&age<=74)
         {
             ageLDLPoints=8;
         }
          
          
          
          
          
          
          
      }
      
      
      
      
      
      
      
      
      return ageLDLPoints;
      
      
  }
   //calculating Chol points from age
  public static int CholPointsStep1(Patient patient)
  {
      int ageCholPoints=0;
      int age=patient.getAge();
      
      
      
      //check if male
      if(patient.getGender().equalsIgnoreCase("male"))
      {
          
           if(age>=30&&age<=34)
         {
             ageCholPoints=(-1);
         }
         else if(age>=35&&age<=39)
         {
             ageCholPoints=0;
         }
         else if(age>=40&&age<=44)
         {
             ageCholPoints=1;
         }
         else if(age>=45&&age<=49)
         {
             ageCholPoints=2;
         }
         else if(age>=50&&age<=54)
         {
             ageCholPoints=3;
         }
         else if(age>=55&&age<=59)
         {
             ageCholPoints=4;
         }
         else if(age>=60&&age<=64)
         {
             ageCholPoints=5;
         }
         else if(age>=65&&age<=69)
         {
             ageCholPoints=6;
         }
         else if(age>=70&&age<=74)
         {
             ageCholPoints=7;
         }
          
          
          
          
          
          
          
          
          
      }
      //check if female
      if(patient.getGender().equalsIgnoreCase("female"))
      {
           if(age>=30&&age<=34)
         {
             ageCholPoints=(-9);
         }
         else if(age>=35&&age<=39)
         {
             ageCholPoints=(-4);
         }
         else if(age>=40&&age<=44)
         {
             ageCholPoints=0;
         }
         else if(age>=45&&age<=49)
         {
             ageCholPoints=3;
         }
         else if(age>=50&&age<=54)
         {
             ageCholPoints=6;
         }
         else if(age>=55&&age<=59)
         {
             ageCholPoints=7;
         }
         else if(age>=60&&age<=64)
         {
             ageCholPoints=8;
         }
         else if(age>=65&&age<=69)
         {
             ageCholPoints=8;
         }
         else if(age>=70&&age<=74)
         {
             ageCholPoints=8;
         }
          
         
          
      }
      
      
      
      
      
      
      
      
      return ageCholPoints;
      
  }
  public static boolean ComparativeRisk(Patient patient)
  {
      boolean atRisk=false;
      int age=patient.getAge();
      float avgCHDrisk=patient.getLDLRisk()+patient.getCholRisk();
      avgCHDrisk=avgCHDrisk/2;
      //check for male patient
      
      if(patient.getGender().equalsIgnoreCase("male"))
      {
            if((age>=30&&age<=34)&&avgCHDrisk>3)
         {
             atRisk=true;
         }
         else if((age>=35&&age<=39)&&avgCHDrisk>5)
         {
             atRisk=true;
         }
         else if((age>=40&&age<=44)&&avgCHDrisk>7)
         {
             atRisk=true;
         }
         else if((age>=45&&age<=49)&&avgCHDrisk>11)
         {
             atRisk=true;
         }
         else if((age>=50&&age<=54)&&avgCHDrisk>14)
         {
             atRisk=true;
         }
         else if((age>=55&&age<=59)&&avgCHDrisk>16)
         {
             atRisk=true;
         }
         else if((age>=60&&age<=64)&&avgCHDrisk>21)
         {
             atRisk=true;
         }
         else if((age>=65&&age<=69)&&avgCHDrisk>25)
         {
             atRisk=true;
         }
         else if((age>=70&&age<=74)&&avgCHDrisk>30)
         {
             atRisk=true;
         }
         
          
          
          
          
          
          
          
          
      }
      //check for female patients
      if(patient.getGender().equalsIgnoreCase("female"))
      {
      
           if((age>=30&&age<=34)&&avgCHDrisk>1)
         {
             atRisk=true;
         }
         else if((age>=35&&age<=39)&&avgCHDrisk>1)
         {
             atRisk=true;
         }
         else if((age>=40&&age<=44)&&avgCHDrisk>2)
         {
             atRisk=true;
         }
         else if((age>=45&&age<=49)&&avgCHDrisk>5)
         {
             atRisk=true;
         }
         else if((age>=50&&age<=54)&&avgCHDrisk>8)
         {
             atRisk=true;
         }
         else if((age>=55&&age<=59)&&avgCHDrisk>12)
         {
             atRisk=true;
         }
         else if((age>=60&&age<=64)&&avgCHDrisk>12)
         {
             atRisk=true;
         }
         else if((age>=65&&age<=69)&&avgCHDrisk>13)
         {
             atRisk=true;
         }
         else if((age>=70&&age<=74)&&avgCHDrisk>14)
         {
             atRisk=true;
         }
         
      
      
      
      }
      return atRisk;
      
  }
    
}
