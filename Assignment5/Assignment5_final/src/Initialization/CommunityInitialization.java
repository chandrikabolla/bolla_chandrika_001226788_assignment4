/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Initialization;
import City.Business.Community;
import City.Business.HouseDirectory;

/**
 *
 * @author Guest
 */
public class CommunityInitialization {
    
    public static Community community;
    public static HouseDirectory houseDirectory;
     public static Community initializeBrooklineCommunity()
    {
        
        community=new Community();
       
        community.setCommunityName("Brookline Community");
        community.setCommunityCode(1);
        community.setHouseDirectory(Initialization.HouseInitialization.intializeBrooklineHouseDirectory());
        return community;
    }
    public static Community initializeWoburnCommunity()
    {
        
        Community community=new Community();
 
        
           community.setCommunityName("Woburn Community");
        community.setCommunityCode(2);
        community.setHouseDirectory(Initialization.HouseInitialization.intializeWoburnHouseDirectory());
        return community;
        
    }
    public static Community initializeMarbleheadCommunity()
    {
        
        Community community=new Community();
 
        
           community.setCommunityName("Marblehead Community");
        community.setCommunityCode(3);
        community.setHouseDirectory(Initialization.HouseInitialization.intializeMarbleheadHouseDirectory());
        return community;
        
    }
    public static Community initializeNewtonCommunity()
    {
        
        Community community=new Community();
 
        
           community.setCommunityName("Newton Community");
        community.setCommunityCode(4);
        community.setHouseDirectory(Initialization.HouseInitialization.intializeNewtonHouseDirectory());
        return community;
        
    }
    public static Community initializeWalthamCommunity()
    {
        
        Community community=new Community();
 

        community.setCommunityName("Waltham Community");
        community.setCommunityCode(5);
        community.setHouseDirectory(Initialization.HouseInitialization.intializeWalthamHouseDirectory());
        return community;
        
    }
    public static Community initializeWinchesterCommunity()
    {
        
        Community community=new Community();
 
        
           community.setCommunityName("WinchesterCommunity");
        community.setCommunityCode(6);
        community.setHouseDirectory(Initialization.HouseInitialization.intializeWinchesterHouseDirectory());
        return community;
    }
    public static Community initializeBelmontCommunity()
    {
        
        Community community=new Community();
 
        
           community.setCommunityName("Belmont Community");
        community.setCommunityCode(7);
        community.setHouseDirectory(Initialization.HouseInitialization.intializeBelmontHouseDirectory());
        return community;
        
    }
    public static Community initializeEastleighCommunity()
    {
        
        Community community=new Community();
 
        
           community.setCommunityName("Eastleigh Community");
        community.setCommunityCode(8); 
        community.setHouseDirectory(Initialization.HouseInitialization.intializeEastleighHouseDirectory());
        return community;
    }
    public static Community initializeCallahanCommunity()
    {
        
        Community community=new Community();
 
        
        community.setCommunityName("Callahan Community");
        community.setCommunityCode(9);  
        community.setHouseDirectory(Initialization.HouseInitialization.intializeCallahanHouseDirectory());
        return community;
        
    }
    public static Community initializeNewEnglandCommunity()
    {
        
        Community community=new Community();
 
        community.setCommunityName("New England Community");
        community.setCommunityCode(10);
        community.setHouseDirectory(Initialization.HouseInitialization.intializeNewEnglandHouseDirectory());
        
        
        
        
        return community;
    } 
}
