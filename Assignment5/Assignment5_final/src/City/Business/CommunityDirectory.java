/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package City.Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Guest
 */
public class CommunityDirectory {
   
    
    List<Community> communityList;
    Community community1;
    String communityName;

    public CommunityDirectory() {
        communityList = new ArrayList<Community>();
    }

    public List<Community> getCommunityList() {
        return communityList;
    }

    public void setCommunityList(List<Community> communityList) {
        this.communityList = communityList;
    }

    public Community addCommunity(Community community) {
        this.community1=community;
        communityList.add(community);
        return community;
    }
    public Community searchCommunity(String communityname)
    {
        this.communityName=communityname;
        for(Community community2 :communityList)
        {
            if(community2.getCommunityName().equals(communityname))
            {
                    return community2;
            }
            
        }
        return null;
    }
}

    

