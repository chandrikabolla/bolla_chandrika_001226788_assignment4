/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package City.Business;

/**
 *
 * @author Nidhi Mittal
 */
public class Patient extends Person {

    private String primDocName;
    private String prefPharmacy;
    private boolean smoking;
    private boolean diabetes;
    private int avgSystolicBloodPressure;
    private double avgDiastolicBloodPressure;
    private double avgCholestrol;
    private double avgLDCholestrol;
    private double avgHDCholestrol;

    private float ldlPoints;
    private float cholPoints;
    private float riskFromLDLPoints;
    private float riskFromCholPoints;
    private int LDLRisk;
    private int CholRisk;
    private boolean atRisk;
    private VitalSignsDirectory vitalSignsDirectory;

    public boolean isAtRisk() {
        return atRisk;
    }

    public void setAtRisk(boolean atRisk) {
        this.atRisk = atRisk;
    }

    public int getLDLRisk() {
        return LDLRisk;
    }

    public void setLDLRisk(int LDLRisk) {
        this.LDLRisk = LDLRisk;
    }

    public int getCholRisk() {
        return CholRisk;
    }

    public void setCholRisk(int CholRisk) {
        this.CholRisk = CholRisk;
    }

    public String getPrimDocName() {
        return primDocName;
    }

    public void setPrimDocName(String primDocName) {
        this.primDocName = primDocName;
    }

    public String getPrefPharmacy() {
        return prefPharmacy;
    }

    public void setPrefPharmacy(String prefPharmacy) {
        this.prefPharmacy = prefPharmacy;
    }

    public boolean isSmoking() {
        return smoking;
    }

    public void setSmoking(boolean smoking) {
        this.smoking = smoking;
    }

    public boolean isDiabetes() {
        return diabetes;
    }

    public void setDiabetes(boolean diabetes) {
        this.diabetes = diabetes;
    }

    public int getAvgSystolicBloodPressure() {
        return avgSystolicBloodPressure;
    }

    public void setAvgSystolicBloodPressure(int avgSystolicBloodPressure) {
        this.avgSystolicBloodPressure = avgSystolicBloodPressure;
    }

    public double getAvgCholestrol() {
        return avgCholestrol;
    }

    public void setAvgCholestrol(double avgCholestrol) {
        this.avgCholestrol = avgCholestrol;
    }

    public double getAvgLDCholestrol() {
        return avgLDCholestrol;
    }

    public void setAvgLDCholestrol(double avgLDCholestrol) {
        this.avgLDCholestrol = avgLDCholestrol;
    }

    public double getAvgHDCholestrol() {
        return avgHDCholestrol;
    }

    public void setAvgHDCholestrol(double avgHDCholestrol) {
        this.avgHDCholestrol = avgHDCholestrol;
    }

    public double getAvgDiastolicBloodPressure() {
        return avgDiastolicBloodPressure;
    }

    public void setAvgDiastolicBloodPressure(double avgDiastolicBloodPressure) {
        this.avgDiastolicBloodPressure = avgDiastolicBloodPressure;
    }

    public float getLdlPoints() {
        return ldlPoints;
    }

    public void setLdlPoints(float ldlPoints) {
        this.ldlPoints = ldlPoints;
    }

    public float getCholPoints() {
        return cholPoints;
    }

    public void setCholPoints(float cholPoints) {
        this.cholPoints = cholPoints;
    }

    public float getRiskFromLDLPoints() {
        return riskFromLDLPoints;
    }

    public void setRiskFromLDLPoints(float riskFromLDLPoints) {
        this.riskFromLDLPoints = riskFromLDLPoints;
    }

    public float getRiskFromCholPoints() {
        return riskFromCholPoints;
    }

    public void setRiskFromCholPoints(float riskFromCholPoints) {
        this.riskFromCholPoints = riskFromCholPoints;
    }

    public VitalSignsDirectory getVitalSignsDirectory() {
        return vitalSignsDirectory;
    }

    public void setVitalSignsDirectory(VitalSignsDirectory vitalSignsDirectory) {
        this.vitalSignsDirectory = vitalSignsDirectory;
    }

    private void updatePointsAccordingToAge() {
        if (getGender().equalsIgnoreCase("M")) {
            if (age >= 30 && age <= 34) {
                ldlPoints += -1;
                cholPoints += -1;
            } else if (age >= 35 && age <= 39) {
                ldlPoints += 0;
                cholPoints += 0;
            } else if (age >= 40 && age <= 44) {
                ldlPoints += 1;
                cholPoints += 1;
            } else if (age >= 45 && age <= 49) {
                ldlPoints += 2;
                cholPoints += 2;
            } else if (age >= 50 && age <= 54) {
                ldlPoints += 3;
                cholPoints += 3;
            } else if (age >= 55 && age <= 59) {
                ldlPoints += 4;
                cholPoints += 4;
            } else if (age >= 60 && age <= 64) {
                ldlPoints += 5;
                cholPoints += 5;
            } else if (age >= 65 && age <= 69) {
                ldlPoints += 6;
                cholPoints += 6;
            } else if (age >= 70 && age <= 74) {
                ldlPoints += 7;
                cholPoints += 7;
            }
        } else if (getGender().equalsIgnoreCase("F")) {
            if (age >= 30 && age <= 34) {
                ldlPoints += -9;
                cholPoints += -9;
            } else if (age >= 35 && age <= 39) {
                ldlPoints += -4;
                cholPoints += -4;
            } else if (age >= 40 && age <= 44) {
                ldlPoints += 0;
                cholPoints += 0;
            } else if (age >= 45 && age <= 49) {
                ldlPoints += 3;
                cholPoints += 3;
            } else if (age >= 50 && age <= 54) {
                ldlPoints += 6;
                cholPoints += 6;
            } else if (age >= 55 && age <= 59) {
                ldlPoints += 7;
                cholPoints += 7;
            } else if (age >= 60 && age <= 64) {
                ldlPoints += 8;
                cholPoints += 8;
            } else if (age >= 65 && age <= 69) {
                ldlPoints += 8;
                cholPoints += 8;
            } else if (age >= 70 && age <= 74) {
                ldlPoints += 8;
                cholPoints += 8;
            }
        }

    }

    private void updatePointsAccordingToLDLCholestrol() {
        if (getGender().equalsIgnoreCase("M")) {
            if (avgLDCholestrol < 2.59) {
                ldlPoints += -3;
            } else if (avgLDCholestrol <= 2.60 && avgLDCholestrol <= 3.36) {
                ldlPoints += 0;
            } else if (avgLDCholestrol >= 3.37 && avgLDCholestrol <= 4.14) {
                ldlPoints += 0;
            } else if (avgLDCholestrol >= 4.15 && avgLDCholestrol < 4.92) {
                ldlPoints += 1;
            } else if (avgLDCholestrol >= 4.92) {
                ldlPoints += 2;
            }
        } else if (getGender().equalsIgnoreCase("F")) {
            if (avgLDCholestrol < 2.59) {
                ldlPoints += -2;
            } else if (avgLDCholestrol <= 2.60 && avgLDCholestrol <= 3.36) {
                ldlPoints += 0;
            } else if (avgLDCholestrol >= 3.37 && avgLDCholestrol <= 4.14) {
                ldlPoints += 0;
            } else if (avgLDCholestrol >= 4.15 && avgLDCholestrol < 4.92) {
                ldlPoints += 2;
            } else if (avgLDCholestrol >= 4.92) {
                ldlPoints += 2;
            }
        }
    }

    private void updatePointsAccordingToHDLCholestrol() {
        if (getGender().equalsIgnoreCase("M")) {
            if (avgHDCholestrol < 0.90) {
                ldlPoints += 2;
                cholPoints += 2;
            } else if (avgHDCholestrol >= 0.91 && avgHDCholestrol <= 1.16) {
                ldlPoints += 1;
                cholPoints += 1;
            } else if (avgHDCholestrol >= 1.17 && avgHDCholestrol <= 1.29) {
                ldlPoints += 0;
                cholPoints += 0;
            } else if (avgHDCholestrol >= 1.30 && avgHDCholestrol <= 1.55) {
                ldlPoints += 0;
                cholPoints += 0;
            } else if (avgHDCholestrol >= 1.56) {
                ldlPoints += -1;
                cholPoints += -2;
            }
        } else if (getGender().equalsIgnoreCase("F")) {
            if (avgHDCholestrol < 0.90) {
                ldlPoints += 5;
                cholPoints += 5;
            } else if (avgHDCholestrol >= 0.91 && avgHDCholestrol <= 1.16) {
                ldlPoints += 2;
                cholPoints += 2;
            } else if (avgHDCholestrol >= 1.17 && avgHDCholestrol <= 1.29) {
                ldlPoints += 1;
                cholPoints += 1;
            } else if (avgHDCholestrol >= 1.30 && avgHDCholestrol <= 1.55) {
                ldlPoints += 0;
                cholPoints += 0;
            } else if (avgHDCholestrol >= 1.56) {
                ldlPoints += -2;
                cholPoints += -3;
            }
        }
    }

    private void updatePointsAccordingToTotalCholestrol() {
        if (getGender().equalsIgnoreCase("M")) {
            if (avgCholestrol < 4.14) {
                cholPoints += -3;
            } else if (avgCholestrol >= 4.15 && avgCholestrol <= 5.17) {
                cholPoints += 0;
            } else if (avgCholestrol >= 5.18 && avgCholestrol <= 6.21) {
                cholPoints += 1;
            } else if (avgCholestrol >= 6.22 && avgCholestrol <= 7.24) {
                cholPoints += 2;
            } else if (avgCholestrol >= 7.25) {
                cholPoints += 3;
            }
        } else if (getGender().equalsIgnoreCase("F")) {
            if (avgCholestrol < 4.14) {
                cholPoints += -2;
            } else if (avgCholestrol >= 4.15 && avgCholestrol <= 5.17) {
                cholPoints += 0;
            } else if (avgCholestrol >= 5.18 && avgCholestrol <= 6.21) {
                cholPoints += 1;
            } else if (avgCholestrol >= 6.22 && avgCholestrol <= 7.24) {
                cholPoints += 1;
            } else if (avgCholestrol >= 7.25) {
                cholPoints += 3;
            }
        }
    }

    private float getPointsAccordingToSystolicBP() {
        float points = 0;

        if (getGender().equalsIgnoreCase("M")) {
            if (avgSystolicBloodPressure < 120) {
                points = 0;
            } else if (avgSystolicBloodPressure >= 120 && avgSystolicBloodPressure <= 129) {
                points = 0;
            } else if (avgSystolicBloodPressure >= 130 && avgSystolicBloodPressure <= 139) {
                points = 1;
            } else if (avgSystolicBloodPressure >= 140 && avgSystolicBloodPressure <= 159) {
                points = 2;
            } else if (avgSystolicBloodPressure >= 160) {
                points = 3;
            }
        } else if (getGender().equalsIgnoreCase("F")) {
            if (avgSystolicBloodPressure < 120) {
                points = -3;
            } else if (avgSystolicBloodPressure >= 120 && avgSystolicBloodPressure <= 129) {
                points = 0;
            } else if (avgSystolicBloodPressure >= 130 && avgSystolicBloodPressure <= 139) {
                points = 0;
            } else if (avgSystolicBloodPressure >= 140 && avgSystolicBloodPressure <= 159) {
                points = 2;
            } else if (avgSystolicBloodPressure >= 160) {
                points = 3;
            }
        }

        return points;
    }

    private float getPointsAccordingToDiastolicBP() {
        float points = 0;

        if (getGender().equalsIgnoreCase("M")) {
            if (avgDiastolicBloodPressure < 80) {
                points = 0;
            } else if (avgDiastolicBloodPressure >= 80 && avgDiastolicBloodPressure <= 84) {
                points = 0;
            } else if (avgDiastolicBloodPressure >= 85 && avgDiastolicBloodPressure <= 89) {
                points = 1;
            } else if (avgDiastolicBloodPressure >= 90 && avgDiastolicBloodPressure <= 99) {
                points = 2;
            } else if (avgDiastolicBloodPressure >= 100) {
                points = 3;
            }
        } else if (getGender().equalsIgnoreCase("F")) {
            if (avgDiastolicBloodPressure < 80) {
                points = -3;
            } else if (avgDiastolicBloodPressure >= 80 && avgDiastolicBloodPressure <= 84) {
                points = 0;
            } else if (avgDiastolicBloodPressure >= 85 && avgDiastolicBloodPressure <= 89) {
                points = 0;
            } else if (avgDiastolicBloodPressure >= 90 && avgDiastolicBloodPressure <= 99) {
                points = 2;
            } else if (avgDiastolicBloodPressure >= 100) {
                points = 3;
            }
        }
        return points;
    }

    private void updatePointsAccordingToBP() {
        float systolicBPPoints = getPointsAccordingToSystolicBP();
        float diastolicBPPoints = getPointsAccordingToDiastolicBP();

        if (systolicBPPoints >= diastolicBPPoints) {
            ldlPoints += systolicBPPoints;
            cholPoints += systolicBPPoints;
        } else {
            ldlPoints += diastolicBPPoints;
            cholPoints += diastolicBPPoints;
        }
    }

    private void updatePointsAccordingToSmoking() {
        if (getGender().equalsIgnoreCase("M")) {
            if (isSmoking()) {
                ldlPoints += 2;
                cholPoints += 2;
            } else {
                ldlPoints += 0;
                cholPoints += 0;
            }
        } else if (getGender().equalsIgnoreCase("F")) {
            if (isSmoking()) {
                ldlPoints += 2;
                cholPoints += 2;
            } else {
                ldlPoints += 0;
                cholPoints += 0;
            }
        }
    }

    private void updatePointsAccordingToDiabetes() {
        if (getGender().equalsIgnoreCase("M")) {

            if (isDiabetes()) {
                ldlPoints += 2;
                cholPoints += 2;
            } else {
                ldlPoints += 0;
                cholPoints += 0;
            }
        } else if (getGender().equalsIgnoreCase("F")) {

            if (isDiabetes()) {
                ldlPoints += 4;
                cholPoints += 4;
            } else {
                ldlPoints += 0;
                cholPoints += 0;
            }
        }
    }

    private void calculateRiskFromLDLPoints() {

        if (getGender().equalsIgnoreCase("M")) {
            if (ldlPoints < -3) {
                riskFromLDLPoints = 1;
            } else if (ldlPoints == -2) {
                riskFromLDLPoints = 2;
            } else if (ldlPoints == -1) {
                riskFromLDLPoints = 2;
            } else if (ldlPoints == 0) {
                riskFromLDLPoints = 3;
            } else if (ldlPoints == 1) {
                riskFromLDLPoints = 4;
            } else if (ldlPoints == 2) {
                riskFromLDLPoints = 4;
            } else if (ldlPoints == 3) {
                riskFromLDLPoints = 6;
            } else if (ldlPoints == 4) {
                riskFromLDLPoints = 7;
            } else if (ldlPoints == 5) {
                riskFromLDLPoints = 9;
            } else if (ldlPoints == 6) {
                riskFromLDLPoints = 11;
            } else if (ldlPoints == 7) {
                riskFromLDLPoints = 14;
            } else if (ldlPoints == 8) {
                riskFromLDLPoints = 18;
            } else if (ldlPoints == 9) {
                riskFromLDLPoints = 22;
            } else if (ldlPoints == 10) {
                riskFromLDLPoints = 27;
            } else if (ldlPoints == 11) {
                riskFromLDLPoints = 33;
            } else if (ldlPoints == 12) {
                riskFromLDLPoints = 40;
            } else if (ldlPoints == 13) {
                riskFromLDLPoints = 47;
            } else if (ldlPoints >= 14) {
                riskFromLDLPoints = 56;
            }
        } else if (getGender().equalsIgnoreCase("F")) {
            if (ldlPoints < -2) {
                riskFromLDLPoints = 1;
            } else if (ldlPoints == -1) {
                riskFromLDLPoints = 2;
            } else if (ldlPoints == 0) {
                riskFromLDLPoints = 2;
            } else if (ldlPoints == 1) {
                riskFromLDLPoints = 2;
            } else if (ldlPoints == 2) {
                riskFromLDLPoints = 3;
            } else if (ldlPoints == 3) {
                riskFromLDLPoints = 3;
            } else if (ldlPoints == 4) {
                riskFromLDLPoints = 4;
            } else if (ldlPoints == 5) {
                riskFromLDLPoints = 5;
            } else if (ldlPoints == 6) {
                riskFromLDLPoints = 6;
            } else if (ldlPoints == 7) {
                riskFromLDLPoints = 7;
            } else if (ldlPoints == 8) {
                riskFromLDLPoints = 8;
            } else if (ldlPoints == 9) {
                riskFromLDLPoints = 9;
            } else if (ldlPoints == 10) {
                riskFromLDLPoints = 11;
            } else if (ldlPoints == 11) {
                riskFromLDLPoints = 13;
            } else if (ldlPoints == 12) {
                riskFromLDLPoints = 15;
            } else if (ldlPoints == 13) {
                riskFromLDLPoints = 17;
            } else if (ldlPoints == 14) {
                riskFromLDLPoints = 20;
            } else if (ldlPoints == 15) {
                riskFromLDLPoints = 24;
            } else if (ldlPoints == 16) {
                riskFromLDLPoints = 27;
            } else if (ldlPoints >= 17) {
                riskFromLDLPoints = 32;
            }
        }
    }

    private void calculateRiskFromCholPoints() {

        if (getGender().equalsIgnoreCase("M")) {
            if (cholPoints < -1) {
                riskFromCholPoints = 2;
            } else if (cholPoints == 0) {
                riskFromCholPoints = 3;
            } else if (cholPoints == 1) {
                riskFromCholPoints = 3;
            } else if (cholPoints == 2) {
                riskFromCholPoints = 4;
            } else if (cholPoints == 3) {
                riskFromCholPoints = 5;
            } else if (cholPoints == 4) {
                riskFromCholPoints = 7;
            } else if (cholPoints == 5) {
                riskFromCholPoints = 8;
            } else if (cholPoints == 6) {
                riskFromCholPoints = 10;
            } else if (cholPoints == 7) {
                riskFromCholPoints = 13;
            } else if (cholPoints == 8) {
                riskFromCholPoints = 16;
            } else if (cholPoints == 9) {
                riskFromCholPoints = 20;
            } else if (cholPoints == 10) {
                riskFromCholPoints = 25;
            } else if (cholPoints == 11) {
                riskFromCholPoints = 31;
            } else if (cholPoints == 12) {
                riskFromCholPoints = 37;
            } else if (cholPoints == 13) {
                riskFromCholPoints = 45;
            } else if (cholPoints >= 14) {
                riskFromCholPoints = 53;
            }
        } else if (getGender().equalsIgnoreCase("F")) {
            if (cholPoints < -2) {
                riskFromCholPoints = 1;
            } else if (cholPoints == -1) {
                riskFromCholPoints = 2;
            } else if (cholPoints == 0) {
                riskFromCholPoints = 2;
            } else if (cholPoints == 1) {
                riskFromCholPoints = 2;
            } else if (cholPoints == 2) {
                riskFromCholPoints = 3;
            } else if (cholPoints == 3) {
                riskFromCholPoints = 3;
            } else if (cholPoints == 4) {
                riskFromCholPoints = 4;
            } else if (cholPoints == 5) {
                riskFromCholPoints = 4;
            } else if (cholPoints == 6) {
                riskFromCholPoints = 5;
            } else if (cholPoints == 7) {
                riskFromCholPoints = 6;
            } else if (cholPoints == 8) {
                riskFromCholPoints = 7;
            } else if (cholPoints == 9) {
                riskFromCholPoints = 8;
            } else if (cholPoints == 10) {
                riskFromCholPoints = 10;
            } else if (cholPoints == 11) {
                riskFromCholPoints = 11;
            } else if (cholPoints == 12) {
                riskFromCholPoints = 13;
            } else if (cholPoints == 13) {
                riskFromCholPoints = 15;
            } else if (cholPoints == 13) {
                riskFromCholPoints = 15;
            } else if (cholPoints == 14) {
                riskFromCholPoints = 18;
            } else if (cholPoints == 15) {
                riskFromCholPoints = 20;
            } else if (cholPoints == 16) {
                riskFromCholPoints = 24;
            } else if (cholPoints >= 17) {
                riskFromCholPoints = 27;
            }
        }
    }

    public void calculateRisk() {
        setLdlPoints(0);
        setCholPoints(0);

        updatePointsAccordingToAge();
        updatePointsAccordingToLDLCholestrol();
        updatePointsAccordingToTotalCholestrol();
        updatePointsAccordingToHDLCholestrol();
        updatePointsAccordingToBP();
        updatePointsAccordingToDiabetes();
        updatePointsAccordingToSmoking();

        //System.out.println("Total LDL points: " + getLdlPoints());
        //System.out.println("Total Chol points: " + getCholPoints());
        calculateRiskFromLDLPoints();
        calculateRiskFromCholPoints();
    }

}
