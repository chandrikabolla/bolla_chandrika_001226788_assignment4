/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package City.Business;

import java.util.ArrayList;

/**
 *
 * @author Guest
 */
public class Person {
    
    public String firstName;
    public String lastName;
    public int age;
    public String gender;
    public ArrayList<Patient> sonList;
    public ArrayList<Patient> daughterList;
    public ArrayList<Patient> siblings;
    
    
    public Person()
    {
        siblings=new ArrayList<Patient>();
        sonList=new ArrayList<Patient>();
        daughterList=new ArrayList<Patient>();
      
        
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }


    public ArrayList<Patient> getSiblings() {
        return siblings;
    }

    public void setSiblings(ArrayList<Patient> siblings) {
        this.siblings = siblings;
    }

    public ArrayList<Patient> getSonList() {
        return sonList;
    }

    public void setSonList(ArrayList<Patient> sonList) {
        this.sonList = sonList;
    }

    public ArrayList<Patient> getDaughterList() {
        return daughterList;
    }

    public void setDaughterList(ArrayList<Patient> daughterList) {
        this.daughterList = daughterList;
    }

    

    
    @Override
    public String toString()
            {
                return firstName;
            }
    
    
    
    
    
    
    
}
