/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package City.Business;

import java.util.ArrayList;

/**
 *
 * @author Guest
 */
public class House {
    public FamilyDirectory familyDirectory;
    public String houseaddress;
    public int houseNumber;
    
    public House(){
        familyDirectory=new FamilyDirectory();
    }

    public String getHouseaddress() {
        return houseaddress;
    }

    public void setHouseaddress(String houseaddress) {
        this.houseaddress = houseaddress;
    }

    public int getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(int houseNumber) {
        this.houseNumber = houseNumber;
    }

    public FamilyDirectory getFamilyDirectory() {
        return familyDirectory;
    }

    public void setFamilyDirectory(FamilyDirectory familyDirectory) {
        this.familyDirectory = familyDirectory;
    }

    
    
    @Override
    public String toString(){
        return houseaddress;
    }
    
}
