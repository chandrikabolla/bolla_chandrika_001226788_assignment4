/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package City.Business;

import java.util.Date;

/**
 *
 * @author Nidhi Mittal
 */
public class VitalSigns {
    
    
    //public PatientDirectory patientDirectory;
    //private float respiratoryRate;
    //private float heartRate;
    private int systolicBloodPressure;
    private double cholestrol;
    private double ldlCholestrol;
    private double hdcCholestrol;
    private double diastolicBloodPressure;
    //private float weight;
    //private String dateCreated;
    //private Date date;
    //private int status;
    private String timestamp;
    

    public VitalSigns() {

        //patientDirectory = new PatientDirectory();
        //respiratoryRate = 0.0F;
        //heartRate = 0.0F;
        systolicBloodPressure = 0;
        cholestrol = 0.0;
        ldlCholestrol = 0.0;
        hdcCholestrol = 0.0;
        diastolicBloodPressure = 0.0;
        //weight = 0.0F;
        //dateCreated = "";
        //date = new Date();
        //status = 0;

    }

    /* Setters & Getters for all the variables in the class */
    /*public float getRespiratoryRate() {
        return respiratoryRate;
    }

    public void setRespiratoryRate(float respiratoryRate) {
        this.respiratoryRate = respiratoryRate;
    }

    public float getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(float heartRate) {
        this.heartRate = heartRate;
    }*/

    public int getSystolicBloodPressure() {
        return systolicBloodPressure;
    }

    public void setSystolicBloodPressure(int systolicBloodPressure) {
        this.systolicBloodPressure = systolicBloodPressure;
    }

    /*public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }*/

    /*public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }*/

    /*public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }*/
    
    

    /*public void setDate(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }*/

    /*public PatientDirectory getPatientDirectory() {
        return patientDirectory;
    }

    public void setPatientDirectory(PatientDirectory patientDirectory) {
        this.patientDirectory = patientDirectory;
    }*/

   
    
    

    /* Overrided function to display Date Created instead of Object
    
     Parameters: NONE
    
     Returns:
     1. Created Date - A String containing the created date */
    /*public String toString() {
        return dateCreated;
    }*/

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public double getCholestrol() {
        return cholestrol;
    }

    public void setCholestrol(double cholestrol) {
        this.cholestrol = cholestrol;
    }

    public double getLdlCholestrol() {
        return ldlCholestrol;
    }

    public void setLdlCholestrol(double ldlCholestrol) {
        this.ldlCholestrol = ldlCholestrol;
    }

    public double getHdcCholestrol() {
        return hdcCholestrol;
    }

    public void setHdcCholestrol(double hdcCholestrol) {
        this.hdcCholestrol = hdcCholestrol;
    }

    public double getDiastolicBloodPressure() {
        return diastolicBloodPressure;
    }

    public void setDiastolicBloodPressure(double diastolicBloodPressure) {
        this.diastolicBloodPressure = diastolicBloodPressure;
    }
    
    
    

}
