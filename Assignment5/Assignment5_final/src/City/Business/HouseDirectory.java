/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package City.Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Guest
 */
public class HouseDirectory {
        
    List<House> houseList;
    House house1;
    String houseaddress;

    public HouseDirectory() {
        houseList = new ArrayList<House>();
    }

    public List<House> getHouseList() {
        return houseList;
    }

    public void setHouseList(List<House> houseList) {
        this.houseList = houseList;
    }

    public House addHouse(House house) {
        this.house1=house;
        houseList.add(house);
        return house;
    }
    public House searchHouse(String houseaddress)
    {
        this.houseaddress=houseaddress;
        for(House house2 :houseList)
        {
            if(house2.getHouseaddress().equals(houseaddress))
            {
                    return house2;
            }
            
        }
        return null;
    }
    
}
