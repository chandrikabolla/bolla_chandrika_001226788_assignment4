/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package City.Business;

import java.util.ArrayList;

/**
 *
 * @author Guest
 */
public class Community {
    public String communityName;
    public int communityCode;
    public int zipCode;
    public HouseDirectory houseDirectory;
    private int numberOfPeopleatRisk;
    private int numberOfPeople;
    private int numberOfMales;
    private int numberOfMalesAtRisk;
    
    
    public Community(){
       houseDirectory=new HouseDirectory();
    }

    public int getNumberOfPeopleatRisk() {
        return numberOfPeopleatRisk;
    }

    public int getNumberOfPeople() {
        return numberOfPeople;
    }

    public void setNumberOfPeople(int numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    public void setNumberOfPeopleatRisk(int numberOfPeopleatRisk) {
        this.numberOfPeopleatRisk = numberOfPeopleatRisk;
    }

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }

    public int getCommunityCode() {
        return communityCode;
    }

    public void setCommunityCode(int communityCode) {
        this.communityCode = communityCode;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public HouseDirectory getHouseDirectory() {
        return houseDirectory;
    }

    public void setHouseDirectory(HouseDirectory houseDirectory) {
        this.houseDirectory = houseDirectory;
    }

    public int getNumberOfMales() {
        return numberOfMales;
    }

    public void setNumberOfMales(int numberOfMales) {
        this.numberOfMales = numberOfMales;
    }

    public int getNumberOfMalesAtRisk() {
        return numberOfMalesAtRisk;
    }

    public void setNumberOfMalesAtRisk(int numberOfMalesAtRisk) {
        this.numberOfMalesAtRisk = numberOfMalesAtRisk;
    }

    
    
    
    @Override
    public String toString(){
        return communityName;
    }
    
}
