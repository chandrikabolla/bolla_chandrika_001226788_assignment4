/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package City.Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Guest
 */
public class FamilyDirectory {
        
    List<Family> familyList;
    Family family1;
    String familyName;

    public FamilyDirectory() {
        familyList = new ArrayList<Family>();
    }

    public List<Family> getFamilyList() {
        return familyList;
    }

    public void setFamilyList(List<Family> familyList) {
        this.familyList = familyList;
    }

    public Family addFamily(Family family) {
        this.family1=family;
        familyList.add(family);
        return family;
    }
    public Family searchFamily(String familyname)
    {
        this.familyName=familyname;
        for(Family family2 :familyList)
        {
            if(family2.getFamilyName().equals(familyname))
            {
                    return family2;
            }
            
        }
        return null;
    }
    
}
