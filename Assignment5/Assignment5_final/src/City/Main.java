/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package City;

import City.Business.Community;
import City.Business.CommunityDirectory;
import City.Business.Family;
import City.Business.House;
import City.Business.Patient;
import Initialization.CityInitialization;
<<<<<<< HEAD
import static Initialization.FamilyInitialization.familyDirectory;
=======
>>>>>>> 43df4c488983164e4c4197f093fdeaeb9e2b2a9b
import java.util.Scanner;

/**
 *
 * @author Guest
 */
public class Main {
    public static CommunityDirectory communityDirectory;
    public static void main(String args[]) {
         communityDirectory=Initialization.CityInitialization.initializecity();
        
       for(Community community:communityDirectory.getCommunityList())
       {
       
        System.out.println(community.getHouseDirectory().getHouseList().get(0).getHouseaddress());
       for(House house:community.getHouseDirectory().getHouseList())
       {
           System.out.println("---------------------------------------------------------------------------------------------------------");
           System.out.println("House number: "+house.getHouseNumber()+"\nHouse Address: "+house.getHouseaddress());
           for(Family family:house.getFamilyDirectory().getFamilyList())
           {
               System.out.println("*******************************************************************************************************");
               System.out.println("Family name: "+family.getFamilyName()+"\nFamily address: "+family.getFamilyAddress()+"Number of patients :"+family.getPatientDirectory().getPatientList().size());
               System.out.println("No of grand parents: "+family.getGrandParents().size());
               for(Patient patient:family.getGrandParents())
               {
                   System.out.println("Patient name: "+patient.getFirstName()+" "+patient.getLastName()+"  Age:"+patient.getAge()+" Gender: "+patient.getGender()+
                           " LDL risk"+patient.getLDLRisk()+" Chol risk"+patient.getCholRisk());
                   
               }
               System.out.println("Number of Parents: "+family.getParents().size());
               for(Patient patient:family.getParents())
                {
                   System.out.println("Patient name: "+patient.getFirstName()+" "+patient.getLastName()+"  Age:"+patient.getAge()+" Gender: "+patient.getGender()+
                           " LDL risk"+patient.getLDLRisk()+" Chol risk"+patient.getCholRisk());
                }
              
               
               
               
               System.out.println("Number of individuals: "+ family.getIndividuals().size());
               for(Patient patient:family.getIndividuals())
               {
                   System.out.println("Patient name: "+patient.getFirstName()+" "+patient.getLastName()+"  Age:"+patient.getAge()+" Gender: "+patient.getGender()+
                           " LDL risk"+patient.getLDLRisk()+" Chol risk"+patient.getCholRisk());
               }
               
               System.out.println("Number of Children: "+family.getChildren().size());
               for(Patient patient:family.getChildren())
               {
                   System.out.println("Patient name: "+patient.getFirstName()+" "+patient.getLastName()+"  Age:"+patient.getAge()+" Gender: "+patient.getGender());
               }
               
               
               
                System.out.println("*******************************************************************************************************");
               
           }
           System.out.println("---------------------------------------------------------------------------------------------------------");
           
       }
       }

        int choice;
        String more;

        do {
            System.out.println("********* Welcome to Framingham Heart Study *************** ");

            System.out.println("1.Enter your data to calculate risk factor");
            System.out.println("2.View person level report");
            System.out.println("3.View family level report");
            System.out.println("4.View house level report");
            System.out.println("5.View community level report");
            System.out.println("6.View city level report");

            Scanner reader = new Scanner(System.in);  // Reading from System.in
            System.out.println("Enter a choice.");
            choice = reader.nextInt(); // Scans the next token of the input as an int.

            switch (choice) {
                case 1:
                    Patient patient = new Patient();

                    System.out.println("Enter your First Name: ");
                    reader = new Scanner(System.in);
                    patient.setFirstName(reader.next());

                    System.out.println("Enter your Last Name: ");
                    patient.setLastName(reader.next());

                    System.out.println("Enter your age (between 30 and 74): ");
                    patient.setAge(reader.nextInt());

                    System.out.println("Enter your gender (M/F): ");
                    patient.setGender(reader.next());

                    System.out.println("Do you smoke (Y/N): ");
                    String smoking = reader.next();
                    if (smoking.equalsIgnoreCase("Y")) {
                        patient.setSmoking(true);
                    } else {
                        patient.setSmoking(false);
                    }

                    System.out.println("Do you have diabetes (Y/N): ");
                    String diabetes = reader.next();
                    if (diabetes.equalsIgnoreCase("Y")) {
                        patient.setDiabetes(true);
                    } else {
                        patient.setDiabetes(false);
                    }

                    System.out.println("Enter Systolic Blood Pressure in mm Hg (integer value between 90 and 200): ");
                    patient.setAvgSystolicBloodPressure(reader.nextInt());

                    System.out.println("Enter Diastolic Blood Pressure in mm Hg (integer value between 40 and 140): ");
                    patient.setAvgDiastolicBloodPressure(reader.nextInt());

                    System.out.println("Enter Total Cholestrol value in mmol/L (real value between 3.6 and 10.3): ");
                    patient.setAvgCholestrol(reader.nextDouble());

                    System.out.println("Enter LDL Cholestrol value in mmol/L (real value between 0.8 and 4.0): ");
                    patient.setAvgLDCholestrol(reader.nextDouble());

                    System.out.println("Enter HDL Cholestrol value in mmol/L (real value between 0.8 and 4.0): ");
                    patient.setAvgHDCholestrol(reader.nextDouble());

                    /*System.out.println(patient.getFirstName() + "\n" + patient.getLastName() + "\n" + patient.getAge() + "\n" +
                            patient.getGender() + "\n" + patient.isSmoking() + "\n" + patient.isDiabetes() + "\n" +
                    patient.getAvgSystolicBloodPressure() + "\n" + patient.getAvgDiastolicBloodPressure() + "\n" + 
                            patient.getAvgCholestrol() + "\n" + patient.getAvgHDCholestrol() + "\n" + patient.getAvgLDCholestrol());*/
                  patient.calculateRisk();

                    System.out.println("Your Framingham Risk Score from LDL points is: " + patient.getRiskFromLDLPoints() + "%");
                    System.out.println("Your Framingham Risk Score from Chol points is: " + patient.getRiskFromCholPoints()+ "%");

                    break;

                case 2:

                    break;

                case 3:
                    break;
                case 4:
                    break;
                case 5:
                    
<<<<<<< HEAD
                    CommunityLevelReport();
=======
                    
>>>>>>> 43df4c488983164e4c4197f093fdeaeb9e2b2a9b
                    break;
                    
                    
                case 6:
                    CityLevelreport();
                    
                    
                    break;
                default:
                    System.out.println("INPUT ENTERED IS INCORRECT");
                    break;
            }

            System.out.println("\nWant to see more (Y/N)");
            Scanner readMore = new Scanner(System.in);  // Reading from System.in
            more = readMore.next();
        } while (more.equalsIgnoreCase("Y"));


    }
    public static void CityLevelreport(){
        
        //1.How many people have a risk of getting heart disease from overall population(1000)
        System.out.println("1.How many people have a risk of getting heart disease from overall population(1000)");
        int numberOfPatientsAtRisk=0;
        int numberOfPeople=0;
        for(Community community:communityDirectory.getCommunityList())
        {
            for(House house:community.getHouseDirectory().getHouseList())
            {
                for(Family family:house.getFamilyDirectory().getFamilyList())
                {
                    for(Patient patient:family.getPatientDirectory().getPatientList())
                    {
                       if(patient.isAtRisk())
                       {
                           
                           numberOfPatientsAtRisk++;
                           
                       }
                       numberOfPeople++;
                    }
                }
            }
        }
        System.out.println("Number of people in the city : "+numberOfPeople);
        System.out.println("Number of People at risk of getting a heart disease in Boston :  "+numberOfPatientsAtRisk);
        
        //2.How many people of age between age 30 to 40 are at risk of getting heart disease
        System.out.println("How many people of age between age 30 to 40 are at risk of getting heart disease");
        int number2=0;
        int numberBTW=0;
        for(Community community:communityDirectory.getCommunityList())
        {
            for(House house:community.getHouseDirectory().getHouseList())
            {
                for(Family family:house.getFamilyDirectory().getFamilyList())
                {
                    for(Patient patient:family.getPatientDirectory().getPatientList())
                    {
                        if((patient.getAge()>=30&&patient.getAge()<=40))
                        {
                            if(patient.isAtRisk())
                            {
                           
                                number2++;
                           
                            }
                       numberBTW++;
                        }
                    }
                }
            }
        }
         System.out.println("Number of people with age between 30 and 40 in the city : "+numberBTW);
        System.out.println("Number of People at risk of getting a heart disease in them :  "+number2);
        
        
       //3.Percentage of women that has risk of getting heart disease from over all population
       int numberOfwomen=0;
        int numberOfWomenAtRisk=0;
        for(Community community:communityDirectory.getCommunityList())
        {
            for(House house:community.getHouseDirectory().getHouseList())
            {
                for(Family family:house.getFamilyDirectory().getFamilyList())
                {
                    for(Patient patient:family.getPatientDirectory().getPatientList())
                    {
                        if(patient.getGender().equalsIgnoreCase("female"))
                        {
                            if(patient.isAtRisk())
                            {
                           
                                numberOfWomenAtRisk++;
                           
                            }
                       numberOfwomen++;
                        }
                    }
                }
            }
        }
        double PercentageOfwomenAtRisk=0.0;
        PercentageOfwomenAtRisk=(double)numberOfWomenAtRisk/numberOfwomen;
        PercentageOfwomenAtRisk=PercentageOfwomenAtRisk*100.0;
         System.out.println("Number of Women in the city : "+numberOfwomen);
         System.out.println("Number of women at risk : "+numberOfWomenAtRisk);
        System.out.println("Number of Women at risk of getting a heart disease in them :  "+PercentageOfwomenAtRisk);
        
       
        
        
        
        
        
    }
<<<<<<< HEAD
    public static void CommunityLevelReport()
    {
        System.out.println("--------------------------------------------------------1 st question-----------------------------------------------------------------------");
        //1.Name the community that has the highest risk of getting heart disease
        System.out.println("Name the community that the has the highest risk of getting a heart disease?  ");
        Community highRiskCommunity=communityDirectory.getCommunityList().get(0);
        int numberOfPeopleatRisk=0;
        int numberOfPeople=0;
        for(Community community:communityDirectory.getCommunityList())
        {
            for(House house:community.getHouseDirectory().getHouseList())
            {
                for(Family family:house.getFamilyDirectory().getFamilyList())
                {
                    for(Patient patient:family.getPatientDirectory().getPatientList())
                    {
                       
                        
                            if(patient.isAtRisk())
                            {
                           
                                numberOfPeopleatRisk++;
                                
                           
                            }
                       numberOfPeople++;
                       
                        
                    }
                }
            }
            community.setNumberOfPeopleatRisk(numberOfPeopleatRisk);
            community.setNumberOfPeople(numberOfPeople);
            if(community.getNumberOfPeopleatRisk()>highRiskCommunity.getNumberOfPeopleatRisk())
            {
                highRiskCommunity=community;
            }
        }
        for(Community community:communityDirectory.getCommunityList())
        {
         System.out.println("\nName of community : "+community.getCommunityName()+"\nNumber of Women in the city : "+community.getNumberOfPeople());
         System.out.println("\nNumber of people at risk : "+community.getNumberOfPeopleatRisk());
        }
        System.out.println("\nCommunity that has highest risk of getting heart disease : "+ highRiskCommunity.getCommunityName()+"----------Number of people at risk : "+highRiskCommunity.getNumberOfPeopleatRisk());
        System.out.println("--------------------------------------------------------2 nd question-----------------------------------------------------------------------");

        //2.In which community there are more males whose blood pressure is high
        System.out.println("In which community there are more Males whose blood pressure is high ");
        Community maleHighRiskCommunity=communityDirectory.getCommunityList().get(0);
        int numberOfMalesatRisk=0;
        int numberOfMales=0;
        for(Community community:communityDirectory.getCommunityList())
        {
            for(House house:community.getHouseDirectory().getHouseList())
            {
                for(Family family:house.getFamilyDirectory().getFamilyList())
                {
                    for(Patient patient:family.getPatientDirectory().getPatientList())
                    {
                       
                        if(patient.getGender().equalsIgnoreCase("female"))
                        {
                            if(patient.isAtRisk())
                            {
                           
                                numberOfMalesatRisk++;
                                
                           
                            }
                       numberOfMales++;
                       
                        }
                        
                    }
                }
            }
            community.setNumberOfMalesAtRisk(numberOfMalesatRisk);
            community.setNumberOfMales(numberOfMales);
            if(community.getNumberOfMalesAtRisk()>highRiskCommunity.getNumberOfMalesAtRisk())
            {
                highRiskCommunity=community;
            }
        }
        for(Community community:communityDirectory.getCommunityList())
        {
         System.out.println("\nName of community : "+community.getCommunityName()+"\nNumber of Males in the city : "+community.getNumberOfMales());
         System.out.println("\nNumber of Males at risk : "+community.getNumberOfMalesAtRisk());
        }
        System.out.println("\nCommunity that has highest risk of getting heart disease : "+ highRiskCommunity.getCommunityName()+"-----------------Number of Males at risk : "+highRiskCommunity.getNumberOfMalesAtRisk());
        
        
        
        
        
        
        
        
        
        
        //3.Name all the communities where population of men having risk of getting heart disease than women is greater
        System.out.println(" Name all the communities where population of men having risk of getting heart disease than women is greater? ");
        
        
        
    }
  
     public static void HouseLevelReport()
    {
        
        //1.In how many houses there are patients above age 60 are sufferring with diabetes
        
System.out.println(" In how many Houses there are patients above age 60 are suffering with diabetes ");
 int number9=0;
        int numberBTW=0;
        for(Community community:communityDirectory.getCommunityList())
        {
            for(House house:community.getHouseDirectory().getHouseList())
            {
                for(Family family:house.getFamilyDirectory().getFamilyList())
                {
                    for(Patient patient:family.getPatientDirectory().getPatientList())
                    {
                        if(patient.getAge()>=60)
                        {
                            if(patient.isAtRisk())
                            {
                           
                                number9++;
                           
                            }
                       numberBTW++;
                        }
                    }
                }
            }
        }
         //System.out.println("Number of people with age between 30 and 40 in the city : "+numberBTW);
        System.out.println("Number of houses in which People are above 60 and arediabetic at risk of getting a heart disease is :  "+number9);
        
       





        //2.Count how many houses are there in the highest heart disease prone community
        System.out.println(" What are the total number of houses in the highest heart disease prone community");
        
        //3.Total number of houses that contains atleast one person between age 12-35 who smokes
        System.out.println(" Total number of houses that contains atleast one person between age 12-35 who smokes");

     
    }
     public static void FamilyLevelReport()
    {
        //1.Name the top family that is prone to heart disease wheer the member of the family do not smoke and are not diabetic
        
System.out.println(" Name the top family that is prone to heart disease where the member of the family do not smoke and are not diabetic? ");
         
        Family highRiskFamily=familyDirectory.getFamilyList().getFamilyName();
        int numberOfFamilyatRisk=0;
        int numberOfFamily=0;
        for(Community community:communityDirectory.getCommunityList())
        {
            for(House house:community.getHouseDirectory().getHouseList())
            {
                for(Family family:house.getFamilyDirectory().getFamilyList())
                {
                    for(Patient patient:family.getPatientDirectory().getPatientList())
                    {
                        
                            if((patient.isDiabetes())&&(patient.isSmoking()))
                            {
                            if(patient.isAtRisk())
                            {
                           
                                numberOfFamilyatRisk++;
                                
                           
                            }
                       numberOfFamily++;
                            }
                        
                        
                
            family.setNumberOfFamilyAtRisk(numberOfFamilyatRisk);
            family.setNumberOfFamily(numberOfFamily);
            if(family.getNumberOfFamilyatRisk()>family.getNumberOfFamilyAtRisk())
            {
                highRiskFamily=family;
            }
        }}}}
        for(Family family:familyDirectory.getFamilyList())
        {
         System.out.println("\nName of family : "+family.getFamilyName());
         System.out.println("\nNumber of family at risk : "+family.getNumberOfFamilyAtRisk());
        }
        System.out.println("\nFamily that has highest risk of getting heart disease : "+ highRiskFamily.getFamilyName());
        
        
        //2.Total Number of families in the city that contains non smokes, non diabetic patients
        
        System.out.println(" Total Number of families in the city that are at high risk and contains non smokes, non diabetic patients?");
        
        //3.how many families contains childern of age 9-17 that smoke
        
System.out.println(" How many families contain children of age 9-17 that smoke and are at high risk? ");

        
    }
      public static void PersonLevelReport()
    {
        //1.Give the ratio of women and men whose age is 20-60 are at risk?
        System.out.println(" Give the ratio of women and men whose age is 20-60 are at risk? ");
        
        
        
        //2.Total Number of people above Age 55 and are diabetic are at risk
System.out.println(" Total Number of people above Age 55 and are diabetic are at risk?");

//3. State percentage of male, female, children have risk of getting heart disease?
System.out.println("  State percentage of male, female, children have risk of getting heart disease? ");
       
    }
=======

>>>>>>> 43df4c488983164e4c4197f093fdeaeb9e2b2a9b
}
