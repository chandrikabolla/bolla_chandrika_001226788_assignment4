/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Business;

import java.util.ArrayList;

/**
 *
 * @author Guest
 */
public class Department {
    private String departmentName;
    private int departmentCode;
    private ArrayList<Degree> degreeList;
   
   private DepartmentStudentDirectory departmentStudentDirectory;
    private DepartmentCourseCatalog courseCatalog;
    
    public void Department(){
        degreeList=new ArrayList<Degree>();
        
        departmentStudentDirectory=new DepartmentStudentDirectory();
    }


    public ArrayList<Degree> getDegreeList() {
        return degreeList;
    }

    public void setDegreeList(ArrayList<Degree> degreeList) {
        this.degreeList = degreeList;
        
    }

    

    public DepartmentStudentDirectory getDepartmentStudentDirectory() {
    return departmentStudentDirectory;
    }
    public void setDepartmentStudentDirectory(DepartmentStudentDirectory departmentStudentDirectory) {
    this.departmentStudentDirectory = departmentStudentDirectory;
    }
    

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public int getDepartmentCode() {
        return departmentCode;
    }

    public void setDepartmentCode(int departmentCode) {
        this.departmentCode = departmentCode;
    }

    public DepartmentCourseCatalog getCourseCatalog() {
        return courseCatalog;
    }

    public void setCourseCatalog(DepartmentCourseCatalog courseCatalog) {
        this.courseCatalog = courseCatalog;
    }
    
    
}
