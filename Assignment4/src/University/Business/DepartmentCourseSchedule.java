/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Business;

import University.*;
import java.util.ArrayList;

/**
 *
 * @author Nidhi Mittal <mittal.n@husky.neu.edu>
 */
public class DepartmentCourseSchedule {

    private ArrayList<CourseOffering> departmentCourseSchedule;

    public DepartmentCourseSchedule() {
        departmentCourseSchedule = new ArrayList<CourseOffering>();
    }

    public ArrayList<CourseOffering> getDepartmentCourseSchedule() {
        return departmentCourseSchedule;
    }

    public void setDepartmentCourseSchedule(ArrayList<CourseOffering> departmentCourseSchedule) {
        this.departmentCourseSchedule = departmentCourseSchedule;
    }
    
    public CourseOffering addCourseOffering() {
        CourseOffering courseOffering = new CourseOffering();
        departmentCourseSchedule.add(courseOffering);
        return courseOffering;
    }

    public  ArrayList<CourseOffering> courseOfferingWithLessThanFiftyPercentEnrollment() {
        ArrayList<CourseOffering> lessEnrolledCourseOfferingList = new ArrayList<CourseOffering>();
        for (CourseOffering courseOffering : departmentCourseSchedule) {
            if (courseOffering.getEnrollmentPercentage() < 50) {
               lessEnrolledCourseOfferingList.add(courseOffering);
            }
        }
        return lessEnrolledCourseOfferingList;
    }

    public double getDepartmentEarning() {
        double totalEarnings = 0;
        for (CourseOffering courseOffering : departmentCourseSchedule) {
            totalEarnings = totalEarnings + courseOffering.getCourseEarning();
        }
        return totalEarnings;
    }
    
}
