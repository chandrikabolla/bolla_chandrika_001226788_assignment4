/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Guest
 */
public class DepartmentStudentDirectory {
  
      //private Department departmentName;
    //private Department departmentCode;
  // private Student student;
   
       Student student;
     ArrayList<Student> studentList;

    public DepartmentStudentDirectory() {
        studentList = new ArrayList<Student>();
        student=new Student();
    }

    public ArrayList<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(ArrayList<Student> studentList) {
        this.studentList = studentList;
    }

    public Student addStudent(Student student) {
       this.student=student;
        studentList.add(student);
        return student;
       
    }
}
