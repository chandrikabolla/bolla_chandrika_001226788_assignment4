/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Business;

import java.util.ArrayList;

/**
 *
 * @author chand
 */
public class ClassRoomDirectory {
    
    private ArrayList<ClassRoom> classRoomList;
    private ClassRoom classRoom1;
    
    
    public void ClassRoomDirectory(){
        classRoomList=new ArrayList<ClassRoom>();
    }

    public ArrayList<ClassRoom> getClassRoomList() {
        return classRoomList;
    }

    public void setClassRoomList(ArrayList<ClassRoom> classRoomList) {
        this.classRoomList = classRoomList;
    }
    public ClassRoom addClassRoom(ClassRoom classRoom){
        this.classRoom1=classRoom;
        classRoomList.add(classRoom1);
        return classRoom;
        
    }
    public ClassRoom searchClassRoom(String classRoomNumber)
    {
        for(ClassRoom classRoom:classRoomList)
        {
            if(classRoom.getRoomNumber().equalsIgnoreCase(classRoomNumber))
            {
                return classRoom;
            }
        }
        return null;
    }
    
    
}
