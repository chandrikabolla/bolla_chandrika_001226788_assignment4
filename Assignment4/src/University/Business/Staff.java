/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Business;

/**
 *
 * @author chand
 */
public class Staff extends Role{
    
    Person person;
    String role1;
    JobPosition position;
    public Staff(String role){
        super(role);
        person=new Person();
        position=new JobPosition();
        
        
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    

    public String getRole1() {
        return role;
    }

    public void setRole1(String role) {
        this.role1 = role;
    }

    public JobPosition getPosition() {
        return position;
    }

    public void setPosition(JobPosition position) {
        this.position = position;
    }
    
    
}
