/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Guest
 */
public class DepartmentDirectory {
       ArrayList<Department> departmentList;
   

    public DepartmentDirectory() {
        departmentList = new ArrayList<Department>();
    }

    public ArrayList<Department> getDepartmentList() {
        return departmentList;
    }

    public void setDepartmentList(ArrayList<Department> departmentList) {
        this.departmentList = departmentList;
    }
    
    public Department searchDepartment(String departmentName)
    {
        for(Department department:departmentList)
        {
            if(department.getDepartmentName().equalsIgnoreCase(departmentName))
            {
            return department;
        }
            
    }return null;
    }

    
}
