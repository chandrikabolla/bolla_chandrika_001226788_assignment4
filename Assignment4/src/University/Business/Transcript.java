/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Business;

import java.util.ArrayList;

/**
 *
 * @author Guest
 */
public class Transcript {
  
    private Student student;
    private ArrayList<CourseLoad> transcript;
    private double totalGPA=0;
    
    public Transcript(){
        transcript=new ArrayList<CourseLoad>();
    }

    public Student getStudentId() {
        return student;
    }

    public void setStudentId(Student studentId) {
        this.student = studentId;
    }

    public ArrayList<CourseLoad> getTranscript() {
        return transcript;
    }

    public void setTranscript(ArrayList<CourseLoad> transcript) {
        this.transcript = transcript;
    }
    
    
    public void setCourseLoadList(ArrayList<CourseLoad> courseLoadList){
        
        this.transcript=courseLoadList;
     
    }
     public ArrayList<CourseLoad> getCourseLoadList(){
        
        
       return this.transcript;
    }
   public double calculateGPA(){
   
    int count=0;
     for(CourseLoad courseLoad:transcript)
       {
           count++;
       }
          
     if(count!=0)
     {
        for(CourseLoad courseLoad:transcript)
       {
           totalGPA=totalGPA+courseLoad.getSemesterGPA();
       }
       
     }
        totalGPA=totalGPA/count;
       return totalGPA;
   }
   
}
