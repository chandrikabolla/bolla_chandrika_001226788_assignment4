/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Business;

import University.*;
import java.util.ArrayList;

/**
 *
 * @author Nidhi Mittal <mittal.n@husky.neu.edu>
 */
public class DepartmentCourseCatalog {

    private ArrayList<Course> departmentCourseList;

    public DepartmentCourseCatalog() {
        departmentCourseList = new ArrayList<Course>();
    }

    public ArrayList<Course> getDepartmentCourseList() {
        return departmentCourseList;
    }

    public void setDepartmentCourseList(ArrayList<Course> departmentCourseCatalog) {
        this.departmentCourseList = departmentCourseCatalog;
    }

    public Course addCourse() {
        Course course = new Course();
        departmentCourseList.add(course);
        return course;
    }

    public void deleteCourse(Course course) {
        departmentCourseList.remove(course);
    }

    public Course findCourseByCourseNumber(String courseNumber) {
        for (Course course : departmentCourseList) {
            if (courseNumber.equals(course.getCourseNumber())) {
                return course;
            }

        }
        return null;
    }
}