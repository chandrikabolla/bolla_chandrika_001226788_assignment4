/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Business;

import java.util.ArrayList;

/**
 *
 * @author Guest
 */
public class College {
    private String collegeName;
 private int collegeCode;
 private CollegeStudentDirectory collegeStudentDirectory;
 private ArrayList<Department> departmentList;
 
    
 public void College(){
     departmentList=new ArrayList<Department>();
     collegeStudentDirectory=new CollegeStudentDirectory();
 }
    public CollegeStudentDirectory getCollegeStudentDirectory() {
        return collegeStudentDirectory;
    }

    public void setCollegeStudentDirectory(CollegeStudentDirectory collegeStudentDirectory) {
        this.collegeStudentDirectory = collegeStudentDirectory;
    }
    
    public String getCollegeName() {
        return collegeName;
    }

    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName;
    }

    public int getCollegeCode() {
        return collegeCode;
    }

    public void setCollegeCode(int collegeCode) {
        this.collegeCode = collegeCode;
    }

    public ArrayList<Department> getDepartmentList() {
        return departmentList;
    }

    public void setDepartmentList(ArrayList<Department> departmentList) {
        this.departmentList = departmentList;
    }
    
    
}
