/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Guest
 */
public class StaffDirectory {
    
    List<Staff> staffList;
    Staff staff1;
    String staffName;

    public StaffDirectory() {
        staffList = new ArrayList<Staff>();
    }

    public List<Staff> getStaffList() {
        return staffList;
    }

    public void setStaffList(List<Staff> staffList) {
        this.staffList = staffList;
    }

    public Staff addStaff(Staff staff) {
        this.staff1=staff;
        staffList.add(staff);
        return staff;
    }
    public Staff searchStaff(String staffname)
    {
        this.staffName=staffname;
        for(Staff staff2 :staffList)
        {
            if(staff2.getPerson().equals(staffname))
            {
                    return staff2;
            }
            
        }
        return null;
    }
}

