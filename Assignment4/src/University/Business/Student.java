/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Business;

/**
 *
 * @author Guest
 */
public class Student {
    private String studentName;
    private String studentId;
    private String email;
    private String phoneNumber;
    private String affiliatedUniversity;
    private String affiliatedDepartment;
    private String affiliatedCollege;
    private double gpa;
    private StudentAccount studentAccount;
    private Transcript transcript; 

    public StudentAccount getStudentAccount() {
        return studentAccount;
    }

    public void setStudentAccount(StudentAccount studentAccount) {
        this.studentAccount = studentAccount;
    }
    
    
    
    public double getGpa() {
        return gpa;
    }

    public void setGpa(double gpa) {
        this.gpa = gpa;
    }

   

    public String getAffiliatedUniversity() {
        return affiliatedUniversity;
    }

    public void setAffiliatedUniversity(String affiliatedUniversity) {
        this.affiliatedUniversity = affiliatedUniversity;
    }

    public String getAffiliatedDepartment() {
        return affiliatedDepartment;
    }

    public void setAffiliatedDepartment(String affiliatedDepartment) {
        this.affiliatedDepartment = affiliatedDepartment;
    }

    public String getAffiliatedCollege() {
        return affiliatedCollege;
    }

    public void setAffiliatedCollege(String affiliatedCollege) {
        this.affiliatedCollege = affiliatedCollege;
    }
    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public Transcript getTranscript()
    {
        return transcript;
    }
    public void setTranscript(Transcript transcript){
        this.transcript=transcript;
    }
    @Override
    public String toString(){
        return studentId;
    }
}
