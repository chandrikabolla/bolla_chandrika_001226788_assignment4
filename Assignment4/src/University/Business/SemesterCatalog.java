/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Business;

import java.util.ArrayList;

/**
 *
 * @author chand
 */
public class SemesterCatalog {
    
    private String semesterName;
    private ArrayList<Semester> semesterList;
    
    public void SemseterCatalog(){
        semesterList=new ArrayList<Semester>();
    }

    public ArrayList<Semester> getSemesterList() {
        return semesterList;
    }

    public void setSemesterList(ArrayList<Semester> semesterList) {
        this.semesterList = semesterList;
    }
    public Semester searchSemester(String semesterName)
    {
        this.semesterName=semesterName;
        for(Semester semester :semesterList)
        {
            if(semester.getSemesterName().equals(semesterName))
            {
                    return semester;
            }
            
        }
        return null;
    }
    
    
}
