/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Guest
 */
public class TeacherDirectory {
    
    List<Teacher> teacherList;
    Teacher teacher1;
    String teacherName;

    public TeacherDirectory() {
        teacherList = new ArrayList<Teacher>();
    }

    public List<Teacher> getTeacherList() {
        return teacherList;
    }

    public void setTeacherList(List<Teacher> teacherList) {
        this.teacherList = teacherList;
    }

    public Teacher addTeacher(Teacher teacher) {
        this.teacher1=teacher;
        teacherList.add(teacher);
        return teacher;
    }
    public Teacher searchTeacher(String teachername)
    {
        this.teacherName=teachername;
        for(Teacher teacher2 :teacherList)
        {
            if(teacher2.getPerson().getFirstName().equals(teachername))
            {
                    return teacher2;
            }
            
        }
        return null;
    }
}
