/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Business;

import University.*;

/**
 *
 * @author Nidhi Mittal <mittal.n@husky.neu.edu>
 */
public class ClassRoom {

    private String roomNumber;
    private String buildingNumber;

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(String buildingNumber) {
        this.buildingNumber = buildingNumber;
    }
    @Override
    public String toString(){
        return this.roomNumber+" "+this.buildingNumber;
    }
}
