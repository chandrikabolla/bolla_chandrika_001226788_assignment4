/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Initialization;


import University.Business.ClassRoom;
import University.Business.ClassRoomDirectory;
import java.util.ArrayList;

/**
 *
 * @author chand
 */
public class ClassRoomInitialization {
    static ClassRoom classRoom1;
    static ClassRoom classRoom2;
    static ClassRoomDirectory classRoomDirectory;
    static ArrayList<ClassRoom> classRoomList;
    
    public static ClassRoomDirectory initializeClassRooms(){
        
        classRoomDirectory=new ClassRoomDirectory();
        classRoomList=new ArrayList<ClassRoom>();
        classRoom1 = new ClassRoom();
        classRoom1.setRoomNumber("315");
        classRoom1.setBuildingNumber("West Village F20");
        classRoomList.add(classRoom1);
        
        
        classRoom2 = new ClassRoom();
        classRoom2.setRoomNumber("208");
        classRoom2.setBuildingNumber("Snell Engineering Center");
        classRoomList.add(classRoom2);
        //classRoomDirectory.addClassRoom(classRoom2);
        classRoomDirectory.setClassRoomList(classRoomList);
        return classRoomDirectory;
    }
    
}
