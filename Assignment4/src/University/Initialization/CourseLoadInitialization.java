/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Initialization;
import University.Business.CourseLoad;
import University.Business.SeatAssignment;
import University.Business.Transcript;
import java.util.ArrayList;

/**
 *
 * @author Guest
 */
public class CourseLoadInitialization {
  static SeatAssignment seatAssignment1;
   static SeatAssignment seatAssignment2,seatAssignment3,seatAssignment4;  
   public static CourseLoad fallCourseLoad,springCourseLoad;
   public static ArrayList<CourseLoad> courseLoadList;
   static ArrayList<SeatAssignment> coursesCompleted;
   static Transcript transcript;
     
     public static Transcript  initializeCourseload()
    {
    coursesCompleted=new ArrayList<SeatAssignment>();
    courseLoadList=new ArrayList<CourseLoad>();
    seatAssignment1= new SeatAssignment();
    seatAssignment1.setCourseFees(2000);
    seatAssignment1.setStudentGrade(3.6);
    coursesCompleted.add(seatAssignment1);
    seatAssignment2= new SeatAssignment();
    seatAssignment2.setCourseFees(1000);
    seatAssignment2.setStudentGrade(3.4);
    coursesCompleted.add(seatAssignment2);
    fallCourseLoad= new CourseLoad();
    fallCourseLoad.calculateSemesterCost(coursesCompleted);
    fallCourseLoad.setListOfCoursesCompleted(coursesCompleted);
    fallCourseLoad.setSemesterGPA(coursesCompleted);
    courseLoadList.add(fallCourseLoad);
    
    coursesCompleted=new ArrayList<SeatAssignment>();
    seatAssignment3= new SeatAssignment();
    seatAssignment3.setCourseFees(2000);
    seatAssignment3.setStudentGrade(3.6);
    coursesCompleted.add(seatAssignment3);
    seatAssignment4= new SeatAssignment();
    seatAssignment4.setCourseFees(1000);
    seatAssignment4.setStudentGrade(3.4);
    coursesCompleted.add(seatAssignment4);
    springCourseLoad= new CourseLoad();
    springCourseLoad.calculateSemesterCost(coursesCompleted);
    springCourseLoad.setListOfCoursesCompleted(coursesCompleted);
    springCourseLoad.setSemesterGPA(coursesCompleted);
    courseLoadList.add(springCourseLoad);
    
   transcript=new Transcript();
   transcript.setCourseLoadList(courseLoadList);
   
   return transcript;
    
    }
    
}
