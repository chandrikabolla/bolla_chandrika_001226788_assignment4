/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Initialization;

import University.Business.ClassRoom;
import University.Business.CourseOffering;
import University.Business.DepartmentCourseSchedule;
import University.Business.JobPosition;
import University.Business.Person;
import University.Business.Semester;
import University.Business.Teacher;
import static Initialization.TeacherStaffInitialization.initializeTeacher;
import static Initialization.TeacherStaffInitialization.teacher;

/**
 *
 * @author Nidhi Mittal <mittal.n@husky.neu.edu>
 */
public class InitializeCourseSchedule {
    
 
    public static DepartmentCourseSchedule departmentCourseSchedule;
    static CourseOffering courseOffering1;
    static CourseOffering courseOffering2;
    
    
    
    public static DepartmentCourseSchedule initializeCourseSchedule() {
        
        
        
        
        
        departmentCourseSchedule = new DepartmentCourseSchedule();
        
        courseOffering1 = departmentCourseSchedule.addCourseOffering();       
        courseOffering1.setSectionNumber("A");
        //courseOffering1.setTeacher("Nan Yang");
        //TeacherStaffInitialization initialization = new TeacherStaffInitialization();
        //initialization.initializeTeacher();
        courseOffering1.setTeacher(Initialization.TeacherStaffInitialization.initializeTeacher().searchTeacher("Chandrika"));
        courseOffering1.setCapacity(250);
        courseOffering1.setSeatsFilled(180);
        courseOffering1.setTiming("Monday 8am - 11:30am");
        courseOffering1.setClassRoom(Initialization.ClassRoomInitialization.initializeClassRooms().searchClassRoom("315"));
        courseOffering1.setSemester(Initialization.SemesterInitialization.initializeSemester().searchSemester("Fall 2016"));
        courseOffering1.setCourse(InitializeCourseCatalog.course1);
        
        courseOffering2 = departmentCourseSchedule.addCourseOffering();
        courseOffering2.setSectionNumber("b");
        //courseOffering2.setTeacher("Neeraj sing Rajput");
        courseOffering2.setTeacher(Initialization.TeacherStaffInitialization.initializeTeacher().searchTeacher("Kal"));
        courseOffering2.setCapacity(150);
        courseOffering2.setSeatsFilled(90);
        courseOffering2.setTiming("Friday 8am - 11:30am");
        courseOffering2.setClassRoom(Initialization.ClassRoomInitialization.initializeClassRooms().searchClassRoom("315"));
        courseOffering2.setSemester(Initialization.SemesterInitialization.initializeSemester().searchSemester("Fall 2016"));
        courseOffering2.setCourse(InitializeCourseCatalog.course2);
        
        return departmentCourseSchedule;
        
    }
    
}
