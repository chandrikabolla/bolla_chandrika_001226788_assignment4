/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Initialization;
import static Initialization.StudentInitialization.student;
import University.Business.StudentAccount;

/**
 *
 * @author Guest
 */
public class StudentAccountInitialization {
    static StudentAccount studentAccount;
    
    public static StudentAccount initializeStudentAccount(){
        
        studentAccount=new StudentAccount();
        studentAccount.setStudent(student);
        studentAccount.setCreditedAmount(4000);
        studentAccount.setDollarAmountOwed(0);
        studentAccount.setStatus("balanced");
        
        
        
        
        return studentAccount;
    }
    
    
}
