/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Initialization;
import University.Business.Student;
import University.Business.University;
import  static Initialization.UniversityInitialization.universityDirectory;
import  static Initialization.DepartmentInitialization.departmentDirectory;
import  static Initialization.CollegeInitialization.collegeDirectory;
import University.Business.College;
import University.Business.Department;
import University.Business.StudentDirectory;
import java.util.ArrayList;
/**
 *
 * @author Guest
 */
public class StudentInitialization {
    
    static Student student;
    static StudentDirectory studentDirectory;
    static University university;
        static Department department;
        static College college;
        static ArrayList<Student> universityStudentList,collegeStudentList,departmentStudentList;
     public static StudentDirectory initializeStudent()
    {
        studentDirectory=new StudentDirectory();
        student=new Student();
        student.setStudentName("Chandrika");
        student.setStudentId("123a");
        student.setEmail("chandrikabolla999@gmail.com");
        student.setPhoneNumber("8577076850");
        student.setAffiliatedUniversity("Northeastern University");
        student.setAffiliatedDepartment("Information Systems");
        student.setAffiliatedCollege("College of Engineering");
        student.setTranscript(Initialization.CourseLoadInitialization.initializeCourseload());
        student.setGpa(Initialization.CourseLoadInitialization.initializeCourseload().calculateGPA());
        student.setStudentAccount(Initialization.StudentAccountInitialization.initializeStudentAccount());
        studentDirectory.addStudent(student);
        university=universityDirectory.searchUniversity(student.getAffiliatedUniversity());
        university.getUniversityStudentDirectory().addStudent(student);
        college=collegeDirectory.searchCollege(student.getAffiliatedCollege());
        college.getCollegeStudentDirectory().addStudent(student);
        department=departmentDirectory.searchDepartment(student.getAffiliatedDepartment());
        department.getDepartmentStudentDirectory().getStudentList().add(student);
        
        
        
        student=new Student();
        student.setStudentName("Nidhi Mittal");
        student.setStudentId("123b");
        student.setEmail("nmnidhimittal@gmail.com");
        student.setPhoneNumber("8577076851");
        student.setAffiliatedUniversity("Northeastern University");
        student.setAffiliatedDepartment("Finance");
        student.setAffiliatedCollege("School of Business");
        student.setTranscript(Initialization.CourseLoadInitialization.initializeCourseload());
        student.setGpa(Initialization.CourseLoadInitialization.initializeCourseload().calculateGPA());
        student.setStudentAccount(Initialization.StudentAccountInitialization.initializeStudentAccount());
        studentDirectory.addStudent(student);
        university=universityDirectory.searchUniversity(student.getAffiliatedUniversity());
        university.getUniversityStudentDirectory().addStudent(student);
        college=collegeDirectory.searchCollege(student.getAffiliatedCollege());
        college.getCollegeStudentDirectory().addStudent(student);
        department=departmentDirectory.searchDepartment(student.getAffiliatedDepartment());
        department.getDepartmentStudentDirectory().getStudentList().add(student);
        
        
        
        student.setStudentName("Nayana");
        student.setStudentId("123c");
        student.setEmail("nayana11@gmail.com");
        student.setPhoneNumber("8577076847");
        student.setAffiliatedUniversity("Northeastern University");
        student.setAffiliatedDepartment("Bio Technology");
        student.setAffiliatedCollege("College of Science");
        student.setTranscript(Initialization.CourseLoadInitialization.initializeCourseload());
        student.setGpa(Initialization.CourseLoadInitialization.initializeCourseload().calculateGPA());
        student.setStudentAccount(Initialization.StudentAccountInitialization.initializeStudentAccount());
        studentDirectory.addStudent(student);
        university=universityDirectory.searchUniversity(student.getAffiliatedUniversity());
        university.getUniversityStudentDirectory().addStudent(student);
        college=collegeDirectory.searchCollege(student.getAffiliatedCollege());
        college.getCollegeStudentDirectory().addStudent(student);
        department=departmentDirectory.searchDepartment(student.getAffiliatedDepartment());
        department.getDepartmentStudentDirectory().getStudentList().add(student);
        
     //students from Boston University   
         student=new Student();
        student.setStudentName("Handrika");
        student.setStudentId("456a");
        student.setEmail("handrikabolla999@gmail.com");
        student.setPhoneNumber("2577076850");
        student.setAffiliatedUniversity("Boston University");
        student.setAffiliatedDepartment("Information Systems");
        student.setAffiliatedCollege("College of Engineering");
        student.setTranscript(Initialization.CourseLoadInitialization.initializeCourseload());
        student.setGpa(Initialization.CourseLoadInitialization.initializeCourseload().calculateGPA());
        student.setStudentAccount(Initialization.StudentAccountInitialization.initializeStudentAccount());
        studentDirectory.addStudent(student);
        
        university=universityDirectory.searchUniversity(student.getAffiliatedUniversity());
        university.getUniversityStudentDirectory().addStudent(student);
        college=collegeDirectory.searchCollege(student.getAffiliatedCollege());
        college.getCollegeStudentDirectory().addStudent(student);
        department=departmentDirectory.searchDepartment(student.getAffiliatedDepartment());
        department.getDepartmentStudentDirectory().getStudentList().add(student);
        
        
        
        student=new Student();
        student.setStudentName("Idhi Mittal");
        student.setStudentId("456b");
        student.setEmail("mnidhimittal@gmail.com");
        student.setPhoneNumber("2577076851");
        student.setAffiliatedUniversity("Boston University");
        student.setAffiliatedDepartment("Finance");
        student.setAffiliatedCollege("School of Business");
        student.setTranscript(Initialization.CourseLoadInitialization.initializeCourseload());
        student.setGpa(Initialization.CourseLoadInitialization.initializeCourseload().calculateGPA());
        student.setStudentAccount(Initialization.StudentAccountInitialization.initializeStudentAccount());
        studentDirectory.addStudent(student);
        university=universityDirectory.searchUniversity(student.getAffiliatedUniversity());
        university.getUniversityStudentDirectory().addStudent(student);
        college=collegeDirectory.searchCollege(student.getAffiliatedCollege());
        college.getCollegeStudentDirectory().addStudent(student);
        department=departmentDirectory.searchDepartment(student.getAffiliatedDepartment());
        department.getDepartmentStudentDirectory().getStudentList().add(student);
        
        
        
        student.setStudentName("Ayana");
        student.setStudentId("456c");
        student.setEmail("ayana11@gmail.com");
        student.setPhoneNumber("2577076847");
        student.setAffiliatedUniversity("Boston University");
        student.setAffiliatedDepartment("Bio Technology");
        student.setAffiliatedCollege("College of Science");
        student.setTranscript(Initialization.CourseLoadInitialization.initializeCourseload());
        student.setGpa(Initialization.CourseLoadInitialization.initializeCourseload().calculateGPA());
        student.setStudentAccount(Initialization.StudentAccountInitialization.initializeStudentAccount());
        studentDirectory.addStudent(student);
        university=universityDirectory.searchUniversity(student.getAffiliatedUniversity());
        university.getUniversityStudentDirectory().addStudent(student);
        college=collegeDirectory.searchCollege(student.getAffiliatedCollege());
        college.getCollegeStudentDirectory().addStudent(student);
        department=departmentDirectory.searchDepartment(student.getAffiliatedDepartment());
        department.getDepartmentStudentDirectory().getStudentList().add(student);
        
        


//students from harvard university
         student=new Student();
        student.setStudentName("Andrika");
        student.setStudentId("678a");
        student.setEmail("andrikabolla999@gmail.com");
        student.setPhoneNumber("3577076850");
        student.setAffiliatedUniversity("Harvard University");
        student.setAffiliatedDepartment("Information Systems");
        student.setAffiliatedCollege("College of Engineering");
        student.setTranscript(Initialization.CourseLoadInitialization.initializeCourseload());
        student.setGpa(Initialization.CourseLoadInitialization.initializeCourseload().calculateGPA());
        student.setStudentAccount(Initialization.StudentAccountInitialization.initializeStudentAccount());
        studentDirectory.addStudent(student);
        
        university=universityDirectory.searchUniversity(student.getAffiliatedUniversity());
        university.getUniversityStudentDirectory().addStudent(student);
        college=collegeDirectory.searchCollege(student.getAffiliatedCollege());
        college.getCollegeStudentDirectory().addStudent(student);
        department=departmentDirectory.searchDepartment(student.getAffiliatedDepartment());
        department.getDepartmentStudentDirectory().getStudentList().add(student);
        
        
        
        student=new Student();
        student.setStudentName("Dhi Mittal");
        student.setStudentId("678b");
        student.setEmail("dhimittal@gmail.com");
        student.setPhoneNumber("3577076851");
        student.setAffiliatedUniversity("Harvard University");
        student.setAffiliatedDepartment("Finance");
        student.setAffiliatedCollege("School of Business");
        student.setTranscript(Initialization.CourseLoadInitialization.initializeCourseload());
        student.setGpa(Initialization.CourseLoadInitialization.initializeCourseload().calculateGPA());
        student.setStudentAccount(Initialization.StudentAccountInitialization.initializeStudentAccount());
        studentDirectory.addStudent(student);
        university=universityDirectory.searchUniversity(student.getAffiliatedUniversity());
        university.getUniversityStudentDirectory().addStudent(student);
        college=collegeDirectory.searchCollege(student.getAffiliatedCollege());
        college.getCollegeStudentDirectory().addStudent(student);
        department=departmentDirectory.searchDepartment(student.getAffiliatedDepartment());
        department.getDepartmentStudentDirectory().getStudentList().add(student);
        
        
        
        student.setStudentName("Yana");
        student.setStudentId("678c");
        student.setEmail("yana11@gmail.com");
        student.setPhoneNumber("3577076847");
        student.setAffiliatedUniversity("Harvard University");
        student.setAffiliatedDepartment("Bio Technology");
        student.setAffiliatedCollege("College of Science");
        student.setTranscript(Initialization.CourseLoadInitialization.initializeCourseload());
        student.setGpa(Initialization.CourseLoadInitialization.initializeCourseload().calculateGPA());
        student.setStudentAccount(Initialization.StudentAccountInitialization.initializeStudentAccount());
        studentDirectory.addStudent(student);
        university=universityDirectory.searchUniversity(student.getAffiliatedUniversity());
        university.getUniversityStudentDirectory().addStudent(student);
        college=collegeDirectory.searchCollege(student.getAffiliatedCollege());
        college.getCollegeStudentDirectory().addStudent(student);
        department=departmentDirectory.searchDepartment(student.getAffiliatedDepartment());
        department.getDepartmentStudentDirectory().getStudentList().add(student);
        
       
        
        
        return studentDirectory;
    }   
}
