/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Initialization;

import University.Business.Course;
import University.Business.DepartmentCourseCatalog;
import java.util.ArrayList;

/**
 *
 * @author Nidhi Mittal <mittal.n@husky.neu.edu>
 */
public class InitializeCourseCatalog {
    
    public static DepartmentCourseCatalog departmentCourseCatalog;
    static Course course1;
    static Course course2;
    static Course course3;
    static Course course4;
    
    public static DepartmentCourseCatalog initializeISCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CIS1001");
        course1.setCourseName("Concepts of Object Oriented");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the concepts of object oriented design");
        course1.setCreditHours("2");
        course1.setIsCore(true);
        course1.setIsElective(true);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CIS1002");
        course2.setCourseName("Application Engineering and Development");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the concepts of Engineering");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
    public static DepartmentCourseCatalog initializeIACourseCatalog() {
    
        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CIA2001");
        course1.setCourseName("Data Mining");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the concepts of data mining");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CIA2002");
        course2.setCourseName("Data Security");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the concepts of data secuirty");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        
        
        
        return departmentCourseCatalog;
    }
    public static DepartmentCourseCatalog initializeMathCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CCM3001");
        course1.setCourseName("Calculus");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the mathematical concepts of calculus");
        course1.setCreditHours("2");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CCM3002");
        course2.setCourseName("Statistics");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the concepts of statistics");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
    public static DepartmentCourseCatalog initializeBioTechCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CBT4001");
        course1.setCourseName("Energy saving");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the concepts of energy saving");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CBT4002");
        course2.setCourseName("Pharmacy");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the pharma concepts");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
    public static DepartmentCourseCatalog initializeAnalyticsCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CCA5001");
        course1.setCourseName("Design Analytics");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the analytics concepts of data");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CCA5002");
        course2.setCourseName("Applied Cyber Security");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the concepts of security");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
    public static DepartmentCourseCatalog initializeApplliedNutritionCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CAN6001");
        course1.setCourseName("Agriculture and Innovation");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the concepts of agriculture");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CAN6002");
        course2.setCourseName("Pharmacitical design");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the desigining concepts in pharma ");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }

    public static DepartmentCourseCatalog initializeFinanceCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CCF7001");
        course1.setCourseName("Business Management");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the Managerial concepts of business");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CCF7002");
        course2.setCourseName("Investment Management");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the investment management concepts ");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
    public static DepartmentCourseCatalog initializeAccountingCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("ACC8001");
        course1.setCourseName("Investment accounting");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the accounting concepts regarding investments");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("ACC8002");
        course2.setCourseName("Health and Science Accounting");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the concepts of Engineering");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
    public static DepartmentCourseCatalog initializeCriminologyourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CCC9001");
        course1.setCourseName("Victimology");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the ciminology concepts related to victims");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CCC9002");
        course2.setCourseName("White collar crime ");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the concepts of white collar crimes");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
    public static DepartmentCourseCatalog initializeHistoryCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CHI10001");
        course1.setCourseName("History of Crusades");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the concepts of history realted to crusades");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CHI10002");
        course2.setCourseName("History of Piracy");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the concepts of piracy");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
    public static DepartmentCourseCatalog initializeBiologyoCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CBI11001");
        course1.setCourseName("Human Anatomy");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the concepts of biology oriented");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CBI11002");
        course2.setCourseName("Cell biology");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the concepts of cell biology");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
    public static DepartmentCourseCatalog initializeBioInformaticsCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("BIOINFO12001");
        course1.setCourseName("Genetics");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the concepts of information biology oriented");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("BIOINFO12002");
        course2.setCourseName("Intensive Cell biology");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the concepts of Intensive Cell biology");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
    public static DepartmentCourseCatalog initializeJDCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CJD13001");
        course1.setCourseName("Business law");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the concepts of Business law oriented");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CJD13002");
        course2.setCourseName("Marketing law");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the concepts of marketing laws");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
    public static DepartmentCourseCatalog initializeLLMCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CLLM14001");
        course1.setCourseName("Consumer law");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the concepts of Consumer law ");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CLLM14002");
        course2.setCourseName("Environmental law");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the concepts of Environmental law");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
    public static DepartmentCourseCatalog initializeArchitectureCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CSA15001");
        course1.setCourseName("Interior Designing");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the concepts of object oriented");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CSA15002");
        course2.setCourseName("Landscape designing");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the concepts of Landscape designing");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
    public static DepartmentCourseCatalog initializeGameDesigningCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CGD16001");
        course1.setCourseName("Game art");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the concepts of GAME art");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CGD16002");
        course2.setCourseName("Video gaming");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the concepts of video gaming");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
    public static DepartmentCourseCatalog initializeComputerScienceCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CSC17001");
        course1.setCourseName("Robotics");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the concepts of robotics");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CSE17002");
        course2.setCourseName("Computer organization");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the concepts of computer organization");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
    public static DepartmentCourseCatalog initializeDataScienceCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CSS18001");
        course1.setCourseName("Business analytics");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the concepts of business analytics");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CSS18002");
        course2.setCourseName("Data Analytics");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the concepts of data analytics");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
//Harvard course catalog
     public static DepartmentCourseCatalog initializeHISCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
 course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CIS1001");
        course1.setCourseName("Concepts of Object Oriented");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the concepts of object oriented design");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(true);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CIS1002");
        course2.setCourseName("Application Engineering and Development");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the concepts of Engineering");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
     }
     public static DepartmentCourseCatalog initializeHAnalyticsCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CCA5001");
        course1.setCourseName("Design Analytics");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the analytics concepts of data");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CCA5002");
        course2.setCourseName("Applied Cyber Security");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the concepts of security");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
    public static DepartmentCourseCatalog initializeHApplliedNutritionCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CAN6001");
        course1.setCourseName("Agriculture and Innovation");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the concepts of agriculture");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CAN6002");
        course2.setCourseName("Pharmacitical design");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the desigining concepts in pharma ");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
     public static DepartmentCourseCatalog initializeHIACourseCatalog() {
    
        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CIA2001");
        course1.setCourseName("Data Mining");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the concepts of data mining");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CIA2002");
        course2.setCourseName("Data Security");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the concepts of data secuirty");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        
        
        
        return departmentCourseCatalog;
    }
      public static DepartmentCourseCatalog initializeHComputerScienceCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CSC17001");
        course1.setCourseName("Robotics");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the concepts of robotics");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CSE17002");
        course2.setCourseName("Computer organization");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the concepts of computer organization");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
    public static DepartmentCourseCatalog initializeHDataScienceCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CSS18001");
        course1.setCourseName("Business analytics");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the concepts of business analytics");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CSS18002");
        course2.setCourseName("Data Analytics");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the concepts of data analytics");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
    public static DepartmentCourseCatalog initializeHArchitectureCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CSA15001");
        course1.setCourseName("Interior Designing");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the concepts of object oriented");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CSA15002");
        course2.setCourseName("Landscape designing");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the concepts of Landscape designing");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
    public static DepartmentCourseCatalog initializeHGameDesigningCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CGD16001");
        course1.setCourseName("Game art");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the concepts of GAME art");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CGD16002");
        course2.setCourseName("Video gaming");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the concepts of video gaming");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
    
    
  //Boston  
    
     public static DepartmentCourseCatalog initializeBMathCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CCM3001");
        course1.setCourseName("Calculus");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the mathematical concepts of calculus");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CCM3002");
        course2.setCourseName("Statistics");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the concepts of statistics");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
    public static DepartmentCourseCatalog initializeBBioTechCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CBT4001");
        course1.setCourseName("Energy saving");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the concepts of energy saving");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CBT4002");
        course2.setCourseName("Pharmacy");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the pharma concepts");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
     public static DepartmentCourseCatalog initializeBCriminologyourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CCC9001");
        course1.setCourseName("Victimology");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the ciminology concepts related to victims");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CCC9002");
        course2.setCourseName("White collar crime ");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the concepts of white collar crimes");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
    public static DepartmentCourseCatalog initializeBHistoryCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CHI10001");
        course1.setCourseName("History of Crusades");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the concepts of history realted to crusades");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CHI10002");
        course2.setCourseName("History of Piracy");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the concepts of piracy");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
    public static DepartmentCourseCatalog initializeBJDCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CJD13001");
        course1.setCourseName("Business law");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the concepts of Business law oriented");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CJD13002");
        course2.setCourseName("Marketing law");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the concepts of marketing laws");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
    public static DepartmentCourseCatalog initializeBLLMCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CLLM14001");
        course1.setCourseName("Consumer law");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the concepts of Consumer law ");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CLLM14002");
        course2.setCourseName("Environmental law");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the concepts of Environmental law");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
      public static DepartmentCourseCatalog initializeBApplliedNutritionCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CAN6001");
        course1.setCourseName("Agriculture and Innovation");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the concepts of agriculture");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CAN6002");
        course2.setCourseName("Pharmacitical design");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the desigining concepts in pharma ");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
       public static DepartmentCourseCatalog initializeBAnalyticsCourseCatalog() {

        departmentCourseCatalog = new DepartmentCourseCatalog();
        
        course1 = departmentCourseCatalog.addCourse();
        course1.setCourseNumber("CCA5001");
        course1.setCourseName("Design Analytics");
        course1.setCourseFees(1000);
        course1.setCourseDescription("Explains all the analytics concepts of data");
        course1.setCreditHours("4");
        course1.setIsCore(true);
        course1.setIsElective(false);
        course1.setPreRequisiteCourses(null);
        
        course2 = departmentCourseCatalog.addCourse();
        course2.setCourseNumber("CCA5002");
        course2.setCourseName("Applied Cyber Security");
        course2.setCourseFees(2000);
        course2.setCourseDescription("Explains all the concepts of security");
        course2.setCreditHours("10");
        course2.setIsCore(true);
        course2.setIsElective(false);
        
        ArrayList<Course> preReq = new ArrayList<Course>();
        preReq.add(course1);
        course2.setPreRequisiteCourses(preReq);
        return departmentCourseCatalog;
        
    }
   
}

