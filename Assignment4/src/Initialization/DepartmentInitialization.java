/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Initialization;
import University.Business.Department;
import University.Business.DepartmentDirectory;
import University.Business.DepartmentStudentDirectory;
import java.util.ArrayList;

/**

/**
 *
 * @author Guest
 */
public class DepartmentInitialization {
    static Department department;
    static DepartmentStudentDirectory departmentStudentDirectory;
      public static DepartmentDirectory departmentDirectory = new DepartmentDirectory();
      
    
      static Department  engineeringDepartment;
     static Department healthScienceDepartment;
     static Department artMediaDesignDepartment;
     static Department  businessDepartment;
     static Department  lawDepartment;
    static Department  scienceDepartment;
    static Department humanitiesDepartment;
   static Department computerScienceDepartment;
   static Department professionalStudiesDepartment;
   static Department hartMediaDesignDepartment; 
                                                                 
     
     public static DepartmentDirectory initializeCOEDepartmentDirectory()
    {
        engineeringDepartment=new Department();
       // departmentDirectory=new DepartmentDirectory();
        departmentStudentDirectory=new DepartmentStudentDirectory(); 
        engineeringDepartment.setDepartmentName("Information Systems");
        engineeringDepartment.setDepartmentCode(11);
        engineeringDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
        engineeringDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
        departmentDirectory.getDepartmentList().add(engineeringDepartment); 
        
        engineeringDepartment=new Department();
        //departmentDirectory=new DepartmentDirectory();
        departmentStudentDirectory=new DepartmentStudentDirectory(); 
        engineeringDepartment.setDepartmentName("Information Assurance");
        engineeringDepartment.setDepartmentCode(12);        
        engineeringDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
        engineeringDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
        departmentDirectory.getDepartmentList().add(engineeringDepartment);
        return departmentDirectory;
    }
     public static DepartmentDirectory initializeCOHDepartmentDirectory()
     {
        healthScienceDepartment=new Department();
        //departmentDirectory=new DepartmentDirectory();
        departmentStudentDirectory=new DepartmentStudentDirectory();
        healthScienceDepartment.setDepartmentName("Biology");
        healthScienceDepartment.setDepartmentCode(21);
        healthScienceDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
        healthScienceDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
        departmentDirectory.getDepartmentList().add(healthScienceDepartment);
        
        healthScienceDepartment=new Department();
        //departmentDirectory=new DepartmentDirectory();
        departmentStudentDirectory=new DepartmentStudentDirectory();
        healthScienceDepartment.setDepartmentName("Bio Informatics");
        healthScienceDepartment.setDepartmentCode(22);
        healthScienceDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
        healthScienceDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
        departmentDirectory.getDepartmentList().add(healthScienceDepartment);
        return departmentDirectory;
     }
   public static DepartmentDirectory initializeCOADepartmentDirectory()
   {
        //departmentDirectory=new DepartmentDirectory();
        artMediaDesignDepartment=new Department();
        departmentStudentDirectory=new DepartmentStudentDirectory();
       artMediaDesignDepartment .setDepartmentName("Architecure");
      artMediaDesignDepartment .setDepartmentCode(31);
      artMediaDesignDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
      artMediaDesignDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
        departmentDirectory.getDepartmentList().add(artMediaDesignDepartment);
        
        artMediaDesignDepartment=new Department();
        departmentStudentDirectory=new DepartmentStudentDirectory();
        artMediaDesignDepartment .setDepartmentName("Game Design");
      artMediaDesignDepartment .setDepartmentCode(32);
     artMediaDesignDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
      artMediaDesignDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
      departmentDirectory.getDepartmentList().add(artMediaDesignDepartment);
        return departmentDirectory;
     }
   public static DepartmentDirectory initializeCOFDepartmentDirectory()
   {
        //departmentDirectory=new DepartmentDirectory();
   
      
       professionalStudiesDepartment=new Department();
       departmentStudentDirectory=new DepartmentStudentDirectory();
       professionalStudiesDepartment.setDepartmentName("Analytics");
       professionalStudiesDepartment.setDepartmentCode(41);
             professionalStudiesDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
              professionalStudiesDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
      departmentDirectory.getDepartmentList().add( professionalStudiesDepartment);
      
      professionalStudiesDepartment=new Department();
       departmentStudentDirectory=new DepartmentStudentDirectory();
        professionalStudiesDepartment.setDepartmentName("Applied nutrition");
      professionalStudiesDepartment .setDepartmentCode(42);
       professionalStudiesDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
       professionalStudiesDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
      departmentDirectory.getDepartmentList().add( professionalStudiesDepartment);
        return departmentDirectory;
   }
    public static DepartmentDirectory initializeCOCDepartmentDirectory()
   {
        //departmentDirectory=new DepartmentDirectory();
  
      
      computerScienceDepartment=new Department();
      departmentStudentDirectory=new DepartmentStudentDirectory();
        computerScienceDepartment.setDepartmentName("Computer Science");
      computerScienceDepartment.setDepartmentCode(51);
           computerScienceDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
           computerScienceDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
      departmentDirectory.getDepartmentList().add(computerScienceDepartment);
      
      computerScienceDepartment=new Department();
      departmentStudentDirectory=new DepartmentStudentDirectory();
       computerScienceDepartment.setDepartmentName("Data Science");
      computerScienceDepartment.setDepartmentCode(52);
      computerScienceDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
      computerScienceDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
      departmentDirectory.getDepartmentList().add(computerScienceDepartment);
        return departmentDirectory;
      
   }
     public static DepartmentDirectory initializeCOLDepartmentDirectory()
   {
        //departmentDirectory=new DepartmentDirectory();
  
      lawDepartment=new Department();
      departmentStudentDirectory=new DepartmentStudentDirectory();
        lawDepartment.setDepartmentName("JD");
       lawDepartment.setDepartmentCode(61);
            lawDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
          lawDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
      departmentDirectory.getDepartmentList().add(lawDepartment);
      
      computerScienceDepartment=new Department();
      departmentStudentDirectory=new DepartmentStudentDirectory();
        lawDepartment.setDepartmentName("LLM");
      lawDepartment .setDepartmentCode(62);
      lawDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
       lawDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
      departmentDirectory.getDepartmentList().add(lawDepartment);
        return departmentDirectory;
   }
      public static DepartmentDirectory initializeCOSDepartmentDirectory()
   {
        //departmentDirectory=new DepartmentDirectory();
  
    
      scienceDepartment=new Department();
      departmentStudentDirectory=new DepartmentStudentDirectory();
        scienceDepartment.setDepartmentName("Mathematics");
       scienceDepartment.setDepartmentCode(71); 
            scienceDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
            scienceDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
      
      departmentDirectory.getDepartmentList().add(scienceDepartment);
      
      scienceDepartment=new Department();
      departmentStudentDirectory=new DepartmentStudentDirectory();
       scienceDepartment.setDepartmentName("Bio Technology");
      scienceDepartment .setDepartmentCode(72);
      scienceDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
      scienceDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
      departmentDirectory.getDepartmentList().add(scienceDepartment);
        return departmentDirectory;
   
   }
       public static DepartmentDirectory initializeCOSoDepartmentDirectory()
   {
        //departmentDirectory=new DepartmentDirectory();
  
       
       humanitiesDepartment =new Department();
       departmentStudentDirectory=new DepartmentStudentDirectory();
        humanitiesDepartment.setDepartmentName("Criminology");
       humanitiesDepartment.setDepartmentCode(81);
       humanitiesDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
       humanitiesDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
       departmentDirectory.getDepartmentList().add(humanitiesDepartment);
       
       humanitiesDepartment =new Department();
       departmentStudentDirectory=new DepartmentStudentDirectory();
        humanitiesDepartment.setDepartmentName("History");
       humanitiesDepartment.setDepartmentCode(82);
       humanitiesDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
         humanitiesDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
       departmentDirectory.getDepartmentList().add(humanitiesDepartment);
        return departmentDirectory;
   }
        public static DepartmentDirectory initializeCOFiDepartmentDirectory()
   {
        //departmentDirectory=new DepartmentDirectory();
  
       
         businessDepartment=new Department();
         departmentStudentDirectory=new DepartmentStudentDirectory();
       businessDepartment .setDepartmentName("Finance");
       businessDepartment.setDepartmentCode(91);
            businessDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
            businessDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
   
      departmentDirectory.getDepartmentList().add(businessDepartment);
      
      businessDepartment=new Department();
         departmentStudentDirectory=new DepartmentStudentDirectory();
       businessDepartment .setDepartmentName("Accounting");
       businessDepartment.setDepartmentCode(92);
       businessDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
   
        businessDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
        
    departmentDirectory.getDepartmentList().add(businessDepartment);
     return departmentDirectory;
   } 
    //harvard
     public static DepartmentDirectory initializeHCOADepartmentDirectory()
   {
        //departmentDirectory=new DepartmentDirectory();
        artMediaDesignDepartment=new Department();
        departmentStudentDirectory=new DepartmentStudentDirectory();
       artMediaDesignDepartment .setDepartmentName("Architecure");
      artMediaDesignDepartment .setDepartmentCode(31);
      artMediaDesignDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
      artMediaDesignDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
        departmentDirectory.getDepartmentList().add(artMediaDesignDepartment);
        
        artMediaDesignDepartment=new Department();
        departmentStudentDirectory=new DepartmentStudentDirectory();
        artMediaDesignDepartment .setDepartmentName("Game Design");
      artMediaDesignDepartment .setDepartmentCode(32);
     artMediaDesignDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
      artMediaDesignDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
      departmentDirectory.getDepartmentList().add(artMediaDesignDepartment);
        return departmentDirectory;
     }
      public static DepartmentDirectory initializeHCOBDepartmentDirectory()
   {
        //departmentDirectory=new DepartmentDirectory();
   
      
       professionalStudiesDepartment=new Department();
       departmentStudentDirectory=new DepartmentStudentDirectory();
       professionalStudiesDepartment.setDepartmentName("Analytics");
       professionalStudiesDepartment.setDepartmentCode(41);
             professionalStudiesDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
              professionalStudiesDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
      departmentDirectory.getDepartmentList().add( professionalStudiesDepartment);
      
      professionalStudiesDepartment=new Department();
       departmentStudentDirectory=new DepartmentStudentDirectory();
        professionalStudiesDepartment.setDepartmentName("Applied nutrition");
      professionalStudiesDepartment .setDepartmentCode(42);
       professionalStudiesDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
       professionalStudiesDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
      departmentDirectory.getDepartmentList().add( professionalStudiesDepartment);
        return departmentDirectory;
   }
    public static DepartmentDirectory initializeHCOCDepartmentDirectory()
   {
        //departmentDirectory=new DepartmentDirectory();
  
      
      computerScienceDepartment=new Department();
      departmentStudentDirectory=new DepartmentStudentDirectory();
        computerScienceDepartment.setDepartmentName("Computer Science");
      computerScienceDepartment.setDepartmentCode(51);
           computerScienceDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
           computerScienceDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
      departmentDirectory.getDepartmentList().add(computerScienceDepartment);
      
      computerScienceDepartment=new Department();
      departmentStudentDirectory=new DepartmentStudentDirectory();
       computerScienceDepartment.setDepartmentName("Data Science");
      computerScienceDepartment.setDepartmentCode(52);
      computerScienceDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
      computerScienceDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
      departmentDirectory.getDepartmentList().add(computerScienceDepartment);
        return departmentDirectory;
      
   }
     public static DepartmentDirectory initializeHCOEDepartmentDirectory()
    {
        engineeringDepartment=new Department();
        //departmentDirectory=new DepartmentDirectory();
        departmentStudentDirectory=new DepartmentStudentDirectory(); 
        engineeringDepartment.setDepartmentName("Information Systems");
        engineeringDepartment.setDepartmentCode(11);
        engineeringDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
        engineeringDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
        departmentDirectory.getDepartmentList().add(engineeringDepartment);  
        
        engineeringDepartment=new Department();
        departmentStudentDirectory=new DepartmentStudentDirectory(); 
        engineeringDepartment.setDepartmentName("Information Assurance");
        engineeringDepartment.setDepartmentCode(12);        
        engineeringDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
        engineeringDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
        departmentDirectory.getDepartmentList().add(engineeringDepartment);
        return departmentDirectory;
    }
    //Boston university
     
     public static DepartmentDirectory initializeBCOSoDepartmentDirectory()
   {
        //departmentDirectory=new DepartmentDirectory();
  
       
       humanitiesDepartment =new Department();
       departmentStudentDirectory=new DepartmentStudentDirectory();
        humanitiesDepartment.setDepartmentName("Criminology");
       humanitiesDepartment.setDepartmentCode(81);
       humanitiesDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
       humanitiesDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
       departmentDirectory.getDepartmentList().add(humanitiesDepartment);
       
        humanitiesDepartment =new Department();
       departmentStudentDirectory=new DepartmentStudentDirectory();
        humanitiesDepartment.setDepartmentName("History");
       humanitiesDepartment.setDepartmentCode(82);
       humanitiesDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
         humanitiesDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
       departmentDirectory.getDepartmentList().add(humanitiesDepartment);
        return departmentDirectory;
   }
          public static DepartmentDirectory initializeBCOSDepartmentDirectory()
   {
        //departmentDirectory=new DepartmentDirectory();
  
    
      scienceDepartment=new Department();
      departmentStudentDirectory=new DepartmentStudentDirectory();
        scienceDepartment.setDepartmentName("Mathematics");
       scienceDepartment.setDepartmentCode(71); 
            scienceDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
            scienceDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
      
      departmentDirectory.getDepartmentList().add(scienceDepartment);
      scienceDepartment=new Department();
      departmentStudentDirectory=new DepartmentStudentDirectory();
       scienceDepartment.setDepartmentName("Bio Technology");
      scienceDepartment .setDepartmentCode(72);
      scienceDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
      scienceDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
      departmentDirectory.getDepartmentList().add(scienceDepartment);
        return departmentDirectory;
   
   }
            public static DepartmentDirectory initializeBCOLDepartmentDirectory()
   {
        //departmentDirectory=new DepartmentDirectory();
  
      lawDepartment=new Department();
      departmentStudentDirectory=new DepartmentStudentDirectory();
        lawDepartment.setDepartmentName("JD");
       lawDepartment.setDepartmentCode(61);
            lawDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
          lawDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
      departmentDirectory.getDepartmentList().add(lawDepartment);
      
      lawDepartment=new Department();
      departmentStudentDirectory=new DepartmentStudentDirectory();
        lawDepartment.setDepartmentName("LLM");
      lawDepartment .setDepartmentCode(62);
      lawDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
       lawDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
      departmentDirectory.getDepartmentList().add(lawDepartment);
        return departmentDirectory;
   }
       public static DepartmentDirectory initializeBCOFDepartmentDirectory()
   {
        //departmentDirectory=new DepartmentDirectory();
   
      
       professionalStudiesDepartment=new Department();
       departmentStudentDirectory=new DepartmentStudentDirectory();
       professionalStudiesDepartment.setDepartmentName("Analytics");
       professionalStudiesDepartment.setDepartmentCode(41);
             professionalStudiesDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
              professionalStudiesDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
              departmentDirectory.getDepartmentList().add( professionalStudiesDepartment);
           
              professionalStudiesDepartment=new Department();
       departmentStudentDirectory=new DepartmentStudentDirectory();
        professionalStudiesDepartment.setDepartmentName("Applied nutrition");
      professionalStudiesDepartment .setDepartmentCode(42);
       professionalStudiesDepartment.setDepartmentStudentDirectory(departmentStudentDirectory);
       professionalStudiesDepartment.setCourseCatalog(Initialization.InitializeCourseCatalog.initializeISCourseCatalog());
      departmentDirectory.getDepartmentList().add( professionalStudiesDepartment);
        return departmentDirectory;
   }  
    
    }
        
     

