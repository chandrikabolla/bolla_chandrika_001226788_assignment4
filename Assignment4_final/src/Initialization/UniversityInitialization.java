/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Initialization;
import University.Business.University;
import University.Business.UniversityDirectory;
import University.Business.UniversityStudentDirectory;
/**
 *
 * @author Guest
 */
public class UniversityInitialization {
      static UniversityStudentDirectory universityStudentDirectory;
      static UniversityDirectory universityDirectory;
      static University university;
    public static UniversityDirectory initializeUniversity()
    {
        universityDirectory=new UniversityDirectory();
        
   university=new University();
   universityStudentDirectory=new UniversityStudentDirectory();
   university.setUniversityName("Northeastern University");
   university.setLocation("Boston");
   university.setUniversityStudentDirectory(universityStudentDirectory);
   university.setCollegeList(Initialization.CollegeInitialization.initializeNUCollegeDirectory().getCollegeList());
   universityDirectory.addUniversity(university);
 
   university=new University();
   universityStudentDirectory=new UniversityStudentDirectory();
   university.setUniversityName("Harvard University");
   university.setLocation("Cambridge");
   university.setUniversityStudentDirectory(universityStudentDirectory);
   university.setCollegeList(Initialization.CollegeInitialization.initializeNUCollegeDirectory().getCollegeList());
   universityDirectory.addUniversity(university);
          
   university=new University();
   universityStudentDirectory=new UniversityStudentDirectory();
   university.setUniversityName("Boston University");
   university.setLocation("Boston");
   university.setUniversityStudentDirectory(universityStudentDirectory);
   university.setCollegeList(Initialization.CollegeInitialization.initializeNUCollegeDirectory().getCollegeList());
   universityDirectory.addUniversity(university);
                         
return universityDirectory;
 
    }
    
}