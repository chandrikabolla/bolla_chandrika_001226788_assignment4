/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Initialization;

import University.Business.Course;
import University.Business.Degree;
import University.Business.DegreeDirectory;
import java.util.ArrayList;

/**
 *
 * @author chand
 */
public class DegreeInitialization {
    static Degree degree;
    static DegreeDirectory degreeDirectory;
    static ArrayList<Degree> degreeList;
    static ArrayList<Course> coreCourseList;
    static ArrayList<Course> electiveCourseList;
    public static DegreeDirectory initializeISDegrees(){
        degreeDirectory=new DegreeDirectory();
        degreeList=new ArrayList<Degree>();
        coreCourseList=new ArrayList<Course>();
        electiveCourseList=new ArrayList<Course>();
        degree=new Degree();
        degree.setDegreeName("Masters");
        degree.setNumberOfCoreCourses(1);
        degree.setNumberOfElectiveCourses(11);
        for(Course course:Initialization.InitializeCourseCatalog.initializeISCourseCatalog().getDepartmentCourseList())
        {
            if(course.isIsCore())
            {
                coreCourseList.add(course);
            //degree.getCoreCourseList().add(course);
            }
            else if(course.isIsElective())
            {
                electiveCourseList.add(course);
                //degree.getCoreCourseList().add(course);
            }
            else{
                //
            }
        }
        degree.setCoreCourseList(coreCourseList);
        degree.setElectiveCourseList(electiveCourseList);
        degreeList.add(degree);
        degreeDirectory.setDegreeList(degreeList);
        degree=new Degree();
        coreCourseList=new ArrayList<Course>();
        electiveCourseList=new ArrayList<Course>();
        degree.setDegreeName("Bachelors");
        degree.setNumberOfCoreCourses(3);
        degree.setNumberOfElectiveCourses(14);
        for(Course course:Initialization.InitializeCourseCatalog.initializeISCourseCatalog().getDepartmentCourseList())
        {
            if(course.isIsCore())
            {
                coreCourseList.add(course);
            //degree.getCoreCourseList().add(course);
            }
            else if(course.isIsElective())
            {
                electiveCourseList.add(course);
                //degree.getCoreCourseList().add(course);
            }
            else{
                //
            }
        }
        degree.setCoreCourseList(coreCourseList);
        degree.setElectiveCourseList(electiveCourseList);
        
        degreeList.add(degree);
        degreeDirectory.setDegreeList(degreeList);
        
        
      
        
        return degreeDirectory;
    }
    
}
