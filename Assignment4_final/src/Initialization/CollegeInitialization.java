/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Initialization;
import University.Business.College;
import University.Business.CollegeDirectory;
import University.Business.CollegeStudentDirectory;
import University.Business.Department;
import java.util.ArrayList;



/**
 *
 * @author Guest
 */
public class CollegeInitialization {
     static CollegeStudentDirectory collegeStudentDirectory;
      static CollegeDirectory collegeDirectory;
      static College college;
    public static CollegeDirectory initializeNUCollegeDirectory()
    {
        
        college=new College();
        collegeDirectory=new CollegeDirectory();
 collegeStudentDirectory=new CollegeStudentDirectory(); 
        college.setCollegeName("College of Arts, Media and Design");
        college.setCollegeCode(1);
        college.setDepartmentList(Initialization.DepartmentInitialization.initializeCOADepartmentDirectory().getDepartmentList());
         college.setCollegeStudentDirectory(collegeStudentDirectory);
   collegeDirectory.addCollege(college);
         
   
   college=new College();
   collegeStudentDirectory=new CollegeStudentDirectory(); 
        college.setCollegeName("School of Business");
        college.setCollegeCode(2);
        college.setDepartmentList(Initialization.DepartmentInitialization.initializeCOFiDepartmentDirectory().getDepartmentList());
         college.setCollegeStudentDirectory(collegeStudentDirectory);
   collegeDirectory.addCollege(college);
        
        college=new College();
        collegeStudentDirectory=new CollegeStudentDirectory(); 
        college.setCollegeName("College of Computer and Information Science");
        college.setDepartmentList(Initialization.DepartmentInitialization.initializeCOCDepartmentDirectory().getDepartmentList());
        college.setCollegeCode(3);
         college.setCollegeStudentDirectory(collegeStudentDirectory);
   collegeDirectory.addCollege(college);
        
        
        
         college=new College();
         collegeStudentDirectory=new CollegeStudentDirectory(); 
        college.setCollegeName("College of Engineering");
        college.setCollegeCode(4);
        college.setDepartmentList(Initialization.DepartmentInitialization.initializeCOEDepartmentDirectory().getDepartmentList());
         college.setCollegeStudentDirectory(collegeStudentDirectory);
   collegeDirectory.addCollege(college);
        
        
        college=new College();
        collegeStudentDirectory=new CollegeStudentDirectory(); 
        college.setCollegeName("College of Health Sciences");
        college.setCollegeCode(5);
        college.setDepartmentList(Initialization.DepartmentInitialization.initializeCOHDepartmentDirectory().getDepartmentList());
         college.setCollegeStudentDirectory(collegeStudentDirectory);
   collegeDirectory.addCollege(college);
        
        college=new College();
        collegeStudentDirectory=new CollegeStudentDirectory(); 
        college.setCollegeName("School of Law");
        college.setCollegeCode(6);
        college.setDepartmentList(Initialization.DepartmentInitialization.initializeCOLDepartmentDirectory().getDepartmentList());
         college.setCollegeStudentDirectory(collegeStudentDirectory);
   collegeDirectory.addCollege(college);
        
        
        
      college=new College();
      collegeStudentDirectory=new CollegeStudentDirectory(); 
        college.setCollegeName("College of Professional Studies");
        college.setCollegeCode(7);
        college.setDepartmentList(Initialization.DepartmentInitialization.initializeCOFDepartmentDirectory().getDepartmentList());
         college.setCollegeStudentDirectory(collegeStudentDirectory);
   collegeDirectory.addCollege(college);
        
        
        
    college=new College();
    collegeStudentDirectory=new CollegeStudentDirectory(); 
        college.setCollegeName("College of Science");
        college.setCollegeCode(8);
        college.setDepartmentList(Initialization.DepartmentInitialization.initializeCOSDepartmentDirectory().getDepartmentList());
         college.setCollegeStudentDirectory(collegeStudentDirectory);
   collegeDirectory.addCollege(college);
        
        
       college=new College();
       collegeStudentDirectory=new CollegeStudentDirectory(); 
        college.setCollegeName("College of Social Sciences and Humanities");
        college.setCollegeCode(9);
        college.setDepartmentList(Initialization.DepartmentInitialization.initializeCOSoDepartmentDirectory().getDepartmentList());
         college.setCollegeStudentDirectory(collegeStudentDirectory);
   collegeDirectory.addCollege(college);
        
        
        
        
        return collegeDirectory;
    }
//initialization of Harvard colleges
       public static CollegeDirectory initializeHVCollegeDirectory()
    {
        
        college=new College();
        collegeDirectory=new CollegeDirectory();
 collegeStudentDirectory=new CollegeStudentDirectory(); 
        college.setCollegeName("College of Arts, Media and Design");
        college.setCollegeCode(11);
        college.setDepartmentList(Initialization.DepartmentInitialization.initializeHCOADepartmentDirectory().getDepartmentList());
         college.setCollegeStudentDirectory(collegeStudentDirectory);
   collegeDirectory.addCollege(college);
         
   
   college=new College();
   collegeStudentDirectory=new CollegeStudentDirectory(); 
        college.setCollegeName("School of Business");
        college.setCollegeCode(22);
        college.setDepartmentList(Initialization.DepartmentInitialization.initializeHCOBDepartmentDirectory().getDepartmentList());
         college.setCollegeStudentDirectory(collegeStudentDirectory);
   collegeDirectory.addCollege(college);
        
        college=new College();
        collegeStudentDirectory=new CollegeStudentDirectory(); 
        college.setCollegeName("College of Computer and Information Science");
        college.setDepartmentList(Initialization.DepartmentInitialization.initializeHCOCDepartmentDirectory().getDepartmentList());
        college.setCollegeCode(33);
         college.setCollegeStudentDirectory(collegeStudentDirectory);
   collegeDirectory.addCollege(college);
        
        
        
         college=new College();
         collegeStudentDirectory=new CollegeStudentDirectory(); 
        college.setCollegeName("College of Engineering");
        college.setCollegeCode(44);
        college.setDepartmentList(Initialization.DepartmentInitialization.initializeHCOEDepartmentDirectory().getDepartmentList());
         college.setCollegeStudentDirectory(collegeStudentDirectory);
   collegeDirectory.addCollege(college);
    
    return collegeDirectory;
    }
       //initialization of boston university colleges
       
       
        public static CollegeDirectory initializeBUCollegeDirectory()
    {
         collegeDirectory=new CollegeDirectory();
        college=new College();
        collegeStudentDirectory=new CollegeStudentDirectory(); 
        college.setCollegeName("School of Law");
        college.setCollegeCode(111);
        college.setDepartmentList(Initialization.DepartmentInitialization.initializeBCOLDepartmentDirectory().getDepartmentList());
         college.setCollegeStudentDirectory(collegeStudentDirectory);
   collegeDirectory.addCollege(college);
        
        
        
      college=new College();
      collegeStudentDirectory=new CollegeStudentDirectory(); 
        college.setCollegeName("College of Professional Studies");
        college.setCollegeCode(222);
        college.setDepartmentList(Initialization.DepartmentInitialization.initializeBCOFDepartmentDirectory().getDepartmentList());
         college.setCollegeStudentDirectory(collegeStudentDirectory);
   collegeDirectory.addCollege(college);
        
        
        
    college=new College();
    collegeStudentDirectory=new CollegeStudentDirectory(); 
        college.setCollegeName("College of Science");
        college.setCollegeCode(333);
        college.setDepartmentList(Initialization.DepartmentInitialization.initializeBCOSDepartmentDirectory().getDepartmentList());
         college.setCollegeStudentDirectory(collegeStudentDirectory);
   collegeDirectory.addCollege(college);
        
        
       college=new College();
       collegeStudentDirectory=new CollegeStudentDirectory(); 
        college.setCollegeName("College of Social Sciences and Humanities");
        college.setCollegeCode(444);
        college.setDepartmentList(Initialization.DepartmentInitialization.initializeBCOSoDepartmentDirectory().getDepartmentList());
         college.setCollegeStudentDirectory(collegeStudentDirectory);
   collegeDirectory.addCollege(college);
        
        
        
        
        return collegeDirectory;
    }
       
}
