/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Initialization;

import University.Business.Semester;
import University.Business.SemesterCatalog;
import java.util.ArrayList;

/**
 *
 * @author chand
 */
public class SemesterInitialization {
    static Semester semester;
    static SemesterCatalog semesterCatalog;
    static ArrayList<Semester> semesterList;
   public static SemesterCatalog initializeSemester(){
       semesterCatalog=new SemesterCatalog();
       semesterList=new ArrayList<Semester>();
       semester=new Semester();
       semester.setSemesterName("Fall 2016");
       semester.setStartDate("07 sept 2016");
       semester.setEndDate("15 December 2016");
       semesterList.add(semester);
    
       semester=new Semester();
       semester.setSemesterName("Spring 2017");
       semester.setStartDate("14 Jan 2017");
       semester.setEndDate("15 May 2017");
       semesterList.add(semester);
       //semesterCatalog.getSemesterList().add(semester);
    
       semester=new Semester();
       semester.setSemesterName("Fall 2017");
       semester.setStartDate("07 spet 2017");
       semester.setEndDate("15 December 2017");
       semesterList.add(semester);
       //semesterCatalog.getSemesterList().add(semester);
       semesterCatalog.setSemesterList(semesterList);
       return semesterCatalog;
   }
    
}
