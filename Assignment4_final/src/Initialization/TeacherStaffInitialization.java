/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Initialization;
import University.Business.TeacherDirectory;
import University.Business.StaffDirectory;
import University.Business.Staff;
import University.Business.Teacher;

/**
 *
 * @author chand
 */
public class TeacherStaffInitialization {
    static Teacher teacher;
    static Staff staff;
   static TeacherDirectory teacherDirectory;
   static StaffDirectory staffDirectory;
   
    public static TeacherDirectory initializeTeacher(){
        teacherDirectory=new TeacherDirectory();
        teacher=new Teacher("professor");
        teacher.getPerson().setFirstName("Chandrika");
        teacher.getPerson().setLastName("Bolla");
        teacher.getPerson().setDateOfBirth("09-Oct-1995");
        teacher.getPerson().setEmailId("Chandrikabolla999@gmail.com");
        teacher.getPerson().setPhoneNumber("857-707-6850");
        teacher.getPerson().setSocialSecurityNumber("333-333-333");
        teacher.getPerson().setAddress("11 tetlow street,Boston,MA 02115");
        teacher.getPosition().setPositionType("Full-time");
        teacherDirectory.addTeacher(teacher);
        
        teacher=new Teacher("professor");
        teacher.getPerson().setFirstName("Kal");
        teacher.getPerson().setLastName("Bugrara");
        teacher.getPerson().setDateOfBirth("09-Oct-1965");
        teacher.getPerson().setEmailId("kalbugrara999@gmail.com");
        teacher.getPerson().setPhoneNumber("857-707-6851");
        teacher.getPerson().setSocialSecurityNumber("333-333-300");
        teacher.getPerson().setAddress("18 tetlow street,Boston,MA 02115");
        teacher.getPosition().setPositionType("Full-time");
        teacherDirectory.addTeacher(teacher);
        
        
        teacher=new Teacher("Assistant professor");
        teacher.getPerson().setFirstName("Mike ");
        teacher.getPerson().setLastName("Simson");
        teacher.getPerson().setDateOfBirth("09-Oct-1985");
        teacher.getPerson().setEmailId("mikesimson999@gmail.com");
        teacher.getPerson().setPhoneNumber("857-707-6856");
        teacher.getPerson().setSocialSecurityNumber("333-333-390");
        teacher.getPerson().setAddress("12 tetlow street,Boston,MA 02115");
        teacher.getPosition().setPositionType("Part-time");
        teacherDirectory.addTeacher(teacher);
       
        
        return teacherDirectory;
    }
    public static StaffDirectory initializeStaff(){
        staffDirectory=new StaffDirectory();
        staff=new Staff("advisor");
        System.out.println(staff.getPerson());
        staff.getPerson().setFirstName("Chadnri");
        staff.getPerson().setLastName("Bolla");
        staff.getPerson().setEmailId("chand9mail@gmail.com");
        staff.getPerson().setPhoneNumber("857-707-6851");
        staff.getPerson().setDateOfBirth("09-08-1995");
        staff.getPerson().setAddress("12 tetlow,Boston,MA 02115");
        staff.getPerson().setSocialSecurityNumber("404-404-404");
        staff.getPosition().setPositionType("Part-time");
        staffDirectory.addStaff(staff);
        
        staff=new Staff("coordinator");
        System.out.println(staff.getPerson());
        staff.getPerson().setFirstName("Rose");
        staff.getPerson().setLastName("Lisa");
        staff.getPerson().setEmailId("lisarose@gmail.com");
        staff.getPerson().setPhoneNumber("857-707-6751");
        staff.getPerson().setDateOfBirth("09-08-1995");
        staff.getPerson().setAddress("12 tetlow,Boston,MA 02115");
        staff.getPerson().setSocialSecurityNumber("404-499-404");
        staff.getPosition().setPositionType("Part-time");
        staffDirectory.addStaff(staff);
        
        
        staff=new Staff("advisor");
        System.out.println(staff.getPerson());
        staff.getPerson().setFirstName("Park");
        staff.getPerson().setLastName("Peterson");
        staff.getPerson().setEmailId("parkpeterson@gmail.com");
        staff.getPerson().setPhoneNumber("857-707-6881");
        staff.getPerson().setDateOfBirth("09-08-1cd 995");
        staff.getPerson().setAddress("12 tetlow,Boston,MA 02115");
        staff.getPerson().setSocialSecurityNumber("404-454-404");
        staff.getPosition().setPositionType("Full-time");
        staffDirectory.addStaff(staff);
        
       return staffDirectory; 
    }
   
}
