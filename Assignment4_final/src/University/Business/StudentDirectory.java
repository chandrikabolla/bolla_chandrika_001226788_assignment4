/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Business;

import University.Business.Student;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Guest
 */
public class StudentDirectory {
    List<Student> studentList;
    Student student;

    public StudentDirectory() {
        studentList = new ArrayList<Student>();
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }

    public Student addStudent(Student student) {
        
        studentList.add(student);
        return student;
    }
    public Student searchStudent(String studentname)
    {
        for(Student student1 :studentList)
        {
            if(student1.getStudentName().equals(studentname))
            {
                    return student1;
            }
            
        }
        return null;
    }
}
