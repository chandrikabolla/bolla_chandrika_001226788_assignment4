/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Guest
 */
public class UniversityStudentDirectory {
 
    
    Student student;
     List<Student> studentList;

    public UniversityStudentDirectory() {
        studentList = new ArrayList<Student>();
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }

    public Student addStudent(Student student) {
           this.student=student;
        studentList.add(student);
        return student;
       
    }
   
    
}
