/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Business;

import University.*;

/**
 *
 * @author Nidhi Mittal <mittal.n@husky.neu.edu>
 */
public class CourseOffering {

    //private String teacher; 
    private Teacher teacher;
    private String timing;
    private int capacity;
    private int seatsFilled;
    private String sectionNumber;
    private ClassRoom classRoom;
    private Semester semester;
    private Course course;

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    //public String getTeacher() {
    //    return teacher;
    //}

    //public void setTeacher(String teacher) {
    //    this.teacher = teacher;
    //}

    public String getTiming() {
        return timing;
    }

    public void setTiming(String timing) {
        this.timing = timing;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public ClassRoom getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(ClassRoom classRoom) {
        this.classRoom = classRoom;
    }

    public String getSectionNumber() {
        return sectionNumber;
    }

    public void setSectionNumber(String sectionNumber) {
        this.sectionNumber = sectionNumber;
    }

    public int getSeatsFilled() {
        return seatsFilled;
    }

    public void setSeatsFilled(int seatsFilled) {
        this.seatsFilled = seatsFilled;
    }

    public Semester getSemester() {
        return semester;
    }

    public void setSemester(Semester semester) {
        this.semester = semester;
    }
    

    public double getCourseEarning() {
        return course.getCourseFees() * seatsFilled;
    }

    public int getStudentsSignedUpForTheClass() {
        return seatsFilled;
    }

    public int getSeatsVacant() {
        return (capacity - seatsFilled);
    }

    public double getEnrollmentPercentage() {
        return (seatsFilled / capacity) * 100;
    }

}
