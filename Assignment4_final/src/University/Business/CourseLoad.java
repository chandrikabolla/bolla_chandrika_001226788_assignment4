/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Business;

import java.util.ArrayList;

/**
 *
 * @author Guest
 */
public class CourseLoad {
    private SeatAssignment seatAssignment;
    private Semester semester;
    private double totalGPA=0;
    
     static ArrayList<SeatAssignment> courseLoad;

    public Semester getSemester() {
        return semester;
    }

    public void setSemester(Semester semester) {
        this.semester = semester;
    }
    
    //take from semister;

    public SeatAssignment getSeatAssignment() {
        return seatAssignment;
    }

    public void setSeatAssignment(SeatAssignment seatAssignment) {
        this.seatAssignment = seatAssignment;
    }

public double calculateSemesterCost(ArrayList<SeatAssignment> courseLoad)
{
    double semesterCost=0;
    for(SeatAssignment seatAssignment: courseLoad)
    {
        semesterCost= semesterCost + seatAssignment.getCourseFees();
    }
    return semesterCost;
}
public void setListOfCoursesCompleted(ArrayList<SeatAssignment> courseLoad)
{
    ArrayList<SeatAssignment> listOfCoursesCompleted = new ArrayList<SeatAssignment>();
    for(SeatAssignment seatAssignment : courseLoad){
        String loadCourse = seatAssignment.getCourse();
        if(loadCourse != null){
        listOfCoursesCompleted.add(seatAssignment);
        }
    }
   
  
}

public void setSemesterGPA(ArrayList<SeatAssignment> courseLoad){
    
    int count=0;
    
    for (SeatAssignment seatAssignment : courseLoad)
    {
        count++;
    }
    if(count!=0)
    {
    for (SeatAssignment seatAssignment : courseLoad)
    {
        totalGPA=totalGPA+seatAssignment.getStudentGrade();
        
    }
    }
    totalGPA=totalGPA/count;
    
}
public double getSemesterGPA(){
    return totalGPA;
}
/*public ArrayList<SeatAssignment> setListOfCoursesCompleted(ArrayList<SeatAssignment>list){
    
    
}
 public SeatAssignment addListOfCoursesCompleted()
    {
        SeatAssignment coursesCompleted=new SeatAssignment();
        coursesCompleted.add(course);
        return coursesCompleted;
    }*/
}