/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Guest
 */
public class UniversityDirectory {
    
    List<University> universityList;
    University university1;
    String universityName;

    public UniversityDirectory() {
        universityList = new ArrayList<University>();
    }

    public List<University> getUniversityList() {
        return universityList;
    }

    public void setUniversityList(List<University> universityList) {
        this.universityList = universityList;
    }

    public University addUniversity(University university) {
        this.university1=university;
        universityList.add(university);
        return university;
    }
    public University searchUniversity(String universityname)
    {
        this.universityName=universityname;
        for(University university2 :universityList)
        {
            if(university2.getUniversityName().equals(universityname))
            {
                    return university2;
            }
            
        }
        return null;
    }
}
