/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Business;

import java.util.ArrayList;

/**
 *
 * @author Guest
 */
public class University {
    private String universityName;
    private String location; 
    private UniversityStudentDirectory universityStudentDirectory;
    private ArrayList<College> collegeList;
    
    public void University(){
        universityStudentDirectory=new UniversityStudentDirectory();
        collegeList= new ArrayList<College>();
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public UniversityStudentDirectory getUniversityStudentDirectory() {
        return universityStudentDirectory;
    }

    public void setUniversityStudentDirectory(UniversityStudentDirectory universityStudentDirectory) {
        this.universityStudentDirectory = universityStudentDirectory;
    }

    public ArrayList<College> getCollegeList() {
        return collegeList;
    }

    public void setCollegeList(ArrayList<College> collegeList) {
        this.collegeList = collegeList;
    }
    
    
    
    @Override
    public String toString(){
        return this.universityName;
    }
    
    
    
}
