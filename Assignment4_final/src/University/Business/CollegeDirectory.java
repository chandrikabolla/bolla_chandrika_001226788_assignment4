/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Guest
 */
public class CollegeDirectory {
      ArrayList<College> collegeList;
    College college1;
    String collegeName;

    public CollegeDirectory() {
        collegeList = new ArrayList<College>();
    }

    public ArrayList<College> getCollegeList() {
        return collegeList;
    }

    public void setCollegeList(ArrayList<College> collegeList) {
        this.collegeList = collegeList;
    }

    public College addCollege(College college) {
        this.college1=college;
        collegeList.add(college);
        return college;
    }
    public College searchCollege(String collegename)
    {
        this.collegeName=collegename;
        for(College college2 :collegeList)
        {
            if(college2.getCollegeName().equals(collegename))
            {
                    return college2;
            }
            
        }
        return null;
    }
}
   



