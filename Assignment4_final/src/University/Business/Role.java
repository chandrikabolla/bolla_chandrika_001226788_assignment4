/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Business;

/**
 *
 * @author chand
 */
public class Role {
    
    private String roleListArray[]={"professor","lecturer","RA","TA","Advisor","co-ordinator"};
    protected String role;
    
    public Role(String role){
        this.role=role;
        
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String[] getRoleListArray() {
        return roleListArray;
    }

    public void setRoleListArray(String[] roleListArray) {
        this.roleListArray = roleListArray;
    }
    
    
}
