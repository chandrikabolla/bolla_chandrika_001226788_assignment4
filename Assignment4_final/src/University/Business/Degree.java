/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.Business;

import java.util.ArrayList;

/**
 *
 * @author Nidhi Mittal <mittal.n@husky.neu.edu>
 */
public class Degree {
    
    private String degreeName;
    private int numberOfCoreCourses;
    private int numberOfElectiveCourses;
    private ArrayList<Course> coreCourseList;
    private ArrayList<Course> electiveCourseList;
    
    public void Degree(){
        coreCourseList= new ArrayList<Course>();
        electiveCourseList=new ArrayList<Course>();
    }

    public String getDegreeName() {
        return degreeName;
    }

    public void setDegreeName(String degreeName) {
        this.degreeName = degreeName;
    }

    public int getNumberOfCoreCourses() {
        return numberOfCoreCourses;
    }

    public void setNumberOfCoreCourses(int numberOfCoreCourses) {
        this.numberOfCoreCourses = numberOfCoreCourses;
    }

    public int getNumberOfElectiveCourses() {
        return numberOfElectiveCourses;
    }

    public void setNumberOfElectiveCourses(int numberOfElectiveCourses) {
        this.numberOfElectiveCourses = numberOfElectiveCourses;
    }

    public ArrayList<Course> getCoreCourseList() {
        return coreCourseList;
    }

    public void setCoreCourseList(ArrayList<Course> CoreCourseList) {
        this.coreCourseList = CoreCourseList;
    }

    public ArrayList<Course> getElectiveCourseList() {
        return electiveCourseList;
    }

    public void setElectiveCourseList(ArrayList<Course> electiveCourseList) {
        this.electiveCourseList = electiveCourseList;
    }
    
    
}
