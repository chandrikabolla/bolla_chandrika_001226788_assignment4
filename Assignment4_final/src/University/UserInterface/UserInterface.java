/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.UserInterface;
import Initialization.InitializeCourseSchedule;
import Initialization.InitializeCourseCatalog;
import Initialization.TeacherStaffInitialization;
import Initialization.UniversityInitialization;
import University.Business.StudentDirectory;
import University.Business.Course;
import University.Business.CourseLoad;
import University.Business.CourseOffering;
import University.Business.Degree;
import University.Business.Student;
import University.Business.Person;
import University.Business.Staff;
import University.Business.Teacher;
import University.Business.TeacherDirectory;
import University.Business.StaffDirectory;

/**
 *
 * @author Guest
 */
public class UserInterface {
      static Person person,person1;
      static Teacher teacher;
      static Staff staff;
     
    public static void main(String args[])
    {
          InitializeCourseCatalog.initializeISCourseCatalog();
          UniversityInitialization.initializeUniversity();
          
          //print IS courses
          System.out.println("IS courses are:\n");
          for(Degree degree:Initialization.DegreeInitialization.initializeISDegrees().getDegreeList())
          {
              System.out.println("Degree:"+degree.getDegreeName()+"\nCoreCourses:"+degree.getNumberOfCoreCourses()+"\nListed:");
              for(Course course:degree.getCoreCourseList())
              {
                  System.out.println(course.getCourseNumber()+""+course.getCourseName());
              }
              System.out.println("\nElective Courses:"+degree.getNumberOfElectiveCourses());
              for(Course course:degree.getElectiveCourseList())
              {
                  System.out.println(course.getCourseNumber()+""+course.getCourseName());
              }
            
                      
          }
          
          
          
          
          
        // print course catalog
        for (Course course : InitializeCourseCatalog.initializeISCourseCatalog().getDepartmentCourseList()) {
            System.out.println("\nCourse Number-->" + course.getCourseNumber() +
                    "\n Course Name-->" + course.getCourseName() + 
                    "\n Course Price-->" + course.getCourseFees() + 
                    "\n Course Description-->" + course.getCourseDescription() + 
                    "\n Credit hours-->" + course.getCreditHours() +
                    "\n Core Course -- >" + course.isIsCore() +
                    "\n Elective Course -- >" + course.isIsElective()
                    );
            if (course.getPreRequisiteCourses() != null) {
                System.out.println("Pre-requisite courses: ");
            for (Course preReqCourse : course.getPreRequisiteCourses()) {
                System.out.print(preReqCourse.getCourseName());
            }
            System.out.println("\n"); 
            }   
        }
        
        // print course schedule
        for (CourseOffering courseOffering : InitializeCourseSchedule.initializeCourseSchedule().getDepartmentCourseSchedule()) {
            System.out.println(
                    "\n Teacher ---> " + courseOffering.getTeacher().toString() + 
                    "\n Timing ---> " + courseOffering.getTiming() + 
                    "\n Capacity ---> " + courseOffering.getCapacity() + 
                    "\n SeatsFilled ---> " + courseOffering.getSeatsFilled() + 
                    "\n Vacant seats ---> " + courseOffering.getSeatsVacant() + 
                    "\n Section number ---> " + courseOffering.getSectionNumber() +
                    "\n Class Room Number ---> " + courseOffering.getClassRoom().getRoomNumber() + 
                    "\n Class Room Building ---> " + courseOffering.getClassRoom().getBuildingNumber() +
                    "\n Semester name ---> " + courseOffering.getSemester().getSemesterName() + 
                    "\n Semester start date ---> " + courseOffering.getSemester().getStartDate() + 
                    "\n Semester End Date ---> " + courseOffering.getSemester().getEndDate() +
                    "\n Course Number ---> " + courseOffering.getCourse().getCourseNumber() + 
                    "\n Course Name ---> " + courseOffering.getCourse().getCourseName() 
             );
        }
        
    
    //teacher staff
    
        
           TeacherDirectory teacherDirectory =Initialization.TeacherStaffInitialization.initializeTeacher();
          
           for(Teacher teacher:teacherDirectory.getTeacherList())
           {
               Person person=teacher.getPerson();
                System.out.println("First name: "+ person.getFirstName()+"\nLast name: "+person.getLastName()+
                 "\nEmail Id: "+person.getEmailId()+"\nContact@: "+person.getPhoneNumber()+
                 "\nDate of Birth: "+person.getDateOfBirth()+"\n Social Scurity Number: "+person.getSocialSecurityNumber()+
                 "\n Address:"+person.getAddress());
        
        
                System.out.println("Position:"+teacher.getPosition().getPositionType()+"\nRole:"+teacher.getRole());
        
                System.out.println("Teacher details are displayed");
           
           }
          StaffDirectory staffDirectory=Initialization.TeacherStaffInitialization.initializeStaff();
            for(Staff staff:staffDirectory.getStaffList())
            {
                Person person=staff.getPerson();
         System.out.println("First name: "+ person.getFirstName()+"\nLast name: "+person.getLastName()+
                 "\nEmail Id: "+person.getEmailId()+"\nContact@: "+person.getPhoneNumber()+
                 "\nDate of Birth: "+person.getDateOfBirth()+"\n Social Scurity Number: "+person.getSocialSecurityNumber()+
                 "\n Address:"+person.getAddress());
         
         System.out.println(staff.getPosition().getPositionType()+"Role:"+staff.getRole());
         
         System.out.println("Staff details are displayed");
            }
      //student
        StudentDirectory studentDirectory=Initialization.StudentInitialization.initializeStudent();
        for(Student student:studentDirectory.getStudentList())
        {
        System.out.println(student.getStudentId()+"\nStudent GPA: "+student.getGpa()+"\nStudentAccount Details:\nAccount status:"
        +student.getStudentAccount().getAccountStatus());
        for(CourseLoad courseLoad:student.getTranscript().getTranscript())
            
        {
            System.out.println("----------------------------------------"+courseLoad.getSemester()+"-----------------------------------------");
            System.out.println(""+courseLoad.getSemesterGPA());
        }
        }
        
    }
    
}
