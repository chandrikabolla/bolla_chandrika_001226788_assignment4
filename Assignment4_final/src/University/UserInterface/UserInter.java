/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University.UserInterface;

import Initialization.CollegeInitialization;
import Initialization.DegreeInitialization;
import Initialization.DepartmentInitialization;
import static Initialization.DepartmentInitialization.initializeCOADepartmentDirectory;
import Initialization.InitializeCourseCatalog;
import Initialization.InitializeCourseSchedule;
import Initialization.StudentInitialization;
import Initialization.UniversityInitialization;
import University.Business.College;
import University.Business.Course;

import University.Business.CourseOffering;
import University.Business.Degree;
//import static Initialization.DepartmentInitialization.departmentDirectory;
import University.Business.Department;
import University.Business.DepartmentCourseCatalog;
import University.Business.DepartmentCourseSchedule;
import University.Business.DepartmentDirectory;
import University.Business.Semester;
import University.Business.Student;
import University.Business.StudentDirectory;
import University.Business.University;
import java.util.Scanner;

/**
 *
 * @author chand
 */
public class UserInter {

    public static void main(String args[]) {
        //Initialization.StudentInitialization.initializeStudent();
        Initialization.StudentAccountInitialization.initializeStudentAccount();
        Initialization.ClassRoomInitialization.initializeClassRooms();
        Initialization.DegreeInitialization.initializeISDegrees();
        Initialization.CourseLoadInitialization.initializeCourseload();

        Initialization.InitializeCourseCatalog.initializeAccountingCourseCatalog();
        Initialization.InitializeCourseCatalog.initializeAnalyticsCourseCatalog();
        Initialization.InitializeCourseCatalog.initializeApplliedNutritionCourseCatalog();
        Initialization.InitializeCourseCatalog.initializeArchitectureCourseCatalog();
        Initialization.InitializeCourseCatalog.initializeBAnalyticsCourseCatalog();
        Initialization.InitializeCourseCatalog.initializeBApplliedNutritionCourseCatalog();
        Initialization.InitializeCourseCatalog.initializeBBioTechCourseCatalog();
        Initialization.InitializeCourseCatalog.initializeBCriminologyourseCatalog();
        Initialization.InitializeCourseCatalog.initializeBHistoryCourseCatalog();
        Initialization.InitializeCourseCatalog.initializeBJDCourseCatalog();
        Initialization.InitializeCourseCatalog.initializeBLLMCourseCatalog();
        Initialization.InitializeCourseCatalog.initializeBMathCourseCatalog();
        Initialization.InitializeCourseCatalog.initializeBioInformaticsCourseCatalog();
        Initialization.InitializeCourseCatalog.initializeBioTechCourseCatalog();
        Initialization.InitializeCourseCatalog.initializeBiologyoCourseCatalog();
        Initialization.InitializeCourseCatalog.initializeComputerScienceCourseCatalog();

        Initialization.InitializeCourseSchedule.initializeCourseSchedule();

        Initialization.SemesterInitialization.initializeSemester();

        Initialization.TeacherStaffInitialization.initializeTeacher();

        Initialization.InitializeCourseSchedule.initializeCourseSchedule();
        //Initialization.DepartmentInitialization.departmentDirectory = new DepartmentDirectory();
        Initialization.DepartmentInitialization.initializeBCOFDepartmentDirectory();
        Initialization.DepartmentInitialization.initializeBCOLDepartmentDirectory();
        Initialization.DepartmentInitialization.initializeBCOSDepartmentDirectory();
        Initialization.DepartmentInitialization.initializeBCOSoDepartmentDirectory();
        Initialization.DepartmentInitialization.initializeCOADepartmentDirectory();
        Initialization.DepartmentInitialization.initializeCOCDepartmentDirectory();
        Initialization.DepartmentInitialization.initializeCOEDepartmentDirectory();
        Initialization.DepartmentInitialization.initializeCOFDepartmentDirectory();
        Initialization.DepartmentInitialization.initializeCOHDepartmentDirectory();
        Initialization.DepartmentInitialization.initializeCOLDepartmentDirectory();
        Initialization.DepartmentInitialization.initializeCOSDepartmentDirectory();
        Initialization.DepartmentInitialization.initializeCOSoDepartmentDirectory();
        Initialization.DepartmentInitialization.initializeHCOADepartmentDirectory();
        Initialization.DepartmentInitialization.initializeHCOBDepartmentDirectory();
        Initialization.DepartmentInitialization.initializeHCOCDepartmentDirectory();
        Initialization.DepartmentInitialization.initializeHCOEDepartmentDirectory();

        Initialization.CollegeInitialization.initializeBUCollegeDirectory();
        Initialization.CollegeInitialization.initializeHVCollegeDirectory();
        Initialization.CollegeInitialization.initializeNUCollegeDirectory();

        Initialization.UniversityInitialization.initializeUniversity();

        Initialization.StudentInitialization.initializeStudent();

        System.out.println("*********Welcome to the EDUCATION LEVEL ECOSYSTEM*************** ");
        System.out.println("*********Please enter the choice below for Reports*************** ");
        System.out.println("1.DEPARTMENT LEVEL REPORTS");
        System.out.println("2.COLLEGE LEVEL REPORTS");
        System.out.println("3.UNIVERSITY LEVEL REPORTS");
        System.out.println("4.EDUCATION ECO SYSTEM REPORTS");

        Scanner reader = new Scanner(System.in);  // Reading from System.in
        System.out.println("Enter a choice.");
        int choice = reader.nextInt(); // Scans the next token of the input as an int.

        Department department;

        switch (choice) {
            case 1:
                System.out.println("***************DEPARTMENT LEVEL REPORTS STARTS******************");
                printDepartmentReport();
                System.out.println("\n ***************DEPARTMENT LEVEL REPORT ENDS******************");
                break;

            case 2:
                //department=Initialization.DepartmentInitialization.initializeCOEDepartmentDirectory().searchDepartment("Information Assurance");
                //System.out.println("Number of students registered in this department:"+String.valueOf(department.getDepartmentStudentDirectory().getStudentList().size()));
                //break;
                System.out.println("***************COLLEGE LEVEL REPORTS STARTS******************");
                printCollegeReport();
                System.out.println("***************COLLEGE LEVEL REPORT ENDS******************");
                break;

            case 3:

                System.out.println("***************UNIVERSITY LEVEL REPORTS STARTS******************");
                printUniversityReport();
                System.out.println("\n ***************UNIVERSITY LEVEL REPORT ENDS******************");
                break;

            case 4:
                System.out.println("***************EDUCATION ECO SYSTEM LEVEL REPORTS STARTS******************");
                printEducationEcoSystemReport();
                System.out.println("***************EDUCATION ECO SYSTEM LEVEL REPORT ENDS******************");
                break;
            default:
                System.out.println("INPUT ENTERED IS INCORRECT");
                break;

        }

    }

    private static void printDepartmentReport() {

        System.out.println("1.Which courses are taught in a given semester?");
        for (Course course : InitializeCourseCatalog.initializeISCourseCatalog().getDepartmentCourseList()) {
            System.out.println("Course Name-->" + course.getCourseName());
        }
        System.out.println(" \n 2.What is the course being offered?");
        for (CourseOffering courseOffering : InitializeCourseSchedule.initializeCourseSchedule().getDepartmentCourseSchedule()) {
            System.out.println(
                    "\n Teacher ---> " + courseOffering.getTeacher()
                    + "\n Timing ---> " + courseOffering.getTiming()
                    + "\n Capacity ---> " + courseOffering.getCapacity()
                    + "\n SeatsFilled ---> " + courseOffering.getSeatsFilled()
                    + "\n Vacant seats ---> " + courseOffering.getSeatsVacant()
                    + "\n Section number ---> " + courseOffering.getSectionNumber()
                    + "\n Class Room Number ---> " + courseOffering.getClassRoom().getRoomNumber()
                    + "\n Class Room Building ---> " + courseOffering.getClassRoom().getBuildingNumber()
                    + "\n Semester name ---> " + courseOffering.getSemester().getSemesterName()
                    + "\n Semester start date ---> " + courseOffering.getSemester().getStartDate()
                    + "\n Semester End Date ---> " + courseOffering.getSemester().getEndDate()
                    + "\n Course Number ---> " + courseOffering.getCourse().getCourseNumber()
                    + "\n Course Name ---> " + courseOffering.getCourse().getCourseName()
            );

        }

        System.out.println(" \n 3.Who is teaching the course?");
        for (CourseOffering courseOffering : InitializeCourseSchedule.initializeCourseSchedule().getDepartmentCourseSchedule()) {
            System.out.println(
                    "\n Teacher ---> " + courseOffering.getTeacher()
                    + "\n Course Number ---> " + courseOffering.getCourse().getCourseNumber()
                    + "\n Course Name ---> " + courseOffering.getCourse().getCourseName()
            );
        }

        System.out.println("\n 4.When is it offered?");
        for (CourseOffering courseOffering : InitializeCourseSchedule.initializeCourseSchedule().getDepartmentCourseSchedule()) {
            System.out.println("\n Course Number ---> " + courseOffering.getCourse().getCourseNumber()
                    + "\n Course Name ---> " + courseOffering.getCourse().getCourseName()
                    + "\n Semester name ---> " + courseOffering.getSemester().getSemesterName()
            );
        }

        System.out.println("\n 5.Where is it offered?");

        for (CourseOffering courseOffering : InitializeCourseSchedule.initializeCourseSchedule().getDepartmentCourseSchedule()) {
            System.out.println("\n Course Number ---> " + courseOffering.getCourse().getCourseNumber()
                    + "\n Course Name ---> " + courseOffering.getCourse().getCourseName()
                    + "\n Class Room Number ---> " + courseOffering.getClassRoom().getRoomNumber()
                    + "\n Class Room Building ---> " + courseOffering.getClassRoom().getBuildingNumber()
            );

        }

        System.out.println("\n 6.Who are the students signed up for the class?");

        StudentDirectory studentDirectory = Initialization.StudentInitialization.initializeStudent();
        for (Student student : studentDirectory.getStudentList()) {
            System.out.println("\n Student Id --> " + student.getStudentId()
                    + "\n Student Name --> " + student.getStudentName()
            );
        }

        System.out.println("\n 7.Find course by course number");
        for (CourseOffering courseOffering : InitializeCourseSchedule.initializeCourseSchedule().getDepartmentCourseSchedule()) {
            System.out.println("\n Course Number ---> " + courseOffering.getCourse().getCourseNumber()
                    + "\n Course Name ---> " + courseOffering.getCourse().getCourseNumber()
            );
        }

        //for(Course course : InitializeCourseCatalog.)
        System.out.println("\n 8.Number of students signed up for the class students signed up for the class?");
        for (CourseOffering courseOffering : InitializeCourseSchedule.initializeCourseSchedule().getDepartmentCourseSchedule()) {
            System.out.println(courseOffering.getStudentsSignedUpForTheClass());
        }

        System.out.println("\n 9.What are the pre requisite courses for Application Engineering and Development course?");
        for (Course course : InitializeCourseCatalog.initializeISCourseCatalog().getDepartmentCourseList()) {
            if (course.getPreRequisiteCourses() != null) {
                System.out.println("Pre-requisite courses: ");
                for (Course preReqCourse : course.getPreRequisiteCourses()) {
                    System.out.print(preReqCourse.getCourseName());
                }
                System.out.println("\n");
            }
        }

        System.out.println("\n 10.Find course offering which needs department attention(less than 50% enrollment)?");
        for (CourseOffering courseOffering : InitializeCourseSchedule.initializeCourseSchedule().courseOfferingWithLessThanFiftyPercentEnrollment()) {

            System.out.println("\n Teacher ---> " + courseOffering.getTeacher()
                    + "\n Timing ---> " + courseOffering.getTiming()
                    + "\n Capacity ---> " + courseOffering.getCapacity()
                    + "\n SeatsFilled ---> " + courseOffering.getSeatsFilled()
                    + "\n Vacant seats ---> " + courseOffering.getSeatsVacant()
                    + "\n Section number ---> " + courseOffering.getSectionNumber()
                    + "\n Class Room Number ---> " + courseOffering.getClassRoom().getRoomNumber()
                    + "\n Class Room Building ---> " + courseOffering.getClassRoom().getBuildingNumber()
                    + "\n Semester name ---> " + courseOffering.getSemester().getSemesterName()
                    + "\n Semester start date ---> " + courseOffering.getSemester().getStartDate()
                    + "\n Semester End Date ---> " + courseOffering.getSemester().getEndDate()
                    + "\n Course Number ---> " + courseOffering.getCourse().getCourseNumber()
                    + "\n Course Name ---> " + courseOffering.getCourse().getCourseName()
            );

        }

        System.out.println("\n 11.Find total earning of a department");
        double totalEarning = 0;
        for (CourseOffering courseOffering : InitializeCourseSchedule.initializeCourseSchedule().getDepartmentCourseSchedule()) {
            totalEarning = totalEarning + courseOffering.getCourseEarning();

        }
        System.out.println("\n Department Earning-->" + totalEarning);

        System.out.println("\n 12.List all courses whose credit hrs are less than 4");
        for (CourseOffering courseOffering : InitializeCourseSchedule.initializeCourseSchedule().getDepartmentCourseSchedule()) {
            if (Integer.parseInt(courseOffering.getCourse().getCreditHours()) < 4) {
                System.out.println("Course Name -->" + courseOffering.getCourse().getCourseName());
            }
        }

        System.out.println("\n 13.How many seats are available in particular course offering");
        for (CourseOffering courseOffering : InitializeCourseSchedule.initializeCourseSchedule().getDepartmentCourseSchedule()) {
            System.out.println("\n Teacher ---> " + courseOffering.getTeacher()
                    + "\n Timing ---> " + courseOffering.getTiming()
                    + "\n Capacity ---> " + courseOffering.getCapacity()
                    + "\n SeatsFilled ---> " + courseOffering.getSeatsFilled()
                    + "\n Vacant seats ---> " + courseOffering.getSeatsVacant()
                    + "\n Section number ---> " + courseOffering.getSectionNumber()
                    + "\n Class Room Number ---> " + courseOffering.getClassRoom().getRoomNumber()
                    + "\n Class Room Building ---> " + courseOffering.getClassRoom().getBuildingNumber()
                    + "\n Semester name ---> " + courseOffering.getSemester().getSemesterName()
                    + "\n Semester start date ---> " + courseOffering.getSemester().getStartDate()
                    + "\n Semester End Date ---> " + courseOffering.getSemester().getEndDate()
                    + "\n Course Number ---> " + courseOffering.getCourse().getCourseNumber()
                    + "\n Course Name ---> " + courseOffering.getCourse().getCourseName()
                    + "\n Seats Available -->" + courseOffering.getSeatsVacant());
        }

        System.out.println("\n 14.List timing of a  particular course offering");
        for (CourseOffering courseOffering : InitializeCourseSchedule.initializeCourseSchedule().getDepartmentCourseSchedule()) {
            System.out.println("\n Course Number ---> " + courseOffering.getCourse().getCourseNumber()
                    + "\n Course Name ---> " + courseOffering.getCourse().getCourseName()
                    + "\n Timing ---> " + courseOffering.getTiming()
            );
        }

        System.out.println("\n 15.Earning from all the courses of a department");
        for (CourseOffering courseOffering : InitializeCourseSchedule.initializeCourseSchedule().getDepartmentCourseSchedule()) {
            System.out.println("Course Number -->" + courseOffering.getCourse().getCourseName()
                    + "\n Course Earning-->" + courseOffering.getCourseEarning()
            );
        }

        System.out.println("\n 16.List all the courses being offered by a dept");
        for (CourseOffering courseOffering : InitializeCourseSchedule.initializeCourseSchedule().getDepartmentCourseSchedule()) {
            System.out.println("\n Course name-->" + courseOffering.getCourse().getCourseName());
        }

        /*System.out.println("\n 17.List all the courses without any pre requisite");
        for (Course course : InitializeCourseCatalog.initializeISCourseCatalog().getDepartmentCourseList()) {
            if (course.getPreRequisiteCourses() != null) {
                System.out.println("Pre-requisite courses: ");
                for (Course preReqCourse : course.getPreRequisiteCourses()) {
                    if (course.getPreRequisiteCourses().isEmpty()) {
                        System.out.print("\n Course Name--> " + course.getCourseName());
                    }

                }
            }
            System.out.println("\n");
        }*/

        System.out.println("\n 18.List all the degree under a department and their requirement");
        for (Degree degree : DegreeInitialization.initializeISDegrees().getDegreeList()) {
            System.out.println("\n Degree Name -->" + degree.getDegreeName()
                    + "\n Number of Core Courses-->" + degree.getNumberOfCoreCourses()
                    + "\n Number of elective courses-->" + degree.getNumberOfElectiveCourses()
            );
        }

        System.out.println("\n 19.How many students are registered in COE department?");
        Department department = Initialization.DepartmentInitialization.initializeCOEDepartmentDirectory().searchDepartment("Information Systems");
        System.out.println("Number of students registered in COE department:" + String.valueOf(department.getDepartmentStudentDirectory().getStudentList().size()));

    }

    private static void printUniversityReport() {
        System.out.println("Is Northeastern university profitable");
        double totalEarning = 0;
        for (College college : CollegeInitialization.initializeNUCollegeDirectory().getCollegeList()) {

            for (CourseOffering courseOffering : InitializeCourseSchedule.initializeCourseSchedule().getDepartmentCourseSchedule()) {
                totalEarning = totalEarning + courseOffering.getCourseEarning();

            }
        }
        System.out.println("\n Northeastern University Earning-->" + totalEarning);

        System.out.println("What is the current student enrollment in our university?");
        int enrollment = 0;
        for (University university : UniversityInitialization.initializeUniversity().getUniversityList()) {
            for (CourseOffering courseOffering : InitializeCourseSchedule.initializeCourseSchedule().getDepartmentCourseSchedule()) {
                enrollment = enrollment + courseOffering.getStudentsSignedUpForTheClass();

            }
            System.out.println("\n Student enrollment-->" + enrollment);
        }

        System.out.println("\n total number of students in a university");
        int totalNumOfStudents = Initialization.StudentInitialization.initializeStudent().getStudentList().size();
        System.out.println("\n Total number of students in a university" + totalNumOfStudents);
    }

    private static void printEducationEcoSystemReport() {
        System.out.println("List all the universities");
        for (University university : UniversityInitialization.initializeUniversity().getUniversityList()) {
            System.out.println("University name:" + university.getUniversityName());
        }

        System.out.println("Total number of universities in education eco system");
        int count = 0;
        for (University university : UniversityInitialization.initializeUniversity().getUniversityList()) {
            if (university.getUniversityName().isEmpty()) {
                System.out.println(" ");
            } else {
                count++;

            }
        }
        System.out.println("Total number of universities are:" + count);
    }

    private static void printCollegeReport() {
        System.out.println("What is the total earning of a college?");
        int total = 0;
        for (Department department : DepartmentInitialization.departmentDirectory.getDepartmentList()) {
            for (CourseOffering courseOffering : InitializeCourseSchedule.initializeCourseSchedule().getDepartmentCourseSchedule()) {
                total = total + courseOffering.getSeatsFilled();
            }
        }
        System.out.println("total earning of college:" + total);

        System.out.println("\n What is the average number of students per class?");
        int count = 0;
        int numberOfStudents = 0;
        for (CourseOffering courseOffering : Initialization.InitializeCourseSchedule.initializeCourseSchedule().getDepartmentCourseSchedule()) {
            numberOfStudents += courseOffering.getSeatsFilled();
            count++;
        }
        int numberOfClasses = count;
        //2-way 
        //numberOfClasses=Initialization.InitializeCourseSchedule.initializeCourseSchedule().getDepartmentCourseSchedule().size();
        float average = numberOfStudents / numberOfClasses;
        System.out.println("Average number of students for class: " + String.valueOf(average));
        System.out.println("Largest class? Smallest class?");

        //largest and smallest classes
        CourseOffering largestClass = Initialization.InitializeCourseSchedule.initializeCourseSchedule().getDepartmentCourseSchedule().get(0);
        CourseOffering smallestClass = Initialization.InitializeCourseSchedule.initializeCourseSchedule().getDepartmentCourseSchedule().get(0);
        for (CourseOffering courseOffering : Initialization.InitializeCourseSchedule.initializeCourseSchedule().getDepartmentCourseSchedule()) {
            if (courseOffering.getSeatsFilled() > largestClass.getSeatsFilled()) {
                largestClass = courseOffering;
            }
            if (courseOffering.getSeatsFilled() < smallestClass.getSeatsFilled()) {
                smallestClass = courseOffering;
            }
        }

        System.out.println("Largest class is: " + largestClass.getSectionNumber() + "" + largestClass.getCourse().getCourseName() + "with " + largestClass.getSeatsFilled() + "seats filled");
        System.out.println("Smallest class is: " + smallestClass.getSectionNumber() + "" + smallestClass.getCourse().getCourseName() + "with" + smallestClass.getSeatsFilled() + "seats filled");

        System.out.println("3.What is the current student enrollment in our college broken down by department?");
        int enrollment = 0;

        for (Department department : Initialization.DepartmentInitialization.initializeCOEDepartmentDirectory().getDepartmentList()) {
            for (CourseOffering courseOffering : Initialization.InitializeCourseSchedule.initializeCourseSchedule().getDepartmentCourseSchedule()) {

                enrollment += courseOffering.getSeatsFilled();
            }

        }
        System.out.println("Enrollment" + enrollment);

    }
}
